#ifndef APPLICATIONSETTINGS_H
#define APPLICATIONSETTINGS_H

#include <QString>

struct ApplicationSettings
{
    QString MainWorkingDirectory;

public:
    ApplicationSettings();
};

extern ApplicationSettings applicationSettings;

#endif // APPLICATIONSETTINGS_H
