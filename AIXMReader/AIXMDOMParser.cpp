#include "AIXMDOMParser.h"

AIXMDOMParser::AIXMDOMParser(QDomDocument& document)
{
    data = new AIXMData(document);
}

AIXMData* AIXMDOMParser::getData(){
    return data;
}
