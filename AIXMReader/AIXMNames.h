#ifndef AIXMNAMES_H
#define AIXMNAMES_H

/*
 * Файл с описанием всех, используемых нод в AIXM. Из него берутся константы
 * для чтения их в AIXM.
*/

// header of entity

#define AIXM_BASIC_MSG "AIXMBasicMessage"
#define AIXM_HAS_MEMBER "hasMember"
#define AIXM_TIME_SLICE "timeSlice"
#define AIXM_INTERPREATION "interpretation"
#define AIXM_SEQ_NUMBER "sequenceNumber"
#define AIXM_CORRECTION_NUMBER "correctionNumber"
#define AIXM_TIME_SLICE_METADATA "timeSliceMetadata"
#define AIXM_FEATURE_LIFE_TIME "featureLifeTime"
#define GML_IDENTIFIER "identifier"
#define GML_VALID_TIME "validTime"
#define GML_TIME_PERIOD "TimePeriod"
#define GML_POS "pos"
#define GML_POS_LIST "posList"
#define GML_BEGIN_POSITION "beginPosition"
#define GML_END_POSITION "endPosition"
#define AIXM_AIRPORT_HELIPORT "airportHeliprot"

// AircraftHaracteristic

#define AIXM_AIRCRAFT_CHARACTERISTIC "aircraftCharacteristic"
#define AIXM_AIRCRAFT_CHARACTERISTIC_A "AircraftCharacteristic"
#define AIXM_AIRCRAFT_LANDING_CAT "aircraftLandingCategory"

// flightTransition

#define AIXM_FLIGHT_TRANSITION "flightTransition"
#define AIXM_PROCEDURE_TRANSITION "ProcedureTransition"
#define AIXM_TYPE "type"
#define AIXM_VECTOR_HEADING "vectorHeading"
#define AIXM_DEPARTURE_RUNWAY_TRANSITION "departureRunwayTransition"
#define AIXM_LANDING_TAKE_OFF_AREA_COLLECTION "LandingTakeoffAreaCollection"
#define AIXM_RUNWAY "runway"
#define AIXM_TRANSITION_LEG "transitionLeg"
#define AIXM_TRANSITION_ID "transitionId"
#define AIXM_PROCEDURE_TRANSITION_LEG "ProcedureTransitionLeg"
#define AIXM_SEQ_NUMBER_ARINC "seqNumberARINC"
#define AIXM_THE_SEGMENT_LEG "theSegmentLeg"

// Procedure

#define AIXM_PROCEDURE "Procedure"
#define AIXM_APPROACH "InstrumentApproachProcedure"
#define AIXM_SID "StandardInstrumentDeparture"
#define AIXM_STAR "StandardInstrumentArrival"
#define AIXM_DESIGN_CRITERIA "designCriteria"
#define AIXM_CODING_STANDARD "codingStandard"
#define AIXM_COMMUNICATION_FAILURE_INSTRUCTION "communicationFailureInstruction"
#define AIXM_INSTRUCTION "instruction"
#define AIXM_FLIGHT_CHECKED "flightChecked"
#define AIXM_NAME "name"
#define AIXM_RNAV "RNAV"
#define AIXM_CONTINGENCY_ROUTE "contingencyRoute"
#define AIXM_ADDITIONAL_EQUIPMENT "additionalEquipment"
#define AIXM_APPROACH_PREFIX "approachPrefix"
#define AIXM_APPROACH_TYPE "approachType"
#define AIXM_CHANGEL_GNSS "channelGNSS"
#define AIXM_CIRCLING_IDENTIFICATION "circlingIdentification"
#define AIXM_COPTER_TRACK "copterTrack"
#define AIXM_COURSE_REVERSAL_INSTRUCTION "courseReversalInstruction"
#define AIXM_MULTIPLE_IDENTIFICATION "multipleIdentification"
#define AIXM_WAAS_RELIABLE "WAASReliable"

// AirportHeliport

#define AIXM_AIRPORT_HELIPORT_A "AirportHeliport"
#define AIXM_AIRPORT_HELIPORT_TIME_SLICE "AirportHeliportTimeSlice"
#define AIXM_FIELD_ELEVATION "fieldElevation"
#define AIXM_FIELD_ELEVATION_ACCURACY "fieldElevationAccuracy"
#define AIXM_MAGNETIC_VARIATION "magneticVariation"
#define AIXM_RESPONSIBLE_ORGANISATION "responsibleOrganisation"
#define AIXM_AIRPORT_HELIPORT_RESPONSIBILITY_ORGANISATION "AirportHeliportResponsibilityOrganisation"
#define AIXM_ROLE "role"
#define AIXM_THE_ORGANISATION_AUTHORITY "theOrganisationAuthority"
#define AIXM_ARP "ARP"
#define AIXM_ELEVATED_POINT "ElevatedPoint"

// OrganisationAuthority

#define AIXM_ORGANISATION_AUTHORITY "OrganisationAuthority"
#define AIXM_ORGANISATION_AUTHORITY_TIME_SLICE "OrganisationAuthorityTimeSlice"

// RunwayDirection

#define AIXM_RUNWAY_DIRECTION "RunwayDirection"
#define AIXM_RUNWAY_DIRECTION_TIME_SLICE "RunwayDirectionTimeSlice"
#define AIXM_DESIGNATIOR "designator"
#define AIXM_TRUE_BEARING "trueBearing"
#define AIXM_MAGNETIC_BEARING "magneticBearing"
#define AIXM_USED_RUNWAY "usedRunway"

// Runway

#define AIXM_RUNWAY_A "Runway"
#define AIXM_RUNWAY_TIME_SLICE "RunwayTimeSlice"
#define AIXM_NOMINAL_LENGTH "nominalLength"
#define AIXM_NOMINAL_WIDTH "nominalWidth"
#define AIXM_LENGTH_STRIP "lengthStrip"
#define AIXM_WIDTH_STRIP "widthStrip"
#define AIXM_LENGTH_OFFSET "lengthOffset"
#define AIXM_WIDTH_OFFSET "widthOffset"
#define AIXM_ASSOCIATED_AIRPORT_HELIPORT "associatedAirportHeliport"

// InitailLeg

#define AIXM_INITAL_LEG "InitialLeg"
#define AIXM_INITAL_LEG_TIME_SLICE "InitialLegTimeSlice"
#define AIXM_END_CONDITION_DESIGNATOR "endConditionDesignator"
#define AIXM_LEG_PATH "legPath"
#define AIXM_LEG_TYPE_ARINC "legTypeARINC"
#define AIXM_COURSE "course"
#define AIXM_COURSE_TYPE "courseType"
#define AIXM_BANK_ANGLE "bankAngle"
#define AIXM_PROCEDURE_TURN_REQUIRED "procedureTurnRequired"
#define AIXM_COURSE_DIRECTION "courseDirection"
#define AIXM_TURN_DIRECTION "turnDirection"
#define AIXM_SPEED_LIMIT "speedLimit"
#define AIXM_SPEED_REFERENCE "speedReference"
#define AIXM_SPEED_INTERPRETATION "speedInterpretation"
#define AIXM_LENGTH "length"
#define AIXM_DURATION "duration"
#define AIXM_UPPER_LIMIT_ALTITUDE "upperLimitAltitude"
#define AIXM_LOWER_LIMIT_ALTITUDE "lowerLimitAltitude"
#define AIXM_UPPER_LIMIT_REFERENCE "upperLimitReference"
#define AIXM_LOWER_LIMIT_REFERENCE "lowerLimitReference"
#define AIXM_ALTITUDE_INTERPRETATION "altitudeInterpretation"
#define AIXM_ALTITUDE_OVERRIDE_ATC "altitudeOverrideATC"
#define AIXM_ALTITUDE_OVERRIDE_REFERENCE "altitudeOverrideReference"
#define AIXM_VERTICAL_ANGLE "verticalAngle"
#define AIXM_START_POINT "startPoint"
#define AIXM_ARC_CENTRE "arcCentre"
#define AIXM_TERMINAL_SEGMENT_POINT "TerminalSegmentPoint"
#define AIXM_LEAD_RADIAL "leadRadial"
#define AIXM_LEAD_DME "leadDME"
#define AIXM_INDICATOR_FACF "indicatorFACF"
#define AIXM_REPORTING_ATC "reportingATC"
#define AIXM_FLY_OVER "flyOver"
#define AIXM_WAYPOINT "waypoint"
#define AIXM_RADAR_GUIDANCE "radarGuidance"
#define AIXM_POINT_CHOICE_FIX_DESIGNATED_POINT "pointChoice_fixDesignatedPoint"
#define AIXM_ROLE "role"
#define AIXM_END_POINT "endPoint"
#define AIXM_TRAJECTORY "trajectory"
#define AIXM_CURVE "Curve"
#define GML_SEGMENTS "segments"
#define GML_LINE_STRING_SEGMENT "LineStringSegment"

// DesignatedPoint

#define AIXM_DESIGNATED_POINT "DesignatedPoint"
#define AIXM_DESIGNATED_POINT_TIME_SLICE "DesignatedPointTimeSlice"
#define AIXM_LOCATION "location"
#define AIXM_POINT "Point"

// Airspace
#define AIXM_AIRSPACE "Airspace"
#define AIXM_CONTROL_TYPE "controlType"
#define AIXM_DESGINATOR_ICAO "designatorICAO"
#define AIXM_LOCAL_TYPE "localType"
#define AIXM_UPPER_LOWER_SEPARATION "upperLowerSeparation"
#define AIXM_GEOMETRY_COMPONENT "geometryComponent"
#define AIXM_AIRSPACE_GEOMETRY_COMPONENT "AirspaceGeometryComponent"
#define AIXM_THE_AIRSPACE_VOLUME "theAirspaceVolume"
#define AIXM_AIRSPACE_VOLUME "AirspaceVolume"
#define AIXM_UPPER_LIMIT "upperLimit"
#define AIXM_LOWER_LIMIT "lowerLimit"
#define AIXM_MINIMUM_LIMIT "minimumLimit"
#define AIXM_MAXIMUM_LIMIT "maximumLimit"
#define AIXM_HORIZONTAL_PROJECTION "horizontalProjection"
#define AIXM_CENTERLINE "centreline"

// VerticalStructure
#define AIXM_VERTICAL_STRUCTURE "VerticalStructure"
#define AIXM_GROUP "group"
#define AIXM_LIGHTED "lighted"
#define AIXM_LIGHTING_ICAO_STANDARD "lightingICAOStandard"
#define AIXM_MARKING_ICAO_STANDARD "markingICAOStandard"
#define AIXM_RADIUS "radius"
#define AIXM_SYNCHRONISED_LIGHTING "synchronisedLighting"
#define AIXM_WIDTH "width"

// VerticalStructurePart
#define AIXM_VERTICAL_STRUCTURE_PART "VerticalStructurePart"
#define AIXM_CONSTRUCTION_STATUS "constructionStatus"
#define AIXM_FRANGIBLE "frangible"
#define AIXM_MARKING_FIRST_COLOUR "markingFirstColour"
#define AIXM_MARKING_PATTERN "markingPattern"
#define AIXM_MARKING_SECOND_COLOUR "markingSecondColour"
#define AIXM_MOBILE "mobile"
#define AIXM_PART "part"
#define AIXM_VERTICAL_EXTENT "verticalExtent"
#define AIXM_VERTICAL_EXTENT_ACCURACY "verticalExtentAccuracy"
#define AIXM_VISIBLE_MATERIAL "visibleMaterial"
#define AIXM_ELEVATED_CURVE "ElevatedCurve"
#define AIXM_ELEVATED_SURFACE "ElevatedSurface"
#define AIXM_ELEVATED_POINT "ElevatedPoint"



// FinalLeg

#define AIXM_FINAL_LEG "FinalLeg"
#define AIXM_FINAL_LEG_TIME_SLICE "FinalLegTimeSlice"

// MissedApproachLeg

#define AIXM_MISSED_APPROACH_LEG "MissedApproachLeg"
#define AIXM_MISSED_APPROACH_LEG_TIME_SLICE "MissedApproachLegTimeSlice"

// Approach

#define AIXM_IAP "InstrumentApproachProcedure"
#define AIXM_IAP_TIME_SLICE "InstrumentApproachProcedureTimeSlice"
#define AIXM_APPROACH_PREFIX "approachPrefix"
#define AIXM_ADDITIONAL_EQUIPMENT "additionalEquipment"

#endif // AIXMNAMES_H
