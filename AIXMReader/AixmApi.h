#ifndef AIXMAPI_H
#define AIXMAPI_H

#include <QString>

#include "Entities/Procedure.h"
#include "Entities/Airspace.h"
#include "Entities/DesignatedPoint.h"
#include "Entities/VerticalStructure.h"
#include <QVector>

class AixmApi
{
private:
    static AixmApi* instance;
private:
    AixmApi() {}
    AixmApi( AixmApi& );
    AixmApi& operator=( AixmApi& );

public:
    virtual ~AixmApi();
    static AixmApi* getInstance();

    void parseFile(QString fileName);

//    Procedure* getProcedureByName(QString name);
//    BaseEntity* getProcedureByUuid(QString uuid);
//    QVector<Procedure*> getProceduresByType(ProcedureType type);
//    QVector<Procedure*> getAllProcedures();
    void addProcedure(Procedure* procedure);

//    Airspace* getAirspaceByName(QString name);
//    BaseEntity* getAirspaceByUuid(QString uuid);
//    QVector<Airspace*> getAirspacesByType(CodeAirspace type);
//    QVector<Airspace*> getAllAirspaces();
    void addAirspace(Airspace* airspace);

//    VerticalStructure* getObstacleByName(QString name);
//    BaseEntity* getObstacleByUuid(QString uuid);
//    QVector<VerticalStructure*> getObstaclesByType(CodeVerticalStructure type);
//    QVector<VerticalStructure*> getAllObstacles();
    void addObstacle(VerticalStructure* obstacle);

//    DesignatedPoint* getPointByName(QString name);
//    BaseEntity* getPointByUuid(QString uuid);
//    QVector<DesignatedPoint*> getAllPoints();
    void addPoint(DesignatedPoint* point);
};

#endif // AIXMAPI_H
