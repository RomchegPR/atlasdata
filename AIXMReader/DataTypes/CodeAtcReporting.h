#ifndef CODEATCREPORTING_H
#define CODEATCREPORTING_H

#define BETTER_ENUMS_STRICT_CONVERSION
#define BETTER_ENUMS_DEFAULT_CONSTRUCTOR(CodeATCReporting) \
  public:                                      \
    CodeATCReporting() = default;
#include "AIXMReader/Utils/enum.h"

BETTER_ENUM(
            CodeATCReporting,
            char,
            COMPULSORY,
            ON_REQUEST,
            NO_REPORT,
            OTHER
           );

/*enum class CodeATCReporting
{
    COMPULSORY,
    ON_REQUEST,
    NO_REPORT,
    OTHER,
    NONE
};*/

#endif // CODEATCREPORTING_H
