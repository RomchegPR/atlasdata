#ifndef CODEAIRCRAFTENGINE_H
#define CODEAIRCRAFTENGINE_H

#define BETTER_ENUMS_STRICT_CONVERSION
#define BETTER_ENUMS_DEFAULT_CONSTRUCTOR(CodeAircraftEngine) \
  public:                                      \
    CodeAircraftEngine() = default;
#include "AIXMReader/Utils/enum.h"

BETTER_ENUM(
            CodeAircraftEngine,
            char,
            JET,
            PISTON,
            TURBOPROP,
            ALL,
            OTHER
           );

/*enum class CodeAircraftEngine
{
    JET,
    PISTON,
    TURBOPROP,
    ALL,
    OTHER,
    NONE
};*/

#endif // CODEAIRCRAFTENGINE_H
