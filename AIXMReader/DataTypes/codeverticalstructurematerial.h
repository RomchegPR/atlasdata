#ifndef CODEVERTICALSTRUCTUREMATERIAL_H
#define CODEVERTICALSTRUCTUREMATERIAL_H

#define BETTER_ENUMS_STRICT_CONVERSION
#define BETTER_ENUMS_DEFAULT_CONSTRUCTOR(CodeVerticalStructureMaterial) \
  public:                                      \
    CodeVerticalStructureMaterial() = default;
#include "AIXMReader/Utils/enum.h"

BETTER_ENUM(
            CodeVerticalStructureMaterial,
            char,
            ADOBE_BRICK,
            ALUMINIUM,
            BRICK,
            CONCRETE,
            FIBREGLASS,
            GLASS,
            IRON,
            MASONRY,
            METAL,
            MUD,
            OTHER,
            PLANT,
            PRESTRESSED_CONCRETE,
            REINFORCED_CONCRETE,
            SOD,
            STEEL,
            STONE,
            TREATED_TIMBER,
            WOOD
           );


#endif // CODEVERTICALSTRUCTUREMATERIAL_H
