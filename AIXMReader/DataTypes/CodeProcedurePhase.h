#ifndef CODEPROCEDUREPHASE_H
#define CODEPROCEDUREPHASE_H

#define BETTER_ENUMS_STRICT_CONVERSION
#define BETTER_ENUMS_DEFAULT_CONSTRUCTOR(CodeProcedurePhase) \
  public:                                      \
    CodeProcedurePhase() = default;
#include "AIXMReader/Utils/enum.h"

BETTER_ENUM(
            CodeProcedurePhase,
            char,
            RWY,
            COMMON,
            EN_ROUTE,
            APPROACH,
            FINAL,
            MISSED,
            MISSED_P,
            MISSED_S,
            ENGINE_OUT,
            OTHER
           );

/*enum class CodeProcedurePhase
{
    RWY,
    COMMON,
    EN_ROUTE,
    APPROACH,
    FINAL,
    MISSED,
    MISSED_P,
    MISSED_S,
    ENGINE_OUT,
    OTHER,
    NONE
};*/

#endif // CODEPROCEDUREPHASE_H
