#ifndef CODEAPPROACHGUIDANCE_H
#define CODEAPPROACHGUIDANCE_H

#define BETTER_ENUMS_STRICT_CONVERSION
#define BETTER_ENUMS_DEFAULT_CONSTRUCTOR(CodeApproachGuidance) \
  public:                                      \
    CodeApproachGuidance() = default;
#include "AIXMReader/Utils/enum.h"

BETTER_ENUM(
            CodeApproachGuidance,
            char,
            NON_PRECISION,
            ILS_PRECISION_CAT_I,
            ILS_PRECISION_CAT_II,
            ILS_PRECISION_CAT_IIIA,
            ILS_PRECISION_CAT_IIIB,
            ILS_PRECISION_CAT_IIIC,
            ILS_PRECISION_CAT_IIID,
            MLS_PRECISION,
            OTHER
           );

/*enum class CodeApproachGuidance
{
    NON_PRECISION,
    ILS_PRECISION_CAT_I,
    ILS_PRECISION_CAT_II,
    ILS_PRECISION_CAT_IIIA,
    ILS_PRECISION_CAT_IIIB,
    ILS_PRECISION_CAT_IIIC,
    ILS_PRECISION_CAT_IIID,
    MLS_PRECISION,
    OTHER,
    NONE
};*/

#endif // CODEAPPROACHGUIDANCE_H
