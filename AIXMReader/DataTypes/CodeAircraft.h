#ifndef CODEAIRCRAFT_H
#define CODEAIRCRAFT_H

#define BETTER_ENUMS_STRICT_CONVERSION
#define BETTER_ENUMS_DEFAULT_CONSTRUCTOR(CodeAircraft) \
  public:                                      \
    CodeAircraft() = default;
#include "AIXMReader/Utils/enum.h"

/*enum class CodeAircraft
{
    LANDPLANE,
    SEAPLANE,
    AMPHIBIAN,
    HELICOPTER,
    GYROCOPTER,
    TILT_WING,
    STOL,
    GLIDER,
    GANGGLIDER,
    PARAGLIDER,
    ULTA_LIGHT,
    BALLOON,
    UAV,
    ALL,
    OTHER,
    NONE
};*/

BETTER_ENUM(
            CodeAircraft,
            char,
            LANDPLANE,
            SEAPLANE,
            AMPHIBIAN,
            HELICOPTER,
            GYROCOPTER,
            TILT_WING,
            STOL,
            GLIDER,
            GANGGLIDER,
            PARAGLIDER,
            ULTA_LIGHT,
            BALLOON,
            UAV,
            ALL,
            OTHER
           );

#endif // CODEAIRCRAFT_H
