#ifndef CODEDIRECTIONREFERENCE_H
#define CODEDIRECTIONREFERENCE_H

#define BETTER_ENUMS_STRICT_CONVERSION
#define BETTER_ENUMS_DEFAULT_CONSTRUCTOR(CodeDirectionReference) \
  public:                                      \
    CodeDirectionReference() = default;
#include "AIXMReader/Utils/enum.h"

BETTER_ENUM(
            CodeDirectionReference,
            char,
            TO,
            FROM,
            OTHER
           );

/*enum class CodeDirectionReference
{
    TO,
    FROM,
    OTHER,
    NONE
};*/

#endif // CODEDIRECTIONREFERENCE_H
