#ifndef CODEAIRCRAFTCATEGORY_H
#define CODEAIRCRAFTCATEGORY_H

#define BETTER_ENUMS_STRICT_CONVERSION
#define BETTER_ENUMS_DEFAULT_CONSTRUCTOR(CodeAircraftCategory) \
  public:                                      \
    CodeAircraftCategory() = default;
#include "AIXMReader/Utils/enum.h"

BETTER_ENUM(
            CodeAircraftCategory,
            char,
            A,
            B,
            C,
            D,
            E,
            H,
            ALL,
            OTHER
           );

/*enum class CodeAircraftCategory
{
    A,
    B,
    C,
    D,
    E,
    H,
    ALL,
    OTHER,
    NONE
};*/



#endif // CODEAIRCRAFTCATEGORY_H
