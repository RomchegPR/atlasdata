#ifndef CODEAPPROACHEQUIPMENTADDITIONAL_H
#define CODEAPPROACHEQUIPMENTADDITIONAL_H

#define BETTER_ENUMS_STRICT_CONVERSION
#define BETTER_ENUMS_DEFAULT_CONSTRUCTOR(CodeApproachEquipmentAdditional) \
  public:                                      \
    CodeApproachEquipmentAdditional() = default;
#include "AIXMReader/Utils/enum.h"

BETTER_ENUM(
            CodeApproachEquipmentAdditional,
            char,
            ADF,
            DME,
            RADAR,
            RADARDME,
            VORLOC,
            DUALVORDME,
            DUALADF,
            ADFMA,
            SPECIAL,
            DUALVHF,
            GPSRNP3,
            ADFILS,
            DUALADF_DME,
            RADAR_RNAV,
            OTHER
           );

/*enum class CodeApproachEquipmentAdditional
{
    ADF,
    DME,
    RADAR,
    RADARDME,
    VORLOC,
    DUALVORDME,
    DUALADF,
    ADFMA,
    SPECIAL,
    DUALVHF,
    GPSRNP3,
    ADFILS,
    DUALADF_DME,
    RADAR_RNAV,
    OTHER,
    NONE
};*/

#endif // CODEAPPROACHEQUIPMENTADDITIONAL_H
