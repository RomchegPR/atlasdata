#ifndef CODECARDINALDIRECTION_H
#define CODECARDINALDIRECTION_H

#define BETTER_ENUMS_STRICT_CONVERSION
#define BETTER_ENUMS_DEFAULT_CONSTRUCTOR(CodeCardinalDirection) \
  public:                                      \
    CodeCardinalDirection() = default;
#include "AIXMReader/Utils/enum.h"

BETTER_ENUM(
            CodeCardinalDirection,
            char,
            N,
            NE,
            E,
            SE,
            S,
            SW,
            W,
            NW,
            NNE,
            ENE,
            ESE,
            SSE,
            SSW,
            WSW,
            WNW,
            NNW,
            OTHER
           );

/*enum class CodeCardinalDirection
{
    N,
    NE,
    E,
    SE,
    S,
    SW,
    W,
    NW,
    NNE,
    ENE,
    ESE,
    SSE,
    SSW,
    WSW,
    WNW,
    NNW,
    OTHER,
    NONE
};*/

#endif // CODECARDINALDIRECTION_H
