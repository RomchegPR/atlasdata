#ifndef CODEMISSEDAPPROACH_H
#define CODEMISSEDAPPROACH_H

#define BETTER_ENUMS_STRICT_CONVERSION
#define BETTER_ENUMS_DEFAULT_CONSTRUCTOR(CodeMissedApproach) \
  public:                                      \
    CodeMissedApproach() = default;
#include "AIXMReader/Utils/enum.h"

BETTER_ENUM(
            CodeMissedApproach,
            char,
            PRIMARY,
            SECONDARY,
            ALTERNATE,
            TACAN,
            TACANALT,
            ENGINEOUT,
            OTHER
           );

/*enum class CodeMissedApproach
{
    PRIMARY,
    SECONDARY,
    ALTERNATE,
    TACAN,
    TACANALT,
    ENGINEOUT,
    OTHER,
    NONE
};*/

BETTER_ENUM(TEST, char, SANYA, MISHA, ROMA)

#endif // CODEMISSEDAPPROACH_H
