#ifndef CODEAIRCRAFTENGINENUMBER_H
#define CODEAIRCRAFTENGINENUMBER_H

#define BETTER_ENUMS_STRICT_CONVERSION
#define BETTER_ENUMS_DEFAULT_CONSTRUCTOR(CodeAircraftEngineNumber) \
  public:                                      \
    CodeAircraftEngineNumber() = default;
#include "AIXMReader/Utils/enum.h"

BETTER_ENUM(
            CodeAircraftEngineNumber,
            char,
            ONE,
            TWO,
            THREE,
            FOUR,
            SIX,
            EIGHT,
            TWO_C,
            OTHER
           );

/*enum class CodeAircraftEngineNumber
{
    ONE = 1,
    TWO,
    THREE,
    FOUR,
    SIX = 6,
    EIGHT = 8,
    TWO_C,
    OTHER,
    NONE
};*/

#endif // CODEAIRCRAFTENGINENUMBER_H
