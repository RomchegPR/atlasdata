#ifndef CODEAPPROACHPREFIX_H
#define CODEAPPROACHPREFIX_H

#define BETTER_ENUMS_STRICT_CONVERSION
#define BETTER_ENUMS_DEFAULT_CONSTRUCTOR(CodeApproachPrefix) \
  public:                                      \
    CodeApproachPrefix() = default;
#include "AIXMReader/Utils/enum.h"

BETTER_ENUM(
            CodeApproachPrefix,
            char,
            HI,
            COPTER,
            CONVERGING,
            OTHER
           );


/*enum class CodeApproachPrefix
{
    HI,
    COPTER,
    CONVERGING,
    OTHER,
    NONE
};*/

#endif // CODEAPPROACHPREFIX_H
