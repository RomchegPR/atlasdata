#ifndef CODEVALUEINTERPRETATION_H
#define CODEVALUEINTERPRETATION_H

#define BETTER_ENUMS_STRICT_CONVERSION
#define BETTER_ENUMS_DEFAULT_CONSTRUCTOR(CodeValueInterpretation) \
  public:                                      \
    CodeValueInterpretation() = default;
#include "AIXMReader/Utils/enum.h"

BETTER_ENUM(
            CodeValueInterpretation,
            char,
            ABOVE,
            AT_OR_ABOVE,
            AT_OR_BELOW,
            BELOW,
            OTHER
           );

/*enum class CodeValueInterpretation
{
    ABOVE,
    AT_OR_ABOVE,
    AT_OR_BELOW,
    BELOW,
    OTHER,
    NONE
};*/

#endif // CODEVALUEINTERPRETATION_H
