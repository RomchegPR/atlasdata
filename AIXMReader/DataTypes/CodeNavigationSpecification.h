#ifndef CODENAVIGATIONSPECIFICATION_H
#define CODENAVIGATIONSPECIFICATION_H

#define BETTER_ENUMS_STRICT_CONVERSION
#define BETTER_ENUMS_DEFAULT_CONSTRUCTOR(CodeNavigationSpecification) \
  public:                                      \
    CodeNavigationSpecification() = default;
#include "AIXMReader/Utils/enum.h"

BETTER_ENUM(
            CodeNavigationSpecification,
            char,
            RNAV_10,
            RNAV_5,
            RNAV_2,
            RNAV_1,
            RNP_4,
            RNP_2,
            BASIC_RNP_1,
            AVANCED_RNP_1,
            RNP_APCH,
            RNP_AP_APCH,
            OTHER
           );

/*enum class CodeNavigationSpecification
{
    RNAV_10,
    RNAV_5,
    RNAV_2,
    RNAV_1,
    RNP_4,
    RNP_2,
    BASIC_RNP_1,
    AVANCED_RNP_1,
    RNP_APCH,
    RNP_AP_APCH,
    OTHER,
    NONE
};*/

#endif // CODENAVIGATIONSPECIFICATION_H
