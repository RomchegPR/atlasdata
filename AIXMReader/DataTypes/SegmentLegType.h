#ifndef SEGMENTLEGTYPE_H
#define SEGMENTLEGTYPE_H

#define BETTER_ENUMS_STRICT_CONVERSION
#define BETTER_ENUMS_DEFAULT_CONSTRUCTOR(SegmentLegType) \
  public:                                      \
    SegmentLegType() = default;
#include "AIXMReader/Utils/enum.h"

BETTER_ENUM(
            SegmentLegType,
            char,
            SegmentLeg,
            ApproachLeg,
            ArrivalFeederLeg,
            DepartureLeg,
            FinalLeg,
            InitialLeg,
            IntermediateLeg,
            ArrivalLeg,
            MissedApproachLeg
           );

#endif // SEGMENTLEGTYPE_H
