#ifndef CODERELATIVEPOSITION_H
#define CODERELATIVEPOSITION_H

#define BETTER_ENUMS_STRICT_CONVERSION
#define BETTER_ENUMS_DEFAULT_CONSTRUCTOR(CodeRelativePosition) \
  public:                                      \
    CodeRelativePosition() = default;
#include "AIXMReader/Utils/enum.h"

BETTER_ENUM(
            CodeRelativePosition,
            char,
            BEFORE,
            AT,
            AFTER,
            OTHER
           );

/*enum class CodeRelativePosition
{
    BEFORE,
    AT,
    AFTER,
    OTHER,
    NONE
};*/

#endif // CODERELATIVEPOSITION_H
