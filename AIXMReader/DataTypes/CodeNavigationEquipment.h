#ifndef CODENAVIGATIONEQUIPMENT_H
#define CODENAVIGATIONEQUIPMENT_H

#define BETTER_ENUMS_STRICT_CONVERSION
#define BETTER_ENUMS_DEFAULT_CONSTRUCTOR(CodeNavigationEquipment) \
  public:                                      \
    CodeNavigationEquipment() = default;
#include "AIXMReader/Utils/enum.h"

BETTER_ENUM(
            CodeNavigationEquipment,
            char,
            DME,
            VOR_DME,
            DME_DME,
            TACAN,
            ILS,
            MLS,
            GNSS,
            WAAS,
            LORAN,
            INS,
            FMS,
            OTHER
           );

/*enum class CodeNavigationEquipment
{
    DME,
    VOR_DME,
    DME_DME,
    TACAN,
    ILS,
    MLS,
    GNSS,
    WAAS,
    LORAN,
    INS,
    FMS,
    OTHER,
    NONE
};*/

#endif // CODENAVIGATIONEQUIPMENT_H
