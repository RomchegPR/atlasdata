#ifndef GEOMETRYTYPE_H
#define GEOMETRYTYPE_H

#define BETTER_ENUMS_STRICT_CONVERSION
#define BETTER_ENUMS_DEFAULT_CONSTRUCTOR(GeometryType) \
  public:                                      \
    GeometryType() = default;
#include "AIXMReader/Utils/enum.h"

BETTER_ENUM(
            GeometryType,
            char,
            POINT,
            CURVE,
            SURFACE
           );

#endif // GEOMETRYTYPE_H
