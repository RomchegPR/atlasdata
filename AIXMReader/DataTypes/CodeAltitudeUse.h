#ifndef CODEALTITUDEUSE_H
#define CODEALTITUDEUSE_H

#define BETTER_ENUMS_STRICT_CONVERSION
#define BETTER_ENUMS_DEFAULT_CONSTRUCTOR(CodeAltitudeUse) \
  public:                                      \
    CodeAltitudeUse() = default;
#include "AIXMReader/Utils/enum.h"

BETTER_ENUM(
            CodeAltitudeUse,
            char,
            ABOVE_LOWER,
            BELOW_UPPER,
            AT_LOWER,
            BETWEEN,
            RECOMMENDED,
            EXPECT_LOWER,
            AS_ASSIGNED,
            OTHER
           );

/*enum class CodeAltitudeUse
{
    ABOVE_LOWER,
    BELOW_UPPER,
    AT_LOWER,
    BETWEEN,
    RECOMMENDED,
    EXPECT_LOWER,
    AS_ASSIGNED,
    OTHER,
    NONE
};*/

#endif // CODEALTITUDEUSE_H
