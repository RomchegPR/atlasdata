#ifndef CODESEGMENTPATH_H
#define CODESEGMENTPATH_H

#define BETTER_ENUMS_STRICT_CONVERSION
#define BETTER_ENUMS_DEFAULT_CONSTRUCTOR(CodeSegmentPath) \
  public:                                      \
    CodeSegmentPath() = default;
#include "AIXMReader/Utils/enum.h"

BETTER_ENUM(
            CodeSegmentPath,
            char,
            AF,
            HF,
            HA,
            HM,
            IF,
            PI,
            PT,
            TF,
            CA,
            CD,
            CI,
            CR,
            CF,
            DF,
            FA,
            FC,
            FT,
            FM,
            VM,
            FD,
            VR,
            VD,
            VI,
            VA,
            RF,
            OTHER
           );

/*enum class CodeSegmentPath
{
    AF,
    HF,
    HA,
    HM,
    IF,
    PI,
    PT,
    TF,
    CA,
    CD,
    CI,
    CR,
    CF,
    DF,
    FA,
    FC,
    FT,
    FM,
    VM,
    FD,
    VR,
    VD,
    VI,
    VA,
    RF,
    OTHER,
    NONE
};*/

#endif // CODESEGMENTPATH_H
