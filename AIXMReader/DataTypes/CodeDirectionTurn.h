#ifndef CODEDIRECTIONTURN_H
#define CODEDIRECTIONTURN_H

#define BETTER_ENUMS_STRICT_CONVERSION
#define BETTER_ENUMS_DEFAULT_CONSTRUCTOR(CodeDirectionTurn) \
  public:                                      \
    CodeDirectionTurn() = default;
#include "AIXMReader/Utils/enum.h"

BETTER_ENUM(
            CodeDirectionTurn,
            char,
            LEFT,
            RIGHT,
            EITHER,
            OTHER
           );

/*enum class CodeDirectionTurn
{
    LEFT,
    RIGHT,
    EITHER,
    OTHER,
    NONE
};*/

#endif // CODEDIRECTIONTURN_H
