#include "MapperFromXml.h"
#include <AIXMReader/AIXMNames.h>
#include <QStringList>
#include <AIXMReader/Utils/AixmHelper.h>
#include <AIXMReader/Entities/Procedure.h>
#include <AIXMReader/Entities/InstrumentApproachProcedure.h>
#include <AIXMReader/Entities/StandardInstrumentArrival.h>
#include <AIXMReader/Entities/StandardInstrumentDeparture.h>
#include <QDebug>
#include "AIXMReader/DataTypes/CodeDesignatedPoint.h"
#include "AIXMReader/Utils/AixmEnumMapper.h"
#include "AIXMReader/Utils/SegmentLegFactory.h"
#include "DataStorage/DataStorage.h"

MapperFromXml::MapperFromXml()
{
}

Procedure* MapperFromXml::mapProcedure(const QDomNode &node){
    QString nodeName = node.localName();
    QDomElement element = node.toElement();
    Procedure *procedure = NULL;

    if(nodeName == AIXM_APPROACH){
        procedure = new InstrumentApproachProcedure();
        InstrumentApproachProcedure* APCHprocedure = dynamic_cast<InstrumentApproachProcedure*>(procedure);

        QString additionalEquipment = AixmHelper::parseString(AIXM_ADDITIONAL_EQUIPMENT, element);
        //qDebug() << "additionalEquipment :" << additionalEquipment;
        if(additionalEquipment.isEmpty()){
            additionalEquipment = "OTHER";
        }
        APCHprocedure->setAdditionalEquipment(CodeApproachEquipmentAdditional::_from_string(additionalEquipment.toStdString().c_str()));

        QString approachPrefix = AixmHelper::parseString(AIXM_APPROACH_PREFIX, element);
        if(approachPrefix.isEmpty()){
            approachPrefix = "OTHER";
        }
        //qDebug() << "approachPrefix :" << approachPrefix;
        APCHprocedure->setApproachPrefix(CodeApproachPrefix::_from_string(approachPrefix.toStdString().c_str()));

        QString approachType = AixmHelper::parseString(AIXM_APPROACH_TYPE, element);
        //qDebug() << "approachType :" << approachType;
        if(approachType == ""){
            APCHprocedure->setApproachType(CodeApproach::OTHER);
        } else {
            APCHprocedure->setApproachType(CodeApproach::_from_string(approachType.toStdString().c_str()));
        }

        double channelGNSS = AixmHelper::parseDouble(AIXM_CHANGEL_GNSS, element);
        //qDebug() << "channelGNSS :" << channelGNSS;
        APCHprocedure->setChannelGNSS(channelGNSS);

        QString circlingIdentification = AixmHelper::parseString(AIXM_CIRCLING_IDENTIFICATION, element);
        //qDebug() << "circlingIdentification :" << circlingIdentification;
        if(circlingIdentification == ""){
            APCHprocedure->setCirclingIdentification(' ');
        } else {
            APCHprocedure->setCirclingIdentification(circlingIdentification.at(0).toLatin1());
        }


        double copterTrack = AixmHelper::parseDouble(AIXM_COPTER_TRACK, element);
        //qDebug() << "copterTrack :" << copterTrack;
        APCHprocedure->setCopterTrack(copterTrack);

        QString courseReversalInstruction = AixmHelper::parseString(AIXM_COURSE_REVERSAL_INSTRUCTION, element);
        //qDebug() << "courseReversalInstruction :" << courseReversalInstruction;
        APCHprocedure->setCourseReversalInstruction(courseReversalInstruction);

        QString multipleIdentification = AixmHelper::parseString(AIXM_MULTIPLE_IDENTIFICATION, element);
        //qDebug() << "multipleIdentification :" << multipleIdentification;
        if(multipleIdentification == ""){
            APCHprocedure->setMultipleIdentification(' ');
        } else {
            APCHprocedure->setMultipleIdentification(multipleIdentification.at(0).toLatin1());
        }


        QString WAASReliable = AixmHelper::parseString(AIXM_WAAS_RELIABLE, element);
        //qDebug() << "WAASReliable :" << WAASReliable;
        APCHprocedure->setWAASReliable((WAASReliable == "YES") ? true : false);

    } else if(nodeName == AIXM_SID){
        procedure = new StandardInstrumentDeparture();
        StandardInstrumentDeparture* SIDporcerure = dynamic_cast<StandardInstrumentDeparture*>(procedure);

        QString designator = AixmHelper::parseString(AIXM_DESIGNATIOR, element);
        SIDporcerure->setDesignator(designator);

        QString contingencyRoute = AixmHelper::parseString(AIXM_CONTINGENCY_ROUTE, element);
        SIDporcerure->setContingencyRoute((contingencyRoute == "YES") ? true : false);

    } else if(nodeName == AIXM_STAR){
        procedure = new StandardInstrumentArrival();
        StandardInstrumentArrival* STARporcerure = dynamic_cast<StandardInstrumentArrival*>(procedure);

        QString designator = AixmHelper::parseString(AIXM_DESIGNATIOR, element);
        STARporcerure->setDesignator(designator);
    }
    procedure->setType(ProcedureType::_from_string(nodeName.toStdString().c_str()));

    QString uuid = AixmHelper::parseString(GML_IDENTIFIER, element);
    procedure->setUuid(uuid);
    //qDebug() << "uuid :" << uuid;

    QString designCriteria = AixmHelper::parseString(AIXM_DESIGN_CRITERIA, element);
    if(designCriteria.isEmpty()){
        designCriteria = "OTHER";
    }
    procedure->setDesignCriteria(CodeDesignStandard::_from_string(designCriteria.toStdString().c_str()));
    //qDebug() << "designCriteria :" << designCriteria;

    QString codingStandard = AixmHelper::parseString(AIXM_CODING_STANDARD, element);
    procedure->setCodingStandard(CodeProcedureCodingStandard::_from_string(codingStandard.toStdString().c_str()));
    //qDebug() << "codingStandard :" << codingStandard;

    QString communicationFailureInstruction = AixmHelper::parseString(AIXM_COMMUNICATION_FAILURE_INSTRUCTION, element);
    procedure->setCommunicationFailureInstruction(communicationFailureInstruction);
    //qDebug() << "communicationFailureInstruction :" << communicationFailureInstruction;

    QString instruction = AixmHelper::parseString(AIXM_INSTRUCTION, element);
    procedure->setInstruction(instruction);
    //qDebug() << "instruction :" << instruction;

    QString flightChecked = AixmHelper::parseString(AIXM_FLIGHT_CHECKED, element);
    procedure->setFlightChecked((flightChecked == "YES") ? true : false );
    //qDebug() << "flightChecked :" << flightChecked;

    QString name = AixmHelper::parseString(AIXM_NAME, element);
    procedure->setName(name);
    //qDebug() << "name :" << name;

    QString RNAV = AixmHelper::parseString(AIXM_RNAV, element);
    procedure->setRNAV((RNAV == "YES") ? true : false);
    //qDebug() << "RNAV :" << RNAV;

    QDomNodeList flightTransitions = AixmHelper::parseNodeList(AIXM_FLIGHT_TRANSITION, element);
    //qDebug() << "Count of transitions: " << flightTransitions.size();

    QVector<ProcedureTransition*> procedureTransitions;

    for(int i = 0; i < flightTransitions.size(); i++){
        QDomNode procedureTransitionNode = AixmHelper::parseNode(AIXM_PROCEDURE_TRANSITION, flightTransitions.at(i).toElement());
        ProcedureTransition* procedureTransition = mapProcedureTransition(procedureTransitionNode);
        procedureTransitions.append(procedureTransition);
    }

    procedure->setFlightTransition(procedureTransitions);
    //qDebug() << procedure->getFlightTransition().size();
    return procedure;
}

SegmentLeg* MapperFromXml::mapSegmentLeg(const QDomNode &node){
    //qDebug() << node.localName().toStdString().c_str();
    SegmentLegType type = SegmentLegType::_from_string(node.localName().toStdString().c_str());
    //qDebug() << type._to_string();
    SegmentLeg* segmentLeg = SegmentLegFactory::makeLeg(type);
    if(segmentLeg){
        segmentLeg->setSegmentLegType(type);
        fillBaseSegmentLegData(node, segmentLeg);
        if(type != SegmentLegType::SegmentLeg){
            fillSpecificSegmentLegData(node, segmentLeg);
        }
    }
    return segmentLeg;
}

TerminalSegmentPoint* MapperFromXml::mapTerminalSegmentPoint(const QDomNode &node){
    QDomElement element = node.toElement();
    DataStorage *dataSource = DataStorage::getInstance();
    QDomNode fixDesignatedPoint = AixmHelper::parseNode(AIXM_POINT_CHOICE_FIX_DESIGNATED_POINT, element);
    QString uuid = AixmHelper::cutUuid(fixDesignatedPoint.toElement().attribute("href"));
    BaseEntity* designatedPoint = dataSource->getObject(uuid);
    TerminalSegmentPoint* point = NULL;

    if(designatedPoint != NULL){
        point = new TerminalSegmentPoint();
        point->setFixDesignatedPoint(designatedPoint);

        QString codeRoleProcedureFixRole = AixmHelper::parseString(AIXM_ROLE, element);
        if(codeRoleProcedureFixRole == "14" || codeRoleProcedureFixRole == ""){ // magic number in input data O_o;
            codeRoleProcedureFixRole = "OTHER";
        }
        //qDebug() << codeRoleProcedureFixRole;
        point->setRole(CodeProcedureFixRole::_from_string(codeRoleProcedureFixRole.toStdString().c_str()));

        double leadRadial = AixmHelper::parseDouble(AIXM_LEAD_RADIAL, element);
        point->setLeadRadial(leadRadial);

        double leadDME = AixmHelper::parseDouble(AIXM_LEAD_DME, element);
        point->setLeadDME(leadDME);

        QString indicatorFACFstr = AixmHelper::parseString(AIXM_INDICATOR_FACF, element);
        bool indicatorFACF = (indicatorFACFstr == "YES") ? true : false;
        point->setIndicatorFACF(indicatorFACF);

        QString codeATCReporting = AixmHelper::parseString(AIXM_REPORTING_ATC, element);
        if(codeATCReporting == ""){
            codeATCReporting = "OTHER";
        }
        CodeATCReporting reportingATC = CodeATCReporting::_from_string(codeATCReporting.toStdString().c_str());
        point->setReportingATC(reportingATC);

        QString flyOverStr = AixmHelper::parseString(AIXM_FLY_OVER, element);
        bool flyOver = (flyOverStr == "YES") ? true : false;
        point->setFlyOver(flyOver);

        QString waypointStr = AixmHelper::parseString(AIXM_WAYPOINT, element);
        bool waypoint = (waypointStr == "YES") ? true : false;
        point->setWaypoint(waypoint);

        QString radarGuidanceStr = AixmHelper::parseString(AIXM_RADAR_GUIDANCE, element);
        bool radarGuidance = (radarGuidanceStr == "YES") ? true : false;
        point->setRadarGuidance(radarGuidance);
    }

    //qDebug() << element.tagName();

    return point;
}

void MapperFromXml::fillBaseSegmentLegData(const QDomNode &node, SegmentLeg *segmentLeg){
    QDomElement element = node.toElement();
    QString uuid = AixmHelper::parseString(GML_IDENTIFIER, element);
    segmentLeg->setUuid(uuid);

    QString codeSegmentTermintaion = AixmHelper::parseString(AIXM_END_CONDITION_DESIGNATOR, element);
    segmentLeg->setEndConditionDesignator(CodeSegmentTermination::_from_string(codeSegmentTermintaion.toStdString().c_str()));
    //qDebug() << AIXM_END_CONDITION_DESIGNATOR << codeSegmentTermintaion;

    QString codeTrajectory = AixmHelper::parseString(AIXM_LEG_PATH, element);
    //qDebug() << AIXM_LEG_PATH << codeTrajectory;
    if(codeTrajectory.isEmpty()){
        segmentLeg->setLegPath(CodeTrajectory::OTHER);
    } else {
        segmentLeg->setLegPath(CodeTrajectory::_from_string(codeTrajectory.toStdString().c_str()));
    }

    QString codeSegmentPath = AixmHelper::parseString(AIXM_LEG_TYPE_ARINC, element);
    segmentLeg->setLegTypeARINC(CodeSegmentPath::_from_string(codeSegmentPath.toStdString().c_str()));
    //qDebug() << AIXM_LEG_TYPE_ARINC << codeSegmentPath;

    double course = AixmHelper::parseDouble(AIXM_COURSE, element);
    segmentLeg->setCourse(course);
    //qDebug() << AIXM_COURSE << course;

    QString codeCourse = AixmHelper::parseString(AIXM_COURSE_TYPE, element);
    if(codeCourse.isEmpty()){
        codeCourse = "OTHER";
    }
    segmentLeg->setCourseType(CodeCourse::_from_string(codeCourse.toStdString().c_str()));
    //qDebug() << AIXM_COURSE_TYPE << codeCourse;

    QString codeDirectionReference = AixmHelper::parseString(AIXM_COURSE_DIRECTION, element);
    segmentLeg->setCourseDirection(CodeDirectionReference::_from_string(codeDirectionReference.toStdString().c_str()));
    //qDebug() << AIXM_COURSE_DIRECTION << codeDirectionReference;

    QString codeDirectionTurn = AixmHelper::parseString(AIXM_TURN_DIRECTION, element);
    //qDebug() << AIXM_TURN_DIRECTION << codeDirectionTurn;
    if(codeDirectionTurn == ""){
        codeDirectionTurn = "OTHER";
    }
    segmentLeg->setTurnDirection(CodeDirectionTurn::_from_string(codeDirectionTurn.toStdString().c_str()));


    double speedLimit = AixmHelper::parseDouble(AIXM_SPEED_LIMIT, element);
    segmentLeg->setSpeedLimit(speedLimit);
    //qDebug() << AIXM_SPEED_LIMIT << speedLimit;

    QString codeSpeedReference = AixmHelper::parseString(AIXM_SPEED_REFERENCE, element);
    segmentLeg->setSpeedReference(CodeSpeedReference::_from_string(codeSpeedReference.toStdString().c_str()));
    //qDebug() << AIXM_SPEED_REFERENCE << codeSpeedReference;

    QString codeAltitudeUseSpeed = AixmHelper::parseString(AIXM_SPEED_INTERPRETATION, element);
    if(codeAltitudeUseSpeed == ""){
        codeAltitudeUseSpeed = "OTHER";
    }
    //qDebug() << codeAltitudeUseSpeed;
    segmentLeg->setSpeedIntrpretation(CodeAltitudeUse::_from_string(codeAltitudeUseSpeed.toStdString().c_str()));
    //qDebug() << AIXM_SPEED_INTERPRETATION << codeAltitudeUseSpeed;

    double bankAngle = AixmHelper::parseDouble(AIXM_BANK_ANGLE, element);
    segmentLeg->setBankAngle(bankAngle);
    //qDebug() << AIXM_BANK_ANGLE << bankAngle;

    double length = AixmHelper::parseDouble(AIXM_LENGTH, element);
    segmentLeg->setLength(length);
    //qDebug() << AIXM_LENGTH << length;

    double duration = AixmHelper::parseDouble(AIXM_DURATION, element);
    segmentLeg->setDuration(duration);
    //qDebug() << AIXM_DURATION << duration;

    QString procedureTurnRequiredString = AixmHelper::parseString(AIXM_PROCEDURE_TURN_REQUIRED, element);
    segmentLeg->setProcedureTurnRequired((procedureTurnRequiredString == "YES") ? true : false);
    //qDebug() << AIXM_PROCEDURE_TURN_REQUIRED << procedureTurnRequiredString;

    QString upperLimitAltitude = AixmHelper::parseString(AIXM_UPPER_LIMIT_ALTITUDE, element);
    segmentLeg->setUpperLimitAltitude(upperLimitAltitude);
    //qDebug() << AIXM_UPPER_LIMIT_ALTITUDE << upperLimitAltitude;

    QString codeVerticalReferenceUpper = AixmHelper::parseString(AIXM_UPPER_LIMIT_REFERENCE, element);
    if(codeVerticalReferenceUpper == ""){
        codeVerticalReferenceUpper = "OTHER";
    }
    segmentLeg->setUpperLimitReference(CodeVerticalReference::_from_string(codeVerticalReferenceUpper.toStdString().c_str()));
    //qDebug() << AIXM_UPPER_LIMIT_REFERENCE << codeVerticalReferenceUpper;

    QString lowerLimitAltitude = AixmHelper::parseString(AIXM_LOWER_LIMIT_ALTITUDE, element);

    segmentLeg->setLowerLimitAltitude(lowerLimitAltitude);
    //qDebug() << AIXM_LOWER_LIMIT_ALTITUDE << lowerLimitAltitude;

    QString codeVerticalReferenceLower = AixmHelper::parseString(AIXM_LOWER_LIMIT_REFERENCE, element);
    if(codeVerticalReferenceLower == ""){
        codeVerticalReferenceLower = "OTHER";
    }
    segmentLeg->setLowerLimitReference(CodeVerticalReference::_from_string(codeVerticalReferenceLower.toStdString().c_str()));
    //qDebug() << AIXM_LOWER_LIMIT_REFERENCE << codeVerticalReferenceLower;

    QString codeAltitudeUseAltitude = AixmHelper::parseString(AIXM_ALTITUDE_INTERPRETATION, element);
    if(codeAltitudeUseAltitude.isEmpty()){
        codeAltitudeUseAltitude = "OTHER";
    }
    segmentLeg->setAltitudeInterpretation(CodeAltitudeUse::_from_string(codeAltitudeUseAltitude.toStdString().c_str()));
    //qDebug() << AIXM_ALTITUDE_INTERPRETATION << codeAltitudeUseAltitude;

    QString altitudeOverrideATC = AixmHelper::parseString(AIXM_ALTITUDE_OVERRIDE_ATC, element);
    segmentLeg->setAltitudeOverrideATC(altitudeOverrideATC);
    //qDebug() << AIXM_ALTITUDE_OVERRIDE_ATC << altitudeOverrideATC;

    QString codeVerticalReferenceOverride = AixmHelper::parseString(AIXM_ALTITUDE_OVERRIDE_REFERENCE, element);
    if(codeVerticalReferenceOverride == ""){
        codeVerticalReferenceOverride = "OTHER";
    }
    segmentLeg->setAltitudeOverrideReference(CodeVerticalReference::_from_string(codeVerticalReferenceOverride.toStdString().c_str()));
    //qDebug() << AIXM_ALTITUDE_OVERRIDE_REFERENCE << codeVerticalReferenceOverride;

    double verticalAngle = AixmHelper::parseDouble(AIXM_VERTICAL_ANGLE, element);
    segmentLeg->setVerticalAngle(verticalAngle);
    //qDebug() << AIXM_VERTICAL_ANGLE << verticalAngle;

    QDomNode startPointNode = AixmHelper::parseNode(AIXM_START_POINT, element);
    QDomNode endPointNode = AixmHelper::parseNode(AIXM_END_POINT, element);
    QDomNode arcPointNode = AixmHelper::parseNode(AIXM_ARC_CENTRE, element);
    QDomNode trajectoryNode = AixmHelper::parseNode(AIXM_TRAJECTORY, element);

    if(startPointNode.toElement().tagName() != element.tagName()){
        TerminalSegmentPoint* startPoint = mapTerminalSegmentPoint(startPointNode);
        segmentLeg->setStartPoint(startPoint);
    }
    if(endPointNode.toElement().tagName() != element.tagName()){
        TerminalSegmentPoint* endPoint = mapTerminalSegmentPoint(endPointNode);
        segmentLeg->setEndPoint(endPoint);
    }
    if(arcPointNode.toElement().tagName() != element.tagName()){
        TerminalSegmentPoint* arcPoint = mapTerminalSegmentPoint(arcPointNode);
        segmentLeg->setArcCentre(arcPoint);
    }
    if(trajectoryNode.toElement().tagName() != element.tagName()){
        QStringList coords = trajectoryNode.toElement().text().split(" ", QString::SplitBehavior::SkipEmptyParts);
        QVector<QPointF> vector;
        QPointF point;
        for(int i = 0; i < coords.size(); i+=2){
            point.setX(coords.at(i).toFloat());
            point.setY(coords.at(i + 1).toFloat());
            vector.append(point);
        }
        segmentLeg->setCoords(vector);
    }
}

void MapperFromXml::fillSpecificSegmentLegData(const QDomNode &node, SegmentLeg *segmentLeg){

}

DesignatedPoint* MapperFromXml::mapDesignatedPoint(const QDomNode &node){
    QString designatedPointName = node.localName();
    QString uuid = AixmHelper::parseString(GML_IDENTIFIER, node.toElement());
    QDomNode timeSlice = AixmHelper::parseNode(AIXM_DESIGNATED_POINT_TIME_SLICE, node.toElement());
    QDomElement timeSliceElement = timeSlice.toElement();
    QString designator = AixmHelper::parseString(AIXM_DESIGNATIOR, timeSliceElement);
    QString strType = AixmHelper::parseString(AIXM_TYPE, timeSliceElement);
    QString name = AixmHelper::parseString(AIXM_NAME, timeSliceElement);
    CodeDesignatedPoint type = CodeDesignatedPoint::_from_string(strType.toStdString().c_str());

    QString pos = AixmHelper::parseNode(GML_POS, node.toElement()).toElement().text();

    float latitude = 0.0f, longitude = 0.0f;
    QStringList latLon = pos.split(" ");
    if(!latLon.isEmpty()){
        latitude = latLon.front().toFloat();
        longitude = latLon.back().toFloat();
    }

    DesignatedPoint* designatedPoint = new DesignatedPoint();
    designatedPoint->setUuid(uuid);
    designatedPoint->setDesignator(designator);
    designatedPoint->setName(name);
    designatedPoint->setType(type);
    designatedPoint->setLatitude(latitude);
    designatedPoint->setLongitude(longitude);

    /*qDebug() << GML_IDENTIFIER << uuid;
    qDebug() << AIXM_DESIGNATIOR << designator;
    qDebug() << AIXM_TYPE << type._to_string();
    qDebug() << AIXM_NAME << name;
    qDebug() << "lat: " << latitude << "lon: " << longitude;*/

    return designatedPoint;
}

AirspaceVolume* MapperFromXml::mapAirspaceVolume(const QDomNode &node){
    AirspaceVolume* volume = new AirspaceVolume();

    QDomElement element = node.toElement();

    QString upperLimit = AixmHelper::parseString(AIXM_UPPER_LIMIT, element);
    //qDebug() << "upperLimit :" << upperLimit;
    volume->setUpperLimit(AixmHelper::altitudeMap(upperLimit));

    QString upperLimitRef = AixmHelper::parseString(AIXM_UPPER_LIMIT_REFERENCE, element);
    //qDebug() << "upperLimitRef :" << upperLimitRef;
    volume->setUpperLimitReference(CodeVerticalReference::_from_string(upperLimitRef.toStdString().c_str()));

    QString lowerLimit = AixmHelper::parseString(AIXM_LOWER_LIMIT, element);
    //qDebug() << "lowerLimit :" << lowerLimit;
    volume->setLowerLimit(AixmHelper::altitudeMap(lowerLimit));

    QString lowerLimitRef = AixmHelper::parseString(AIXM_LOWER_LIMIT_REFERENCE, element);
    //qDebug() << "lowerLimitRef :" << lowerLimitRef;
    volume->setLowerLimitReference(CodeVerticalReference::_from_string(lowerLimitRef.toStdString().c_str()));

    QVector<QPointF> horizontalProjection;
    QDomNode horizontalProjectionNode = AixmHelper::parseNode(AIXM_HORIZONTAL_PROJECTION, element);
    if(horizontalProjectionNode.localName() != node.localName()){
        QDomNodeList list = horizontalProjectionNode.toElement().elementsByTagName("pos");
        QPointF point;
        if(!list.isEmpty()){
            for(int i = 0; i < list.size(); i++){
                QStringList latLon = list.at(i).toElement().text().split(" ");
                //qDebug() << latLon;
                point.setX(latLon.front().toFloat());
                point.setY(latLon.back().toFloat());
                horizontalProjection.append(point);
            }
        } else {
            QStringList coords = horizontalProjectionNode.toElement().text().split(" ", QString::SplitBehavior::SkipEmptyParts);
            //qDebug() << coords;
            for(int i = 0; i < coords.size(); i+=2){
                point.setX(coords.at(i).toFloat());
                point.setY(coords.at(i + 1).toFloat());
                horizontalProjection.append(point);
            }
        }
        volume->setHorizontalProjection(horizontalProjection);
    }

    QVector<QPointF> centerline;
    QDomNode centerlineNode = AixmHelper::parseNode(AIXM_CENTERLINE, element);
    if(centerlineNode.localName() != node.localName()){
        QDomNodeList list = centerlineNode.toElement().elementsByTagName("pos");
        QPointF point;
        if(!list.isEmpty()){
            for(int i = 0; i < list.size(); i++){
                QStringList latLon = list.at(i).toElement().text().split(" ");
                point.setX(latLon.front().toFloat());
                point.setY(latLon.back().toFloat());
                centerline.append(point);
            }
        } else {
            QStringList coords = centerlineNode.toElement().text().split(" ", QString::SplitBehavior::SkipEmptyParts);
            for(int i = 0; i < coords.size(); i+=2){
                point.setX(coords.at(i).toFloat());
                point.setY(coords.at(i + 1).toFloat());
                centerline.append(point);
            }
        }
        volume->setCentreline(centerline);
    }

    return volume;
}

Airspace* MapperFromXml::mapAirspace(const QDomNode &node){
    Airspace* airspace = new Airspace();

    QDomElement element = node.toElement();
    QString uuid = AixmHelper::parseString(GML_IDENTIFIER, element);
    //qDebug() << "uuid :" << uuid;
    airspace->setUuid(uuid);

    QString controlType = AixmHelper::parseString(AIXM_CONTROL_TYPE, element);
    if(controlType == ""){
        controlType = "OTHER";
    }
    //qDebug() << "controlType :" << controlType;
    airspace->setControlType(CodeMilitaryOperations::_from_string(controlType.toStdString().c_str()));

    QString designator = AixmHelper::parseString(AIXM_DESIGNATIOR, element);
    //qDebug() << "designator :" << designator;
    airspace->setDesignator(designator);

    QString designatorICAO = AixmHelper::parseString(AIXM_DESGINATOR_ICAO, element);
    //qDebug() << "designatorICAO :" << designatorICAO;
    airspace->setDesignatorICAO((designatorICAO == "YES") ? true : false);

    QString localType = AixmHelper::parseString(AIXM_LOCAL_TYPE, element);
    //qDebug() << "localType :" << localType;
    airspace->setLocalType(localType);

    QString name = AixmHelper::parseString(AIXM_NAME, element);
    //qDebug() << "name :" << name;
    airspace->setName(name);

    QString type = AixmHelper::parseString(AIXM_TYPE, element);
    //qDebug() << "type :" << type;
    airspace->setType(CodeAirspace::_from_string(type.toStdString().c_str()));

    QString upperLowerSeparation = AixmHelper::parseString(AIXM_UPPER_LOWER_SEPARATION, element);
    //qDebug() << "upperLowerSeparation :" << upperLowerSeparation;
    airspace->setUpperLowerSeparation(upperLowerSeparation);

    QDomNodeList airSpaceVolumeNodes = AixmHelper::parseNodeList(
        AIXM_AIRSPACE_VOLUME,
        AixmHelper::parseNode(
            AIXM_THE_AIRSPACE_VOLUME,
            AixmHelper::parseNode(
                AIXM_AIRSPACE_GEOMETRY_COMPONENT,
                AixmHelper::parseNode(
                    AIXM_GEOMETRY_COMPONENT,
                    element
                ).toElement()
            ).toElement()
        ).toElement()
    );

    QVector<AirspaceVolume*> volumes;

    for(int i = 0; i < airSpaceVolumeNodes.size(); i++){
        AirspaceVolume* airspaceVolume = mapAirspaceVolume(airSpaceVolumeNodes.at(i));
        airspaceVolume->setAirspace(airspace);
        volumes.append(airspaceVolume);
    }

    airspace->setVolumes(volumes);

    return airspace;
}

ProcedureTransition* MapperFromXml::mapProcedureTransition(const QDomNode &node){
    ProcedureTransition* procedureTransition = new ProcedureTransition();

    QDomElement element = node.toElement();
    QString type = AixmHelper::parseString(AIXM_TYPE, element);
    //qDebug() << "type :" << type;
    procedureTransition->setType(CodeProcedurePhase::_from_string(type.toStdString().c_str()));

    QString instruction = AixmHelper::parseString(AIXM_INSTRUCTION, element);
    //qDebug() << "instruction :" << instruction;
    procedureTransition->setInstruction(instruction);

    QString transitionId = AixmHelper::parseString(AIXM_TRANSITION_ID, element);
    //qDebug() << "transitionId :" << transitionId;
    procedureTransition->setTransitionId(transitionId);

    double vectorHeading = AixmHelper::parseDouble(AIXM_VECTOR_HEADING, element);
    //qDebug() << "vectorHeading :" << vectorHeading;
    procedureTransition->setVectorHeading(vectorHeading);

    QDomNodeList transitionLegs = AixmHelper::parseNodeList(AIXM_TRANSITION_LEG, element);
    //qDebug() << "transitionLegs size :" << transitionLegs.size();

    QVector <SegmentLeg*> segments;
    DataStorage *dataSource = DataStorage::getInstance();

    for(int i = 0; i < transitionLegs.size(); i++){
        QDomElement transitionLegElement = transitionLegs.at(i).toElement();
        QDomNode theSegmentLeg = AixmHelper::parseNode(AIXM_THE_SEGMENT_LEG, transitionLegElement);
        QString segmentUuid = AixmHelper::cutUuid(theSegmentLeg.toElement().attribute("href"));
        SegmentLeg *segment = dataSource->getSegmentLeg(segmentUuid);
        if(segment != NULL){
            segments.append(segment);
        }
    }

    procedureTransition->setSegments(segments);
    return procedureTransition;
}

VerticalStructure* MapperFromXml::mapVerticalStructure(const QDomNode &node){
    VerticalStructure *obstacle = new VerticalStructure();

    QDomElement element = node.toElement();

    QString uuid = AixmHelper::parseString(GML_IDENTIFIER, element);
    //qDebug() << "Uuid :" << uuid;
    obstacle->setUuid(uuid);

    double length = AixmHelper::parseDouble(AIXM_LENGTH, element);
    //qDebug() << "Length :" << length;
    obstacle->setLength(length);

    QString lighted = AixmHelper::parseString(AIXM_LIGHTED, element);
    //qDebug() << "Lighted :" << lighted;
    obstacle->setLighted( (lighted == "YES") ? true : false);

    QString lightingICAOStandard = AixmHelper::parseString(AIXM_LIGHTING_ICAO_STANDARD, element);
    //qDebug() << "lightingICAOStandard :" << lightingICAOStandard;
    obstacle->setLightingICAOStandard( (lightingICAOStandard == "YES") ? true : false );

    QString markingICAOStandard = AixmHelper::parseString(AIXM_MARKING_ICAO_STANDARD, element);
    //qDebug() << "markingICAOStandard :" << markingICAOStandard;
    obstacle->setMarkingICAOStandard( (markingICAOStandard == "YES") ? true : false );

    QString name = AixmHelper::parseString(AIXM_NAME, element);
    //qDebug() << "name :" << name;
    obstacle->setName(name);

    double radius = AixmHelper::parseDouble(AIXM_RADIUS, element);
    //qDebug() << "radius :" << radius;
    obstacle->setRadius(radius);

    QString synchronisedLighting = AixmHelper::parseString(AIXM_SYNCHRONISED_LIGHTING, element);
    //qDebug() << "synchronisedLighting :" << synchronisedLighting;
    obstacle->setSynchronisedLighting( (synchronisedLighting == "YES") ? true : false );

    QString type = AixmHelper::parseString(AIXM_TYPE, element);
    //qDebug() << "type :" << type;
    obstacle->setType(CodeVerticalStructure::_from_string(type.toStdString().c_str()));

    double width = AixmHelper::parseDouble(AIXM_WIDTH, element);
    //qDebug() << "width :" << width;
    obstacle->setWidth(width);

    QDomNodeList verticalStructurePartNodes = AixmHelper::parseNodeList(AIXM_PART, element);

    QVector<VerticalStructurePart*> parts;
    for(int i = 0; i < verticalStructurePartNodes.size(); i++){
        VerticalStructurePart* obstaclePart = mapVerticalStructurePart(verticalStructurePartNodes.at(i));
        parts.append(obstaclePart);
    }

    obstacle->setParts(parts);

    return obstacle;
}

VerticalStructurePart* MapperFromXml::mapVerticalStructurePart(const QDomNode &node){
    VerticalStructurePart *obstaclePart = new VerticalStructurePart();

    QDomElement element = node.toElement();

    QDomNodeList isCurve = element.elementsByTagName(AIXM_ELEVATED_CURVE);
    QDomNodeList isPoint = element.elementsByTagName(AIXM_ELEVATED_POINT);
    QDomNodeList isSurface = element.elementsByTagName(AIXM_ELEVATED_SURFACE);

    QDomNodeList posList;

    if(!isCurve.isEmpty()){
        obstaclePart->setGeometryType(GeometryType::CURVE);
        posList = element.elementsByTagName(GML_POS_LIST);
    } else if (!isPoint.isEmpty()){
        obstaclePart->setGeometryType(GeometryType::POINT);
        posList = element.elementsByTagName(GML_POS);
    } else if (!isSurface.isEmpty()){
        obstaclePart->setGeometryType(GeometryType::SURFACE);
        posList = element.elementsByTagName(GML_POS_LIST);
    }

    QDomNode coordsNode = posList.at(0);

    QStringList coordslist = coordsNode.toElement().text().split(" ");

    QVector<QPointF> coords;
    for(int i = 0; i < coordslist.size(); i+=2){
        QPointF latLon;
        latLon.setX(coordslist.at(i).toFloat());
        latLon.setY(coordslist.at(i + 1).toFloat());
        coords.append(latLon);
    }

    //qDebug() << "coords :" << coords;

    obstaclePart->setCoords(coords);

    QString constructionStatus = AixmHelper::parseString(AIXM_CONSTRUCTION_STATUS, element);
    //qDebug() << "constructionStatus :" << constructionStatus;
    if(constructionStatus == ""){
        constructionStatus = "OTHER";
    }
    obstaclePart->setConstructionStatus(CodeStatusConstruction::_from_string(constructionStatus.toStdString().c_str()));

    QString designator = AixmHelper::parseString(AIXM_DESIGNATIOR, element);
    //qDebug() << "designator :" << designator;
    obstaclePart->setDesignator(designator);

    QString markingFirstColour = AixmHelper::parseString(AIXM_MARKING_FIRST_COLOUR, element);
    //qDebug() << "markingFirstColour :" << markingFirstColour;
    if(markingFirstColour == ""){
        markingFirstColour = "OTHER";
    }
    obstaclePart->setMarkingFirstColour(CodeColour::_from_string(markingFirstColour.toStdString().c_str()));

    QString markingPattern = AixmHelper::parseString(AIXM_MARKING_PATTERN, element);
    //qDebug() << "markingPattern :" << markingPattern;
    if(markingPattern == ""){
        markingPattern = "OTHER";
    }
    obstaclePart->setMarkingPattern(CodeVerticalStructureMarking::_from_string(markingPattern.toStdString().c_str()));

    QString markingSecondColour = AixmHelper::parseString(AIXM_MARKING_SECOND_COLOUR, element);
    //qDebug() << "markingSecondColour :" << markingSecondColour;
    if(markingSecondColour == ""){
        markingSecondColour = "OTHER";
    }
    obstaclePart->setMarkingSecondColour(CodeColour::_from_string(markingSecondColour.toStdString().c_str()));

    QString mobile = AixmHelper::parseString(AIXM_MOBILE, element);
    //qDebug() << "mobile :" << mobile;
    obstaclePart->setMobile( (mobile == "YES") ? true : false );

    QString type = AixmHelper::parseString(AIXM_TYPE, element);
    //qDebug() << "type :" << type;
    if(type == ""){
        type = "OTHER";
    }
    obstaclePart->setType(CodeVerticalStructure::_from_string(type.toStdString().c_str()));

    double verticalExtent = AixmHelper::parseDouble(AIXM_VERTICAL_EXTENT, element);
    //qDebug() << "verticalExtent :" << verticalExtent;
    obstaclePart->setVerticalExtent(verticalExtent);

    double verticalExtentAccuracy = AixmHelper::parseDouble(AIXM_VERTICAL_EXTENT_ACCURACY, element);
    //qDebug() << "verticalExtentAccuracy :" << verticalExtentAccuracy;
    obstaclePart->setVerticalExtentAccuracy(verticalExtentAccuracy);

    QString visibleMaterial = AixmHelper::parseString(AIXM_VISIBLE_MATERIAL, element);
    //qDebug() << "visibleMaterial :" << visibleMaterial;
    if(visibleMaterial == ""){
        visibleMaterial = "OTHER";
    }
    obstaclePart->setVisibleMaterial(CodeVerticalStructureMaterial::_from_string(visibleMaterial.toStdString().c_str()));

    return obstaclePart;
}
