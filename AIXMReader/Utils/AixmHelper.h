#ifndef AIXMHELPER_H
#define AIXMHELPER_H
#include <QDomNode>

// Утилитный класс, который позволяет извлекать значения из AIXM

class AixmHelper
{
private:
    AixmHelper();

public:
    // Метод для получения строкового значения
    static QString parseString(QString featureName, const QDomElement& parentElement);

    // Метод для получения double значения
    static double parseDouble(QString featureName, const QDomElement& parentElement);

    // Метод для получения целочисленного значения
    static int parseInteger(QString featureName, const QDomElement& parentElement);

    // Метод для получения логического значения
    static bool parseBoolean(QString featureName, const QDomElement& parentElement);

    // Метод для получения нода
    static QDomNode parseNode(QString featureName, const QDomElement& parentElement);

    // Метод для получения листа нодов
    static QDomNodeList parseNodeList(QString featureName, const QDomElement& parentElement);

    // Метод для обрезания uuid, необходим для поиска ссылок в AIXM
    static QString cutUuid(QString uuid);

    // Метод для маппинга высот
    static double altitudeMap(QString alt);
};

#endif // AIXMHELPER_H
