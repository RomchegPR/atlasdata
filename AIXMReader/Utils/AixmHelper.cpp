#include "AixmHelper.h"
#include <QStringList>
#include <QDomNodeList>
#include <QDebug>
#include "limits.h"

AixmHelper::AixmHelper()
{

}

QDomNode AixmHelper::parseNode(QString featureName, const QDomElement &parentElement){
    QDomNodeList nodes = parentElement.elementsByTagName(featureName);
    if(!nodes.isEmpty()) return nodes.at(0);
    else return parentElement.cloneNode();

}

double AixmHelper::altitudeMap(QString alt){
    double altitude = 0;
    if(alt == "GND"){
        altitude = 0;
    } else if (alt == "UNL"){
        altitude = std::numeric_limits<double>::infinity();
    } else {
        altitude = alt.toDouble();
    }
    return altitude;
}

QString AixmHelper::cutUuid(QString uuid){
    return uuid.mid(9, 36);
}

QDomNodeList AixmHelper::parseNodeList(QString featureName, const QDomElement& parentElement){
    QDomNodeList nodes = parentElement.elementsByTagName(featureName);
    /*qDebug() << nodes.size();
    for(int i = 0; i < nodes.size(); i++){
        qDebug() << nodes.at(i).localName();
    }*/
    return nodes;
}

QString AixmHelper::parseString(QString featureName, const QDomElement &parentElement){
    QString parentName = parentElement.tagName();
    QDomElement foundElement = parseNode(featureName, parentElement).toElement();
    QString foundName = foundElement.tagName();
    if(parentName == foundName){
        return "";
    }
    return foundElement.text();
}

double AixmHelper::parseDouble(QString featureName, const QDomElement &parentElement){
    QString parentName = parentElement.tagName();
    QDomElement foundElement = parseNode(featureName, parentElement).toElement();
    QString foundName = foundElement.tagName();
    if(parentName == foundName){
        return 0.0;
    }
    return foundElement.text().toDouble();
}
