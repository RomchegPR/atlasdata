#ifndef MAPPERFROMXML_H
#define MAPPERFROMXML_H
#include <AIXMReader/Reader/AIXMData.h>
#include <AIXMReader/Entities/Procedure.h>
#include "AIXMReader/Entities/Airspace.h"
#include "AIXMReader/Entities/VerticalStructure.h"

// Класс для маппинга сущностей AIXM в класс-модели

class MapperFromXml
{
private:

public:
    MapperFromXml();

    // Метод для маппинга процедуры
    Procedure* mapProcedure(const QDomNode &node);

    // Метод для маппинга геоточек
    DesignatedPoint* mapDesignatedPoint(const QDomNode &node);

    // Методя для маппинга сегментов
    SegmentLeg* mapSegmentLeg(const QDomNode &node);

    // Метод для маппинга точек сегментов
    TerminalSegmentPoint* mapTerminalSegmentPoint(const QDomNode &node);

    // Метод для маппинга пути процедуры
    ProcedureTransition* mapProcedureTransition(const QDomNode &node);

    // Метод для маппинга воздушного пространства
    Airspace* mapAirspace(const QDomNode &node);

    // Метод для маппинга объема воздушного пространства
    AirspaceVolume* mapAirspaceVolume(const QDomNode &node);

    VerticalStructure* mapVerticalStructure(const QDomNode &node);

    VerticalStructurePart* mapVerticalStructurePart(const QDomNode &node);
private:

    // Метод для заполенния специфичной информации о сегменте (ArrivalLeg, InitialLeg, FinalLeg и т.д.)
    void fillSpecificSegmentLegData(const QDomNode &node, SegmentLeg* segmentLeg);

    // Метод для заполенния базовой информации о сегменте
    void fillBaseSegmentLegData(const QDomNode &node, SegmentLeg* segmentLeg);
};

#endif // MAPPERFROMXML_H
