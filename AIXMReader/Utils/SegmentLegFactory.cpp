#include "SegmentLegFactory.h"
#include <AIXMReader/Entities/SegmentLeg.h>
#include <AIXMReader/Entities/ApproachLeg.h>
#include <AIXMReader/Entities/ArrivalFeederLeg.h>
#include <AIXMReader/Entities/ArrivalLeg.h>
#include <AIXMReader/Entities/DepartureLeg.h>
#include <AIXMReader/Entities/FinalLeg.h>
#include <AIXMReader/Entities/InitialLeg.h>
#include <AIXMReader/Entities/IntermediateLeg.h>
#include <AIXMReader/Entities/MissedApproachLeg.h>


SegmentLegFactory::SegmentLegFactory()
{

}

SegmentLeg* SegmentLegFactory::makeLeg(SegmentLegType type){
    SegmentLeg* ptr = NULL;
    if(type == SegmentLegType::ApproachLeg) {
        ptr = new ApproachLeg();
    } else if (type == SegmentLegType::ArrivalFeederLeg) {
        ptr = new ArrivalFeederLeg();
    } else if (type == SegmentLegType::DepartureLeg) {
        ptr = new DepartureLeg();
    } else if (type == SegmentLegType::FinalLeg) {
        ptr = new FinalLeg();
    } else if (type == SegmentLegType::InitialLeg) {
        ptr = new InitialLeg();
    } else if (type == SegmentLegType::IntermediateLeg) {
        ptr = new IntermediateLeg();
    } else if (type == SegmentLegType::MissedApproachLeg) {
        ptr = new MissedApproachLeg();
    } else if (type == SegmentLegType::SegmentLeg) {
        ptr = new SegmentLeg();
    } else if (type == SegmentLegType::ArrivalLeg) {
        ptr = new ArrivalLeg();
    }
    return ptr;
}
