#include "AixmApi.h"
#include "Reader/AIXMData.h"
#include <AIXMReader/Utils/MapperFromXml.h>
#include "Reader/AIXMReader.h"
#include <QDomDocument>
#include <QDebug>
#include <QStringList>
#include "DataStorage/DataStorage.h"

AixmApi* AixmApi::instance = NULL;

AixmApi* AixmApi::getInstance(){
    if(!instance){
        instance = new AixmApi();
    }
    return instance;
}

AixmApi::~AixmApi()
{
    delete instance;
}

void AixmApi::parseFile(QString fileName){
    AIXMReader reader(fileName);

    AIXMData aixm(reader.getDocument());

    // загрузка нодов из AIXM
    aixm.loadNodes();

    // Вывод информации о загруженных нодах

    qDebug() << "Count of Procedures:" << aixm.mProcedures.size();
    qDebug() << "Count of SegmentLegs:" << aixm.mSegmentLegs.size();
    qDebug() << "Count of DesignatedPoints:" << aixm.mDesignatedPoints.size();
    qDebug() << "Count of Airspaces:" << aixm.mAirspaces.size();
    qDebug() << "Count of Obstacles:" << aixm.mObstacles.size();
    qDebug() << "\n";

    MapperFromXml mapper;
    DataStorage* dataSource = DataStorage::getInstance();

    // Маппинг нодов в классы и сохранение информации в класс DataStorage

    for(QDomNode &node : aixm.mDesignatedPoints){
        DesignatedPoint* curPoint = mapper.mapDesignatedPoint(node);
        dataSource->addDesignatedPoint(curPoint->getUuid(), curPoint);
    }

    for(QDomNode &node : aixm.mSegmentLegs){
        SegmentLeg* segment = mapper.mapSegmentLeg(node);
        dataSource->addSegmentLeg(segment->getUuid(), segment);
    }

    for(QDomNode &node : aixm.mProcedures){
        Procedure* procedure = mapper.mapProcedure(node);
        dataSource->addProcedure(procedure->getUuid(), procedure);
    }

    for(QDomNode &node : aixm.mAirspaces){
        Airspace* airspace = mapper.mapAirspace(node);
        dataSource->addAirspace(airspace->getUuid(), airspace);
    }

    for(QDomNode &node : aixm.mObstacles){
        VerticalStructure* obstacle = mapper.mapVerticalStructure(node);
        dataSource->addObstacle(obstacle->getUuid(), obstacle);
    }
}

//QVector<Procedure*> AixmApi::getAllProcedures(){
//    DataStorage *datasource = DataStorage::getInstance();
////    QVector< Procedure* >& allprocedures = datasource->getProcedures();
////    QVector<Procedure*> vector;
////    for(Procedure* procedure: allprocedures){
////        vector.append(procedure);
////    }
//    return datasource->getProcedures();
//}

//BaseEntity* AixmApi::getProcedureByUuid(QString uuid){
//    DataStorage *dataSource = DataStorage::getInstance();
//    if(dataSource->getAllObjects().contains(uuid))
//    {
//        return dataSource->getObject(uuid);
//    }
//}

//QVector<Procedure*> AixmApi::getProceduresByType(ProcedureType type){
//    QVector<Procedure*> allProcedures = getAllProcedures();
//    QVector<Procedure*> filteredProcedures;
//    for(Procedure *procedure : allProcedures){
//        if(procedure->getType() == type){
//            filteredProcedures.append(procedure);
//        }
//    }
//    return filteredProcedures;
//}

//Procedure* AixmApi::getProcedureByName(QString name){
//    QVector<Procedure*> allProcedures = getAllProcedures();
//    for(Procedure *procedure : allProcedures){
//        if(procedure->getName() == name){
//            return procedure;
//        }
//    }
//    return NULL;
//}

void AixmApi::addProcedure(Procedure *procedure){
    DataStorage *datasource = DataStorage::getInstance();
    datasource->addProcedure(procedure->getUuid(), procedure);
}

//QVector<Airspace*> AixmApi::getAllAirspaces(){
//    DataStorage *datasource = DataStorage::getInstance();

//    QVector < Airspace* >& allAirspaces = datasource->getAirspaces();
//    QVector<Airspace*> vector;
//    for(Airspace * airspace : allAirspaces){
//        vector.append(airspace);
//    }
//    return vector;
//}

//BaseEntity* AixmApi::getAirspaceByUuid(QString uuid){
//    DataStorage *datasource = DataStorage::getInstance();
//    return datasource->getObject(uuid);
//}

//QVector<Airspace*> AixmApi::getAirspacesByType(CodeAirspace type){
//    QVector<Airspace*> allAirspaces = getAllAirspaces();
//    QVector<Airspace*> filteredAirspaces;
//    for(Airspace* airspace : allAirspaces){
//        if(airspace->getType() == type){
//            filteredAirspaces.append(airspace);
//        }
//    }
//    return filteredAirspaces;
//}

//Airspace* AixmApi::getAirspaceByName(QString name){
//    QVector<Airspace*> allAirspaces = getAllAirspaces();
//    for(Airspace* airspace : allAirspaces){
//        if(airspace->getName() == name){
//            return airspace;
//        }
//    }
//    return NULL;
//}

void AixmApi::addAirspace(Airspace *airspace){
    DataStorage *datasource = DataStorage::getInstance();
    datasource->addAirspace(airspace->getUuid(), airspace);
}

//QVector<VerticalStructure*> AixmApi::getAllObstacles(){
//    DataStorage *datasource = DataStorage::getInstance();
////    QVector<VerticalStructure*>& allObstacles = datasource->getObstacles();
//    QVector<VerticalStructure*> vector = datasource->getObstacles();
////    for(VerticalStructure * obstacle : allObstacles){
////        vector.append(obstacle);
////    }
//    return vector;
//}

//BaseEntity* AixmApi::getObstacleByUuid(QString uuid){
//    DataStorage *datasource = DataStorage::getInstance();
//    return datasource->getObject(uuid);
//}

//QVector<VerticalStructure*> AixmApi::getObstaclesByType(CodeVerticalStructure type){
//    QVector<VerticalStructure*> allObstacles = getAllObstacles();
//    QVector<VerticalStructure*> filteredObstacles;
//    for(VerticalStructure *obstacle : allObstacles){
//        if(obstacle->getType() == type){
//            filteredObstacles.append(obstacle);
//        }
//    }
//    return filteredObstacles;
//}

//VerticalStructure* AixmApi::getObstacleByName(QString name){
//    QVector<VerticalStructure*> allObstacles = getAllObstacles();
//    QVector<VerticalStructure*> filteredObstacles;
//    for(VerticalStructure *obstacle : allObstacles){
//        if(obstacle->getName() == name){
//            return obstacle;
//        }
//    }
//    return NULL;
//}

void AixmApi::addObstacle(VerticalStructure *obstacle){
    DataStorage *dataSource = DataStorage::getInstance();
    dataSource->addObstacle(obstacle->getUuid(), obstacle);
}

//QVector<DesignatedPoint*> AixmApi::getAllPoints(){
//    DataStorage *dataSource = DataStorage::getInstance();
//    QVector<DesignatedPoint *>& allPoints = dataSource->getDesignatedPoints();
//    QVector<DesignatedPoint *> vector;
//    for(DesignatedPoint *point : allPoints){
//        vector.append(vector);
//    }
//    return vector;

//}

//BaseEntity* AixmApi::getPointByUuid(QString uuid){
//    DataStorage *dataSource = DataStorage::getInstance();
//    return dataSource->getObject(uuid);
//}

//DesignatedPoint* AixmApi::getPointByName(QString name){
//    QVector<DesignatedPoint*> allPoints = getAllPoints();
//    for(DesignatedPoint *point : allPoints){
//        if(point->getName() == name){
//            return point;
//        }
//    }
//    return NULL;
//}

void AixmApi::addPoint(DesignatedPoint *point){
    DataStorage *dataSource = DataStorage::getInstance();
    dataSource->addDesignatedPoint(point->getUuid(), point);
}

