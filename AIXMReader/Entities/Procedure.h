#ifndef PROCEDURE_H
#define PROCEDURE_H
#include <QString>
#include <AIXMReader/Entities/ProcedureTransition.h>
#include <AIXMReader/Entities/BaseEntity.h>
#include <AIXMReader/Entities/AircraftCharacteristic.h>
#include <AIXMReader/Entities/TerminalSegmentPoint.h>
#include <QVector>
#include <AIXMReader/DataTypes/CodeDesignStandard.h>
#include <AIXMReader/DataTypes/CodeProcedureCodingStandard.h>
#include <AIXMReader/DataTypes/ProcedureType.h>

// Класс, описывающий процедуру

class Procedure : public BaseEntity
{
protected:
    QString communicationFailureInstruction;
    QString instruction;
    CodeDesignStandard designCriteria;
    CodeProcedureCodingStandard codingStandard;
    bool flightChecked;
    QString name;
    bool RNAV;
    ProcedureType type;
    QVector < ProcedureTransition* > flightTransition;
public:
    Procedure();
    Procedure(QString uuid);
    virtual ~Procedure();

    QString getCommunicationFailureInstruction() const;
    void setCommunicationFailureInstruction(const QString &value);

    ProcedureType getType() const;
    void setType(const ProcedureType &type);

    QString getInstruction() const;
    void setInstruction(const QString &value);

    CodeDesignStandard getDesignCriteria() const;
    void setDesignCriteria(const CodeDesignStandard &value);

    CodeProcedureCodingStandard getCodingStandard() const;
    void setCodingStandard(const CodeProcedureCodingStandard &value);

    bool getFlightChecked() const;
    void setFlightChecked(bool value);

    QString getName() const;
    void setName(const QString &value);

    bool getRNAV() const;
    void setRNAV(bool value);

    QVector<ProcedureTransition *> getFlightTransition() const;
    void setFlightTransition(const QVector<ProcedureTransition *> &value);

private:
    void init(
            QString communicationFailureInstruction = "",
            QString instruction = "",
            CodeDesignStandard designCriteria = CodeDesignStandard::OTHER,
            CodeProcedureCodingStandard codingStandard = CodeProcedureCodingStandard::OTHER,
            bool flightChecked = false,
            QString name = "",
            bool RNAV = false
            );
};

#endif // PROCEDURE_H
