#include "TerminalSegmentPoint.h"

TerminalSegmentPoint::TerminalSegmentPoint() : BaseEntity()
{
    init();
}

TerminalSegmentPoint::TerminalSegmentPoint(QString uuid) : BaseEntity(uuid)
{
    init();
}

TerminalSegmentPoint::~TerminalSegmentPoint()
{
    delete fixDesignatedPoint;
}

BaseEntity *TerminalSegmentPoint::getFixDesignatedPoint() const
{
    return fixDesignatedPoint;
}

void TerminalSegmentPoint::setFixDesignatedPoint(BaseEntity *value)
{
    fixDesignatedPoint = value;
}

bool TerminalSegmentPoint::getRadarGuidance() const
{
    return radarGuidance;
}

void TerminalSegmentPoint::setRadarGuidance(bool value)
{
    radarGuidance = value;
}

bool TerminalSegmentPoint::getWaypoint() const
{
    return waypoint;
}

void TerminalSegmentPoint::setWaypoint(bool value)
{
    waypoint = value;
}

bool TerminalSegmentPoint::getFlyOver() const
{
    return flyOver;
}

void TerminalSegmentPoint::setFlyOver(bool value)
{
    flyOver = value;
}

CodeATCReporting TerminalSegmentPoint::getReportingATC() const
{
    return reportingATC;
}

void TerminalSegmentPoint::setReportingATC(const CodeATCReporting &value)
{
    reportingATC = value;
}

bool TerminalSegmentPoint::getIndicatorFACF() const
{
    return indicatorFACF;
}

void TerminalSegmentPoint::setIndicatorFACF(bool value)
{
    indicatorFACF = value;
}

double TerminalSegmentPoint::getLeadDME() const
{
    return leadDME;
}

void TerminalSegmentPoint::setLeadDME(double value)
{
    leadDME = value;
}

double TerminalSegmentPoint::getLeadRadial() const
{
    return leadRadial;
}

void TerminalSegmentPoint::setLeadRadial(double value)
{
    leadRadial = value;
}

CodeProcedureFixRole TerminalSegmentPoint::getRole() const
{
    return role;
}

void TerminalSegmentPoint::setRole(const CodeProcedureFixRole &value)
{
    role = value;
}

void TerminalSegmentPoint::init(CodeProcedureFixRole role, double leadRadial, double leadDME, bool indicatorFACF, CodeATCReporting reportingATC,
                           bool flyOver, bool waypoint, bool radarGuidance, DesignatedPoint *fixDesignatedPoint)
{
    setRole(role);
    setLeadRadial(leadRadial);
    setLeadDME(leadDME);
    setIndicatorFACF(indicatorFACF);
    setReportingATC(reportingATC);
    setFlyOver(flyOver);
    setWaypoint(waypoint);
    setRadarGuidance(radarGuidance);
    setFixDesignatedPoint(fixDesignatedPoint);
}
