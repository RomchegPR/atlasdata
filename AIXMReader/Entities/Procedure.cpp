#include "AIXMReader/Entities/Procedure.h"

Procedure::Procedure() : BaseEntity()
{
    init();
}

Procedure::Procedure(QString uuid) : BaseEntity(uuid)
{
    init();
}

Procedure::~Procedure()
{
    for(int i = 0; i < flightTransition.size(); i++){
        delete flightTransition[i];
    }
}

void Procedure::init(QString communicationFailureInstruction, QString instruction, CodeDesignStandard designCriteria,
                 CodeProcedureCodingStandard codingStandard, bool flightChecked, QString name, bool RNAV)
{
    setCommunicationFailureInstruction(communicationFailureInstruction);
    setInstruction(instruction);
    setDesignCriteria(designCriteria);
    setCodingStandard(codingStandard);
    setFlightChecked(flightChecked);
    setName(name);
    setRNAV(RNAV);
}

QVector<ProcedureTransition *> Procedure::getFlightTransition() const
{
    return flightTransition;
}

void Procedure::setFlightTransition(const QVector<ProcedureTransition *> &value)
{
    flightTransition = value;
}

bool Procedure::getRNAV() const
{
    return RNAV;
}

void Procedure::setRNAV(bool value)
{
    RNAV = value;
}

QString Procedure::getName() const
{
    return name;
}

void Procedure::setName(const QString &value)
{
    name = value;
}

bool Procedure::getFlightChecked() const
{
    return flightChecked;
}

void Procedure::setFlightChecked(bool value)
{
    flightChecked = value;
}

CodeProcedureCodingStandard Procedure::getCodingStandard() const
{
    return codingStandard;
}

void Procedure::setCodingStandard(const CodeProcedureCodingStandard &value)
{
    codingStandard = value;
}

CodeDesignStandard Procedure::getDesignCriteria() const
{
    return designCriteria;
}

void Procedure::setDesignCriteria(const CodeDesignStandard &value)
{
    designCriteria = value;
}

ProcedureType Procedure::getType() const
{
    return type;
}

void Procedure::setType(const ProcedureType &type)
{
    this->type = type;
}

QString Procedure::getInstruction() const
{
    return instruction;
}

void Procedure::setInstruction(const QString &value)
{
    instruction = value;
}

QString Procedure::getCommunicationFailureInstruction() const
{
    return communicationFailureInstruction;
}

void Procedure::setCommunicationFailureInstruction(const QString &value)
{
    communicationFailureInstruction = value;
}

