#ifndef PROCEDURETRANSITION_H
#define PROCEDURETRANSITION_H
#include <QString>
#include <AIXMReader/Entities/SegmentLeg.h>
#include <AIXMReader/Entities/BaseEntity.h>
#include <QVector>
#include <AIXMReader/DataTypes/CodeProcedurePhase.h>

// Класс, описывающий путь/геометрию процедуры

class ProcedureTransition : public BaseEntity
{
private:
    QString transitionId;
    CodeProcedurePhase type;
    QString instruction;
    double vectorHeading; // min 0, max 360
    QVector<SegmentLeg* > segments;
public:
    ProcedureTransition();
    ProcedureTransition(QString uuid);
    virtual ~ProcedureTransition();

    QString getTransitionId() const;
    void setTransitionId(const QString &value);

    CodeProcedurePhase getType() const;
    void setType(const CodeProcedurePhase &value);

    QString getInstruction() const;
    void setInstruction(const QString &value);

    double getVectorHeading() const;
    void setVectorHeading(double value);

    QVector<SegmentLeg *> getSegments() const;
    void setSegments(const QVector<SegmentLeg *> &value);

private:
    void init(
            QString transitionId = "",
            CodeProcedurePhase type = CodeProcedurePhase::OTHER,
            QString instruction = "",
            double vectorHeading = INF
            );
};

#endif // PROCEDURETRANSITION_H
