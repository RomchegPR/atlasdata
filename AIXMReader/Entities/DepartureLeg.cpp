#include "AIXMReader/Entities/DepartureLeg.h"

DepartureLeg::DepartureLeg() : SegmentLeg()
{
    init();
}

DepartureLeg::DepartureLeg(QString uuid) : SegmentLeg(uuid)
{
    init();
}

DepartureLeg::~DepartureLeg()
{
}

StandardInstrumentDeparture *DepartureLeg::getSID() const
{
    return SID;
}

void DepartureLeg::setSID(StandardInstrumentDeparture *value)
{
    SID = value;
}

double DepartureLeg::getMinimumObstacleClearanceAltitude() const
{
    return minimumObstacleClearanceAltitude;
}

void DepartureLeg::setMinimumObstacleClearanceAltitude(double value)
{
    minimumObstacleClearanceAltitude = value;
}

QString DepartureLeg::getRequiredNavigationPerformance() const
{
    return requiredNavigationPerformance;
}

void DepartureLeg::setRequiredNavigationPerformance(const QString &value)
{
    requiredNavigationPerformance = value;
}

void DepartureLeg::init(QString requiredNavigationPerformance, double minimumObstacleClearanceAltitude, StandardInstrumentDeparture *SID)
{
    setRequiredNavigationPerformance(requiredNavigationPerformance);
    setMinimumObstacleClearanceAltitude(minimumObstacleClearanceAltitude);
    setSID(SID);
}
