#ifndef TERMINALSEGMENTPOINT_H
#define TERMINALSEGMENTPOINT_H

#include <AIXMReader/Entities/BaseEntity.h>
#include <AIXMReader/Entities/DesignatedPoint.h>
#include <AIXMReader/DataTypes/CodeProcedureFixRole.h>
#include <AIXMReader/DataTypes/CodeATCReporting.h>

// Класс, описывающий точку сегмента процедуры

class TerminalSegmentPoint : public BaseEntity
{
private:
    CodeProcedureFixRole role;
    double leadRadial;
    double leadDME;
    bool indicatorFACF;
    CodeATCReporting reportingATC;
    bool flyOver;
    bool waypoint;
    bool radarGuidance;
    BaseEntity* fixDesignatedPoint;
public:
    TerminalSegmentPoint();
    TerminalSegmentPoint(QString uuid);
    virtual ~TerminalSegmentPoint();

    CodeProcedureFixRole getRole() const;
    void setRole(const CodeProcedureFixRole &value);

    double getLeadRadial() const;
    void setLeadRadial(double value);

    double getLeadDME() const;
    void setLeadDME(double value);

    bool getIndicatorFACF() const;
    void setIndicatorFACF(bool value);

    CodeATCReporting getReportingATC() const;
    void setReportingATC(const CodeATCReporting &value);

    bool getFlyOver() const;
    void setFlyOver(bool value);

    bool getWaypoint() const;
    void setWaypoint(bool value);

    bool getRadarGuidance() const;
    void setRadarGuidance(bool value);

    BaseEntity *getFixDesignatedPoint() const;
    void setFixDesignatedPoint(BaseEntity *value);

private:
    void init(
            CodeProcedureFixRole role = CodeProcedureFixRole::OTHER,
            double leadRadial = INF,
            double leadDME = INF,
            bool indicatorFACF = false,
            CodeATCReporting reportingATC = CodeATCReporting::OTHER,
            bool flyOver = false,
            bool waypoint = false,
            bool radarGuidance = false,
            DesignatedPoint* fixDesignatedPoint = NULL
            );
};

#endif // TERMINALSEGMENTPOINT_H
