#ifndef ARRIVALLEG_H
#define ARRIVALLEG_H

#include <AIXMReader/Entities/SegmentLeg.h>
#include <AIXMReader/Entities/StandardInstrumentArrival.h>

// Сегмент STAR'a

class ArrivalLeg : public SegmentLeg
{
private:
    QString requiredNavigationPerformance;
    StandardInstrumentArrival* STAR;
public:
    ArrivalLeg();
    ArrivalLeg(QString uuid);
    virtual ~ArrivalLeg();

    QString getRequiredNavigationPerformance() const;
    void setRequiredNavigationPerformance(const QString &value);

    StandardInstrumentArrival *getSTAR() const;
    void setSTAR(StandardInstrumentArrival *value);

private:
    void init(QString requiredNavigationPerformance = "", StandardInstrumentArrival *star = NULL);
};

#endif // ARRIVALLEG_H
