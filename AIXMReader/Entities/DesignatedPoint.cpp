#include "DesignatedPoint.h"

QString DesignatedPoint::getDesignator() const
{
    return designator;
}

void DesignatedPoint::setDesignator(const QString &value)
{
    designator = value;
}

DesignatedPoint::DesignatedPoint() : BaseEntity()
{
    init();
}

DesignatedPoint::DesignatedPoint(QString uuid) : BaseEntity(uuid)
{
    init();
}

DesignatedPoint::~DesignatedPoint(){

}

QPointF DesignatedPoint::getPoint() const
{
    return point;
}

void DesignatedPoint::setPoint(const QPointF &value)
{
    point = value;
}

float DesignatedPoint::latitude()
{
    return point.x();
}

float DesignatedPoint::longitude()
{
    return point.y();
}

void DesignatedPoint::setLatitude(float latitude)
{
    point.setX(latitude);
}

void DesignatedPoint::setLongitude(float longitude)
{
    point.setY(longitude);
}

QString DesignatedPoint::getName()
{
    return name;
}

void DesignatedPoint::setName(const QString& name)
{
    this->name = name;
}

CodeDesignatedPoint DesignatedPoint::getType()
{
    return type;
}

void DesignatedPoint::setType(CodeDesignatedPoint type)
{
    this->type = type;
}

void DesignatedPoint::init(CodeDesignatedPoint type, QString name, QPointF point)
{
    setType(type);
    setName(name);
    setPoint(point);
}
