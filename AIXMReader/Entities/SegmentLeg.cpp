#include "AIXMReader/Entities/SegmentLeg.h"

SegmentLegType SegmentLeg::getSegmentLegType() const
{
    return segmentLegType;
}

void SegmentLeg::setSegmentLegType(const SegmentLegType &value)
{
    segmentLegType = value;
}

SegmentLeg::SegmentLeg() : BaseEntity()
{
    init();
}

SegmentLeg::SegmentLeg(QString uuid) : BaseEntity(uuid)
{
    init();
}

SegmentLeg::~SegmentLeg()
{

}

QVector<QPointF> SegmentLeg::getCoords() const
{
    return coords;
}

void SegmentLeg::setCoords(const QVector<QPointF> &value)
{
    coords = value;
}

HoldingPattern *SegmentLeg::getHolding() const
{
    return holding;
}

void SegmentLeg::setHolding(HoldingPattern *value)
{
    holding = value;
}

QVector<AircraftCharacteristic *> SegmentLeg::getAircraftCategory() const
{
    return aircraftCategory;
}

void SegmentLeg::setAircraftCategory(const QVector<AircraftCharacteristic *> &value)
{
    aircraftCategory = value;
}

DistanceIndication *SegmentLeg::getDistance() const
{
    return distance;
}

void SegmentLeg::setDistance(DistanceIndication *value)
{
    distance = value;
}

AngleIndication *SegmentLeg::getAngle() const
{
    return angle;
}

void SegmentLeg::setAngle(AngleIndication *value)
{
    angle = value;
}

TerminalSegmentPoint *SegmentLeg::getArcCentre() const
{
    return arcCentre;
}

void SegmentLeg::setArcCentre(TerminalSegmentPoint *value)
{
    arcCentre = value;
}

TerminalSegmentPoint *SegmentLeg::getEndPoint() const
{
    return endPoint;
}

void SegmentLeg::setEndPoint(TerminalSegmentPoint *value)
{
    endPoint = value;
}

TerminalSegmentPoint *SegmentLeg::getStartPoint() const
{
    return startPoint;
}

void SegmentLeg::setStartPoint(TerminalSegmentPoint *value)
{
    startPoint = value;
}

double SegmentLeg::getVerticalAngle() const
{
    return verticalAngle;
}

void SegmentLeg::setVerticalAngle(double value)
{
    verticalAngle = value;
}

CodeVerticalReference SegmentLeg::getAltitudeOverrideReference() const
{
    return altitudeOverrideReference;
}

void SegmentLeg::setAltitudeOverrideReference(const CodeVerticalReference &value)
{
    altitudeOverrideReference = value;
}

QString SegmentLeg::getAltitudeOverrideATC() const
{
    return altitudeOverrideATC;
}

void SegmentLeg::setAltitudeOverrideATC(const QString &value)
{
    altitudeOverrideATC = value;
}

CodeAltitudeUse SegmentLeg::getAltitudeInterpretation() const
{
    return altitudeInterpretation;
}

void SegmentLeg::setAltitudeInterpretation(const CodeAltitudeUse &value)
{
    altitudeInterpretation = value;
}

CodeVerticalReference SegmentLeg::getLowerLimitReference() const
{
    return lowerLimitReference;
}

void SegmentLeg::setLowerLimitReference(const CodeVerticalReference &value)
{
    lowerLimitReference = value;
}

QString SegmentLeg::getLowerLimitAltitude() const
{
    return lowerLimitAltitude;
}

void SegmentLeg::setLowerLimitAltitude(const QString &value)
{
    lowerLimitAltitude = value;
}

CodeVerticalReference SegmentLeg::getUpperLimitReference() const
{
    return upperLimitReference;
}

void SegmentLeg::setUpperLimitReference(const CodeVerticalReference &value)
{
    upperLimitReference = value;
}

QString SegmentLeg::getUpperLimitAltitude() const
{
    return upperLimitAltitude;
}

void SegmentLeg::setUpperLimitAltitude(const QString &value)
{
    upperLimitAltitude = value;
}

bool SegmentLeg::getProcedureTurnRequired() const
{
    return procedureTurnRequired;
}

void SegmentLeg::setProcedureTurnRequired(bool value)
{
    procedureTurnRequired = value;
}

double SegmentLeg::getDuration() const
{
    return duration;
}

void SegmentLeg::setDuration(double value)
{
    duration = value;
}

double SegmentLeg::getLength() const
{
    return length;
}

void SegmentLeg::setLength(double value)
{
    length = value;
}

double SegmentLeg::getBankAngle() const
{
    return bankAngle;
}

void SegmentLeg::setBankAngle(double value)
{
    bankAngle = value;
}

CodeAltitudeUse SegmentLeg::getSpeedIntrpretation() const
{
    return speedIntrpretation;
}

void SegmentLeg::setSpeedIntrpretation(const CodeAltitudeUse &value)
{
    speedIntrpretation = value;
}

CodeSpeedReference SegmentLeg::getSpeedReference() const
{
    return speedReference;
}

void SegmentLeg::setSpeedReference(const CodeSpeedReference &value)
{
    speedReference = value;
}

double SegmentLeg::getSpeedLimit() const
{
    return speedLimit;
}

void SegmentLeg::setSpeedLimit(double value)
{
    speedLimit = value;
}

CodeDirectionTurn SegmentLeg::getTurnDirection() const
{
    return turnDirection;
}

void SegmentLeg::setTurnDirection(const CodeDirectionTurn &value)
{
    turnDirection = value;
}

CodeDirectionReference SegmentLeg::getCourseDirection() const
{
    return courseDirection;
}

void SegmentLeg::setCourseDirection(const CodeDirectionReference &value)
{
    courseDirection = value;
}

CodeCourse SegmentLeg::getCourseType() const
{
    return courseType;
}

void SegmentLeg::setCourseType(const CodeCourse &value)
{
    courseType = value;
}

double SegmentLeg::getCourse() const
{
    return course;
}

void SegmentLeg::setCourse(double value)
{
    course = value;
}

CodeSegmentPath SegmentLeg::getLegTypeARINC() const
{
    return legTypeARINC;
}

void SegmentLeg::setLegTypeARINC(const CodeSegmentPath &value)
{
    legTypeARINC = value;
}

CodeTrajectory SegmentLeg::getLegPath() const
{
    return legPath;
}

void SegmentLeg::setLegPath(const CodeTrajectory &value)
{
    legPath = value;
}

CodeSegmentTermination SegmentLeg::getEndConditionDesignator() const
{
    return endConditionDesignator;
}

void SegmentLeg::setEndConditionDesignator(const CodeSegmentTermination &value)
{
    endConditionDesignator = value;
}

void SegmentLeg::init(CodeSegmentTermination endConditionDesignator, CodeTrajectory legPath, CodeSegmentPath legTypeARINC,
                   double course, CodeCourse courseType, CodeDirectionReference courseDirection, CodeDirectionTurn turnDirection, 
                   double speedLimit, CodeSpeedReference speedReference, CodeAltitudeUse speedIntrpretation, double bankAngle, 
                   double length, double duration, bool procedureTurnRequired, QString upperLimitAltitude, CodeVerticalReference upperLimitReference, 
                   QString lowerLimitAltitude, CodeVerticalReference lowerLimitReference, CodeAltitudeUse altitudeInterpretation, QString altitudeOverrideATC, 
                   CodeVerticalReference altitudeOverrideReference, double verticalAngle, TerminalSegmentPoint *startPoint, TerminalSegmentPoint *endPoint, 
                   TerminalSegmentPoint *arcCentre, AngleIndication *angle, DistanceIndication *distance, HoldingPattern *holding)
{
    setEndConditionDesignator(endConditionDesignator);
    setLegPath(legPath);
    setLegTypeARINC(legTypeARINC);
    setCourse(course);
    setCourseType(courseType);
    setCourseDirection(courseDirection);
    setTurnDirection(turnDirection);
    setSpeedLimit(speedLimit);
    setSpeedReference(speedReference);
    setSpeedIntrpretation(speedIntrpretation);
    setBankAngle(bankAngle);
    setLength(length);
    setDuration(duration);
    setProcedureTurnRequired(procedureTurnRequired);
    setUpperLimitAltitude(upperLimitAltitude);
    setUpperLimitReference(upperLimitReference);
    setLowerLimitAltitude(lowerLimitAltitude);
    setLowerLimitReference(lowerLimitReference);
    setAltitudeInterpretation(altitudeInterpretation);
    setAltitudeOverrideATC(altitudeOverrideATC);
    setAltitudeOverrideReference(altitudeOverrideReference);
    setVerticalAngle(verticalAngle);
    setStartPoint(startPoint);
    setEndPoint(endPoint);
    setArcCentre(arcCentre);
    setAngle(angle);
    setDistance(distance);
    setHolding(holding);
}
