#ifndef INITIALLEG_H
#define INITIALLEG_H
#include <AIXMReader/Entities/ApproachLeg.h>

// Inital сегмент апроача

class InitialLeg: public ApproachLeg
{
public:
    InitialLeg();
    InitialLeg(QString uuid);
    virtual ~InitialLeg();
};

#endif // INITIALLEG_H
