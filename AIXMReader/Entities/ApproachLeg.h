#ifndef APPROACHLEG_H
#define APPROACHLEG_H

#include <AIXMReader/Entities/SegmentLeg.h>
#include <AIXMReader/Entities/InstrumentApproachProcedure.h>

// Сегмент апроача

class ApproachLeg : public SegmentLeg
{
private:
    QString requiredNavigationPerformance;
    InstrumentApproachProcedure *approach;
public:
    ApproachLeg();
    ApproachLeg(QString uuid);
    virtual ~ApproachLeg();

    QString getRequiredNavigationPerformance() const;
    void setRequiredNavigationPerformance(const QString &value);

    InstrumentApproachProcedure *getApproach() const;
    void setApproach(InstrumentApproachProcedure *value);
private:
    void init(
            QString requiredNavigationPerformance = "",
            InstrumentApproachProcedure *approach = NULL
            );
};

#endif // APPROACHLEG_H
