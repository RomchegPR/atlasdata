#include "AIXMReader/Entities/ArrivalLeg.h"

ArrivalLeg::ArrivalLeg() : SegmentLeg()
{
    init();
}


ArrivalLeg::ArrivalLeg(QString uuid) : SegmentLeg(uuid)
{
    init();
}

ArrivalLeg::~ArrivalLeg()
{
    STAR = NULL;
}

void ArrivalLeg::init(QString requiredNavigationPerformance, StandardInstrumentArrival *star)
{
    setRequiredNavigationPerformance(requiredNavigationPerformance);
    setSTAR(star);
}

StandardInstrumentArrival *ArrivalLeg::getSTAR() const
{
    return STAR;
}

void ArrivalLeg::setSTAR(StandardInstrumentArrival *value)
{
    STAR = value;
}

QString ArrivalLeg::getRequiredNavigationPerformance() const
{
    return requiredNavigationPerformance;
}

void ArrivalLeg::setRequiredNavigationPerformance(const QString &value)
{
    requiredNavigationPerformance = value;
}
