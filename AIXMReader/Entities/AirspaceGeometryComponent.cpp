#include "AirspaceGeometryComponent.h"

AirspaceGeometryComponent::AirspaceGeometryComponent()
{

}

CodeAirspaceAggregation AirspaceGeometryComponent::getOperation() const
{
    return operation;
}

void AirspaceGeometryComponent::setOperation(const CodeAirspaceAggregation &value)
{
    operation = value;
}

int AirspaceGeometryComponent::getOperationSeq() const
{
    return operationSeq;
}

void AirspaceGeometryComponent::setOperationSeq(int value)
{
    operationSeq = value;
}
