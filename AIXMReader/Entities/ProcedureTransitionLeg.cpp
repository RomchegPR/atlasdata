#include "AIXMReader/Entities/ProcedureTransitionLeg.h"

ProcedureTransitionLeg::ProcedureTransitionLeg() : BaseEntity()
{
    init();
}

ProcedureTransitionLeg::ProcedureTransitionLeg(QString uuid) : BaseEntity(uuid)
{
    init();
}

ProcedureTransitionLeg::~ProcedureTransitionLeg()
{

}

int ProcedureTransitionLeg::getSeqNumberARINC() const
{
    return seqNumberARINC;
}

void ProcedureTransitionLeg::setSeqNumberARINC(int value)
{
    seqNumberARINC = value;
}

void ProcedureTransitionLeg::init(int seqNumberARINC)
{
    setSeqNumberARINC(seqNumberARINC);
}
