#ifndef STANDARDINSTRUMENTARRIVAL_H
#define STANDARDINSTRUMENTARRIVAL_H
#include <AIXMReader/Entities/Procedure.h>
#include <QString>

// STAR

class StandardInstrumentArrival : public Procedure
{
private:
    QString designator;
public:
    StandardInstrumentArrival();
    StandardInstrumentArrival(QString uuid);
    virtual ~StandardInstrumentArrival();
    QString getDesignator() const;
    void setDesignator(const QString &value);
private:
    void init(QString designator = "");
};

#endif // STANDARDINSTRUMENTARRIVAL_H
