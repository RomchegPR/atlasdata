#ifndef INTERMEDIATELEG_H
#define INTERMEDIATELEG_H
#include <AIXMReader/Entities/ApproachLeg.h>

// Intremediate сегмент апроача

class IntermediateLeg : public ApproachLeg
{
public:
    IntermediateLeg();
    IntermediateLeg(QString uuid);
    virtual ~IntermediateLeg();
};

#endif // INTERMEDIATELEG_H
