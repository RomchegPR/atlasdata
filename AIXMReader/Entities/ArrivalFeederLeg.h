#ifndef ARRIVALFEEDERLEG_H
#define ARRIVALFEEDERLEG_H
#include <AIXMReader/Entities/ApproachLeg.h>

// Сегмент апроача

class ArrivalFeederLeg : public ApproachLeg
{
private:
public:
    ArrivalFeederLeg();
    ArrivalFeederLeg(QString uuid);
    ArrivalFeederLeg(QString uuid, InstrumentApproachProcedure* approach);
    virtual ~ArrivalFeederLeg();
};

#endif // ARRIVALFEEDERLEG_H
