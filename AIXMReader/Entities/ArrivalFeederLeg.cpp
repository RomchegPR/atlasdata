#include "ArrivalFeederLeg.h"

ArrivalFeederLeg::ArrivalFeederLeg() : ApproachLeg()
{

}

ArrivalFeederLeg::ArrivalFeederLeg(QString uuid) : ApproachLeg(uuid)
{

}

ArrivalFeederLeg::ArrivalFeederLeg(QString uuid, InstrumentApproachProcedure *approach) : ApproachLeg(uuid)
{
    setApproach(approach);
}

ArrivalFeederLeg::~ArrivalFeederLeg()
{

}
