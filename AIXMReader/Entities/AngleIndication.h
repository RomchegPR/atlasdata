#ifndef ANGLEINDICATION_H
#define ANGLEINDICATION_H

#include <AIXMReader/Entities/BaseEntity.h>
#include <AIXMReader/DataTypes/CodeDirectionReference.h>
#include <AIXMReader/DataTypes/CodeBearing.h>
#include <AIXMReader/DataTypes/CodeCardinalDirection.h>

// Неиспользуемый класс

class AngleIndication : public BaseEntity
{
private:
    double angle;
    CodeBearing angleType;
    CodeDirectionReference indicationDirection;
    double trueAngle;
    CodeCardinalDirection cardinalDirection;
    double minimumReceptionAltitude;

public:
    AngleIndication();
    AngleIndication(QString uuid);
    virtual ~AngleIndication();

    double getAngle() const;
    void setAngle(double value);

    CodeBearing getAngleType() const;
    void setAngleType(const CodeBearing &value);

    CodeDirectionReference getIndicationDirection() const;
    void setIndicationDirection(const CodeDirectionReference &value);

    double getTrueAngle() const;
    void setTrueAngle(double value);

    CodeCardinalDirection getCardinalDirection() const;
    void setCardinalDirection(const CodeCardinalDirection &value);

    double getMinimumReceptionAltitude() const;
    void setMinimumReceptionAltitude(double value);

private:
    void init(
            double angle = INF,
            CodeBearing angleType = CodeBearing::OTHER,
            CodeDirectionReference indicationDirection = CodeDirectionReference::OTHER,
            double trueAngle = INF,
            CodeCardinalDirection cardinalDirection = CodeCardinalDirection::OTHER,
            double minimumReceptionAltitude = INF
            );
};

#endif // ANGLEINDICATION_H
