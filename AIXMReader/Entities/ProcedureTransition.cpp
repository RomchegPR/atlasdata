#include "AIXMReader/Entities/ProcedureTransition.h"

ProcedureTransition::ProcedureTransition() : BaseEntity()
{
    init();
}

ProcedureTransition::ProcedureTransition(QString uuid) : BaseEntity(uuid)
{
    init();
}

ProcedureTransition::~ProcedureTransition()
{
    for(int i = 0; i < segments.size(); i++){
        delete segments[i];
    }
}

QVector<SegmentLeg *> ProcedureTransition::getSegments() const
{
    return segments;
}

void ProcedureTransition::setSegments(const QVector<SegmentLeg *> &value)
{
    segments = value;
}

double ProcedureTransition::getVectorHeading() const
{
    return vectorHeading;
}

void ProcedureTransition::setVectorHeading(double value)
{
    vectorHeading = value;
}

QString ProcedureTransition::getInstruction() const
{
    return instruction;
}

void ProcedureTransition::setInstruction(const QString &value)
{
    instruction = value;
}

CodeProcedurePhase ProcedureTransition::getType() const
{
    return type;
}

void ProcedureTransition::setType(const CodeProcedurePhase &value)
{
    type = value;
}

QString ProcedureTransition::getTransitionId() const
{
    return transitionId;
}

void ProcedureTransition::setTransitionId(const QString &value)
{
    transitionId = value;
}

void ProcedureTransition::init(QString transitionId, CodeProcedurePhase type, QString instruction, double vectorHeading)
{
    setTransitionId(transitionId);
    setType(type);
    setInstruction(instruction);
    setVectorHeading(vectorHeading);
}
