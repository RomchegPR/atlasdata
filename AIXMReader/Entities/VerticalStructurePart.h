#ifndef VERTICALSTRUCTUREPART_H
#define VERTICALSTRUCTUREPART_H

#include "AIXMReader/DataTypes/CodeStatusConstruction.h"
#include <QString>
#include "AIXMReader/DataTypes/CodeColour.h"
#include "AIXMReader/DataTypes/CodeVerticalStructureMarking.h"
#include "AIXMReader/DataTypes/CodeVerticalStructure.h"
#include "AIXMReader/DataTypes/CodeVerticalStructureMaterial.h"
#include "AIXMReader/DataTypes/GeometryType.h"
#include <QVector>
#include <QPointF>

class VerticalStructurePart
{
public:
    VerticalStructurePart();
    CodeStatusConstruction getConstructionStatus() const;
    void setConstructionStatus(const CodeStatusConstruction &value);

    QString getDesignator() const;
    void setDesignator(const QString &value);

    bool getFrangible() const;
    void setFrangible(bool value);

    CodeColour getMarkingFirstColour() const;
    void setMarkingFirstColour(const CodeColour &value);

    CodeVerticalStructureMarking getMarkingPattern() const;
    void setMarkingPattern(const CodeVerticalStructureMarking &value);

    CodeColour getMarkingSecondColour() const;
    void setMarkingSecondColour(const CodeColour &value);

    bool getMobile() const;
    void setMobile(bool value);

    CodeVerticalStructure getType() const;
    void setType(const CodeVerticalStructure &value);

    double getVerticalExtent() const;
    void setVerticalExtent(double value);

    double getVerticalExtentAccuracy() const;
    void setVerticalExtentAccuracy(double value);

    CodeVerticalStructureMaterial getVisibleMaterial() const;
    void setVisibleMaterial(const CodeVerticalStructureMaterial &value);

    GeometryType getGeometryType() const;
    void setGeometryType(const GeometryType &value);

    QVector<QPointF> getCoords() const;
    void setCoords(const QVector<QPointF> &value);

private:
    CodeStatusConstruction constructionStatus;
    QString designator;
    bool frangible;
    CodeColour markingFirstColour;
    CodeVerticalStructureMarking markingPattern;
    CodeColour markingSecondColour;
    bool mobile;
    CodeVerticalStructure type;
    double verticalExtent;
    double verticalExtentAccuracy;
    CodeVerticalStructureMaterial visibleMaterial;
    GeometryType geometryType;
    QVector<QPointF> coords;
};

#endif // VERTICALSTRUCTUREPART_H
