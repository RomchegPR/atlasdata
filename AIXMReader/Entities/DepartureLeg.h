#ifndef DEPARTURELEG_H
#define DEPARTURELEG_H

#include <AIXMReader/Entities/SegmentLeg.h>
#include <AIXMReader/Entities/StandardInstrumentDeparture.h>

// Сегмент SID'a

class DepartureLeg : public SegmentLeg
{
private:
    QString requiredNavigationPerformance; // RNP type
    double minimumObstacleClearanceAltitude;
    StandardInstrumentDeparture* SID;
public:
    DepartureLeg();
    DepartureLeg(QString uuid);
    virtual ~DepartureLeg();

    QString getRequiredNavigationPerformance() const;
    void setRequiredNavigationPerformance(const QString &value);

    double getMinimumObstacleClearanceAltitude() const;
    void setMinimumObstacleClearanceAltitude(double value);

    StandardInstrumentDeparture *getSID() const;
    void setSID(StandardInstrumentDeparture *value);

private:
    void init(
            QString requiredNavigationPerformance = "",
            double minimumObstacleClearanceAltitude = INF,
            StandardInstrumentDeparture *SID = NULL
            );
};

#endif // DEPARTURELEG_H
