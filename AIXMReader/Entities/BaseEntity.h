#ifndef BASEENTITY_H
#define BASEENTITY_H

#include <QString>
#include <limits>
#include <QUuid>
#include "DataStorage/RenderData/RenderData.h"

const int HUGE_VALUE = std::numeric_limits<int>::max();
const double INF =  std::numeric_limits<double>::infinity();

// Базовая сущность для всех фич AIXM'a

class BaseEntity
{
protected:
    QUuid       uuid;
    RenderData  mRenderData;

public:
    BaseEntity();
    BaseEntity(QString uuid);
    virtual ~BaseEntity();

    QString getUuid();
    void setUuid(QString uuid);
};

#endif // BASEENTITY_H
