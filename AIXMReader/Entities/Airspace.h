#ifndef AIXMAIRSPACE_H
#define AIXMAIRSPACE_H

#include "AIXMReader/DataTypes/CodeMilitaryOperations.h"
#include "AIXMReader/Entities/AirspaceVolume.h"
#include "AIXMReader/DataTypes/CodeAirspace.h"
#include <QString>
#include <QVector>
#include <AIXMReader/Entities/BaseEntity.h>

// Класс-модель, описывающий воздушное пространство

class Airspace : public BaseEntity
{
public:
    Airspace();
    QString getLocalType() const;
    void setLocalType(const QString &value);

    CodeMilitaryOperations getControlType() const;
    void setControlType(const CodeMilitaryOperations &value);

    QString getDesignator() const;
    void setDesignator(const QString &value);

    bool getDesignatorICAO() const;
    void setDesignatorICAO(bool value);

    QString getName() const;
    void setName(const QString &value);

    CodeAirspace getType() const;
    void setType(const CodeAirspace &value);

    QString getUpperLowerSeparation() const;
    void setUpperLowerSeparation(const QString &value);

    QVector<AirspaceVolume *> getVolumes() const;
    void setVolumes(const QVector<AirspaceVolume *> &value);

private:
    CodeMilitaryOperations controlType;
    QString designator;
    bool designatorICAO;
    QString localType;
    QString name;
    CodeAirspace type;
    QString upperLowerSeparation;

    // Воздушное пространство может состоять измножества объемов. CTR как пример
    QVector<AirspaceVolume*> volumes;
};

#endif // AIXMAIRSPACE_H
