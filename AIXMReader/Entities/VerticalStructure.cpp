#include "VerticalStructure.h"

VerticalStructure::VerticalStructure()
{

}

bool VerticalStructure::getGroup() const
{
    return group;
}

void VerticalStructure::setGroup(bool value)
{
    group = value;
}

double VerticalStructure::getLength() const
{
    return length;
}

void VerticalStructure::setLength(double value)
{
    length = value;
}

bool VerticalStructure::getLighted() const
{
    return lighted;
}

void VerticalStructure::setLighted(bool value)
{
    lighted = value;
}

bool VerticalStructure::getLightingICAOStandard() const
{
    return lightingICAOStandard;
}

void VerticalStructure::setLightingICAOStandard(bool value)
{
    lightingICAOStandard = value;
}

bool VerticalStructure::getMarkingICAOStandard() const
{
    return markingICAOStandard;
}

void VerticalStructure::setMarkingICAOStandard(bool value)
{
    markingICAOStandard = value;
}

QString VerticalStructure::getName() const
{
    return name;
}

void VerticalStructure::setName(const QString &value)
{
    name = value;
}

double VerticalStructure::getRadius() const
{
    return radius;
}

void VerticalStructure::setRadius(double value)
{
    radius = value;
}

bool VerticalStructure::getSynchronisedLighting() const
{
    return synchronisedLighting;
}

void VerticalStructure::setSynchronisedLighting(bool value)
{
    synchronisedLighting = value;
}

double VerticalStructure::getWidth() const
{
    return width;
}

void VerticalStructure::setWidth(double value)
{
    width = value;
}

CodeVerticalStructure VerticalStructure::getType() const
{
    return type;
}

void VerticalStructure::setType(const CodeVerticalStructure &value)
{
    type = value;
}

QVector<VerticalStructurePart *> VerticalStructure::getParts() const
{
    return parts;
}

void VerticalStructure::setParts(const QVector<VerticalStructurePart *> &value)
{
    parts = value;
}
