#ifndef VERTICALSTRUCTURE_H
#define VERTICALSTRUCTURE_H

#include <QString>
#include "AIXMReader/Entities/BaseEntity.h"
#include "AIXMReader/DataTypes/CodeVerticalStructure.h"
#include "AIXMReader/Entities/VerticalStructurePart.h"
#include <QVector>

class VerticalStructure : public BaseEntity
{
public:
    VerticalStructure();

    bool getGroup() const;
    void setGroup(bool value);

    double getLength() const;
    void setLength(double value);

    bool getLighted() const;
    void setLighted(bool value);

    bool getLightingICAOStandard() const;
    void setLightingICAOStandard(bool value);

    bool getMarkingICAOStandard() const;
    void setMarkingICAOStandard(bool value);

    QString getName() const;
    void setName(const QString &value);

    double getRadius() const;
    void setRadius(double value);

    bool getSynchronisedLighting() const;
    void setSynchronisedLighting(bool value);

    double getWidth() const;
    void setWidth(double value);

    CodeVerticalStructure getType() const;
    void setType(const CodeVerticalStructure &value);

    QVector<VerticalStructurePart *> getParts() const;
    void setParts(const QVector<VerticalStructurePart *> &value);

private:
    bool group;
    double length;
    bool lighted;
    bool lightingICAOStandard;
    bool markingICAOStandard;
    QString name;
    double radius;
    bool synchronisedLighting;
    double width;
    CodeVerticalStructure type;
    QVector<VerticalStructurePart *> parts;
};

#endif // VERTICALSTRUCTURE_H
