#ifndef INSTRUMENTAPPROACHPROCEDURE_H
#define INSTRUMENTAPPROACHPROCEDURE_H
#include <QString>
#include <AIXMReader/Entities/Procedure.h>
#include <AIXMReader/DataTypes/CodeApproachPrefix.h>
#include <AIXMReader/DataTypes/CodeApproach.h>
#include <AIXMReader/DataTypes/CodeApproachEquipmentAdditional.h>

// Класс, описывающий Апроач

class InstrumentApproachProcedure : public Procedure
{
private:
    CodeApproachPrefix approachPrefix;
    CodeApproach approachType;
    char multipleIdentification; // UpperCaseChars
    double copterTrack; // min 0, max 360
    char circlingIdentification;
    QString courseReversalInstruction;
    CodeApproachEquipmentAdditional additionalEquipment;
    double channelGNSS;
    bool WAASReliable;
public:
    InstrumentApproachProcedure();
    InstrumentApproachProcedure(QString uuid);
    virtual ~InstrumentApproachProcedure();
    CodeApproachPrefix getApproachPrefix() const;
    void setApproachPrefix(const CodeApproachPrefix &value);
    CodeApproach getApproachType() const;
    void setApproachType(const CodeApproach &value);
    char getMultipleIdentification() const;
    void setMultipleIdentification(char value);
    double getCopterTrack() const;
    void setCopterTrack(double value);
    char getCirclingIdentification() const;
    void setCirclingIdentification(char value);
    QString getCourseReversalInstruction() const;
    void setCourseReversalInstruction(const QString &value);
    CodeApproachEquipmentAdditional getAdditionalEquipment() const;
    void setAdditionalEquipment(const CodeApproachEquipmentAdditional &value);
    double getChannelGNSS() const;
    void setChannelGNSS(double value);
    bool getWAASReliable() const;
    void setWAASReliable(bool value);
private:
    void init(
            CodeApproachPrefix approachPrefix = CodeApproachPrefix::OTHER,
            CodeApproach approachType = CodeApproach::OTHER,
            char multipleIdentification = '-',
            double copterTrack = INF,
            char circlingIdentification = '-',
            QString courseReversalInstruction = "",
            CodeApproachEquipmentAdditional additionalEquipment = CodeApproachEquipmentAdditional::OTHER,
            double channelGNSS = INF,
            bool WAASReliable = false
            );
};

#endif // INSTRUMENTAPPROACHPROCEDURE_H
