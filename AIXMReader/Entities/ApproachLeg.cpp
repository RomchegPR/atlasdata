#include "AIXMReader/Entities/ApproachLeg.h"

ApproachLeg::ApproachLeg() : SegmentLeg()
{
    init();
}

ApproachLeg::ApproachLeg(QString uuid) : SegmentLeg(uuid)
{
    init();
}

ApproachLeg::~ApproachLeg()
{
    delete approach;
}

QString ApproachLeg::getRequiredNavigationPerformance() const
{
    return requiredNavigationPerformance;
}

void ApproachLeg::setRequiredNavigationPerformance(const QString &value)
{
    requiredNavigationPerformance = value;
}

InstrumentApproachProcedure *ApproachLeg::getApproach() const
{
    return approach;
}

void ApproachLeg::setApproach(InstrumentApproachProcedure *value)
{
    approach = value;
}

void ApproachLeg::init(QString requiredNavigationPerformance, InstrumentApproachProcedure *approach)
{
    setRequiredNavigationPerformance(requiredNavigationPerformance);
    setApproach(approach);
}
