#ifndef AIRSPACEVOLUME_H
#define AIRSPACEVOLUME_H

#include "AIXMReader/DataTypes/CodeVerticalReference.h"
#include <QVector>
#include <QPointF>

class Airspace;

// Класс, описывающий объем воздушного пространства

class AirspaceVolume
{
public:
    AirspaceVolume();

    double getLowerLimit() const;
    void setLowerLimit(double value);

    CodeVerticalReference getLowerLimitReference() const;
    void setLowerLimitReference(const CodeVerticalReference &value);

    double getMaximumLimit() const;
    void setMaximumLimit(double value);

    CodeVerticalReference getMaximumLimitReference() const;
    void setMaximumLimitReference(const CodeVerticalReference &value);

    double getMinimumLimit() const;
    void setMinimumLimit(double value);

    CodeVerticalReference getMinimumLimitReference() const;
    void setMinimumLimitReference(const CodeVerticalReference &value);

    double getUpperLimit() const;
    void setUpperLimit(double value);

    CodeVerticalReference getUpperLimitReference() const;
    void setUpperLimitReference(const CodeVerticalReference &value);

    double getWidth() const;
    void setWidth(double value);

    QVector<QPointF> getCentreline() const;
    void setCentreline(const QVector<QPointF> &value);

    QVector<QPointF> getHorizontalProjection() const;
    void setHorizontalProjection(const QVector<QPointF> &value);

    Airspace *getAirspace() const;
    void setAirspace(Airspace *value);

private:
    double lowerLimit;
    CodeVerticalReference lowerLimitReference;
    double maximumLimit;
    CodeVerticalReference maximumLimitReference;
    double minimumLimit;
    CodeVerticalReference minimumLimitReference;
    double upperLimit;
    CodeVerticalReference upperLimitReference;
    double width;

    // Воздушное пространство могут описываться центральной линией, если они коридорного типа.
    QVector<QPointF> centreline;

    // Вектор, хранящий координаты плоскости воздушного пространства
    QVector<QPointF> horizontalProjection;

    Airspace* airspace;
};

#endif // AIRSPACEVOLUME_H
