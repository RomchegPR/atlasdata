#include "VerticalStructurePart.h"

VerticalStructurePart::VerticalStructurePart()
{

}

CodeStatusConstruction VerticalStructurePart::getConstructionStatus() const
{
    return constructionStatus;
}

void VerticalStructurePart::setConstructionStatus(const CodeStatusConstruction &value)
{
    constructionStatus = value;
}

QString VerticalStructurePart::getDesignator() const
{
    return designator;
}

void VerticalStructurePart::setDesignator(const QString &value)
{
    designator = value;
}

bool VerticalStructurePart::getFrangible() const
{
    return frangible;
}

void VerticalStructurePart::setFrangible(bool value)
{
    frangible = value;
}

CodeColour VerticalStructurePart::getMarkingFirstColour() const
{
    return markingFirstColour;
}

void VerticalStructurePart::setMarkingFirstColour(const CodeColour &value)
{
    markingFirstColour = value;
}

CodeVerticalStructureMarking VerticalStructurePart::getMarkingPattern() const
{
    return markingPattern;
}

void VerticalStructurePart::setMarkingPattern(const CodeVerticalStructureMarking &value)
{
    markingPattern = value;
}

CodeColour VerticalStructurePart::getMarkingSecondColour() const
{
    return markingSecondColour;
}

void VerticalStructurePart::setMarkingSecondColour(const CodeColour &value)
{
    markingSecondColour = value;
}

bool VerticalStructurePart::getMobile() const
{
    return mobile;
}

void VerticalStructurePart::setMobile(bool value)
{
    mobile = value;
}

CodeVerticalStructure VerticalStructurePart::getType() const
{
    return type;
}

void VerticalStructurePart::setType(const CodeVerticalStructure &value)
{
    type = value;
}

double VerticalStructurePart::getVerticalExtent() const
{
    return verticalExtent;
}

void VerticalStructurePart::setVerticalExtent(double value)
{
    verticalExtent = value;
}

double VerticalStructurePart::getVerticalExtentAccuracy() const
{
    return verticalExtentAccuracy;
}

void VerticalStructurePart::setVerticalExtentAccuracy(double value)
{
    verticalExtentAccuracy = value;
}

CodeVerticalStructureMaterial VerticalStructurePart::getVisibleMaterial() const
{
    return visibleMaterial;
}

void VerticalStructurePart::setVisibleMaterial(const CodeVerticalStructureMaterial &value)
{
    visibleMaterial = value;
}

GeometryType VerticalStructurePart::getGeometryType() const
{
    return geometryType;
}

void VerticalStructurePart::setGeometryType(const GeometryType &value)
{
    geometryType = value;
}

QVector<QPointF> VerticalStructurePart::getCoords() const
{
    return coords;
}

void VerticalStructurePart::setCoords(const QVector<QPointF> &value)
{
    coords = value;
}
