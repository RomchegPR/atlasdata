#include "DistanceIndication.h"

DistanceIndication::DistanceIndication() : BaseEntity()
{
    init();
}

DistanceIndication::DistanceIndication(QString uuid) : BaseEntity(uuid)
{
    init();
}

DistanceIndication::~DistanceIndication()
{

}

void DistanceIndication::init(double distance, double minimumReceptionAltitude)
{
    setDistance(distance);
    setMinimumReceptionAltitude(minimumReceptionAltitude);
}

double DistanceIndication::getMinimumReceptionAltitude() const
{
    return minimumReceptionAltitude;
}

void DistanceIndication::setMinimumReceptionAltitude(double value)
{
    minimumReceptionAltitude = value;
}

double DistanceIndication::getDistance() const
{
    return distance;
}

void DistanceIndication::setDistance(double value)
{
    distance = value;
}
