#include "FinalLeg.h"

FinalLeg::FinalLeg() : ApproachLeg()
{
    init();
}

FinalLeg::FinalLeg(QString uuid) : ApproachLeg(uuid)
{
    init();
}

FinalLeg::~FinalLeg()
{

}

void FinalLeg::init(CodeFinalGuidance guidanceSystem, CodeApproachGuidance landingSystemCategory, double minimumBaroVnavTemperature,
               bool rnpDMEAuthorized, double courseOffsetAngle, CodeSide courseOffsetSide, double courseCentrelineDistance,
               double courseOffsetDistance, CodeRelativePosition courseCentrelineIntersect)
{
    setGuidanceSystem(guidanceSystem);
    setLandingSystemCategory(landingSystemCategory);
    setMinimumBaroVnavTemperature(minimumBaroVnavTemperature);
    setRnpDMEAuthorized(rnpDMEAuthorized);
    setCourseOffsetAngle(courseOffsetAngle);
    setCourseOffsetSide(courseOffsetSide);
    setCourseCentrelineDistance(courseCentrelineDistance);
    setCourseOffsetDistance(courseOffsetDistance);
    setCourseCentrelineIntersect(courseCentrelineIntersect);
}

CodeFinalGuidance FinalLeg::getGuidanceSystem() const
{
    return guidanceSystem;
}

void FinalLeg::setGuidanceSystem(const CodeFinalGuidance &value)
{
    guidanceSystem = value;
}

CodeApproachGuidance FinalLeg::getLandingSystemCategory() const
{
    return landingSystemCategory;
}

void FinalLeg::setLandingSystemCategory(const CodeApproachGuidance &value)
{
    landingSystemCategory = value;
}

double FinalLeg::getMinimumBaroVnavTemperature() const
{
    return minimumBaroVnavTemperature;
}

void FinalLeg::setMinimumBaroVnavTemperature(double value)
{
    minimumBaroVnavTemperature = value;
}

bool FinalLeg::getRnpDMEAuthorized() const
{
    return rnpDMEAuthorized;
}

void FinalLeg::setRnpDMEAuthorized(bool value)
{
    rnpDMEAuthorized = value;
}

double FinalLeg::getCourseOffsetAngle() const
{
    return courseOffsetAngle;
}

void FinalLeg::setCourseOffsetAngle(double value)
{
    courseOffsetAngle = value;
}

CodeSide FinalLeg::getCourseOffsetSide() const
{
    return courseOffsetSide;
}

void FinalLeg::setCourseOffsetSide(const CodeSide &value)
{
    courseOffsetSide = value;
}

double FinalLeg::getCourseCentrelineDistance() const
{
    return courseCentrelineDistance;
}

void FinalLeg::setCourseCentrelineDistance(double value)
{
    courseCentrelineDistance = value;
}

double FinalLeg::getCourseOffsetDistance() const
{
    return courseOffsetDistance;
}

void FinalLeg::setCourseOffsetDistance(double value)
{
    courseOffsetDistance = value;
}

CodeRelativePosition FinalLeg::getCourseCentrelineIntersect() const
{
    return courseCentrelineIntersect;
}

void FinalLeg::setCourseCentrelineIntersect(const CodeRelativePosition &value)
{
    courseCentrelineIntersect = value;
}
