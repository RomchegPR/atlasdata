#include "AIXMReader/Entities/AircraftCharacteristic.h"
#include <limits>

AircraftCharacteristic::AircraftCharacteristic() : BaseEntity()
{
    init();
}

AircraftCharacteristic::AircraftCharacteristic(QString uuid) : BaseEntity(uuid)
{
    init();
}

AircraftCharacteristic::~AircraftCharacteristic()
{
}

void AircraftCharacteristic::init( CodeAircraft type, CodeAircraftEngine engine, CodeAircraftEngineNumber numberEngine, int typeAircraftICAO, CodeAircraftCategory aircraftLandingCategory,
                           double wingSpan, CodeValueInterpretation wingSpanInterpretation, CodeAircraftWingspanClass classWingSpan,
                           double weight, CodeValueInterpretation weightInterpretation, int passengers, CodeValueInterpretation passengersInterpretation,
                           double speed, CodeValueInterpretation speedInterpretation, CodeWakeTurbulence wakeTurbulence, CodeNavigationEquipment navigationEquipment,
                           CodeNavigationSpecification navigationSpecification, bool verticalSeparationCapability, CodeEquipmentAntiCollision antiCollisionAndSeparationEquipment,
                           CodeCommunicationMode communicationEquipment, CodeTransponder surveillanceEquipment)
{
    setType(type);
    setEngine(engine);
    setNumberEngine(numberEngine);
    setTypeAircraftICAO(typeAircraftICAO);
    setAircraftLandingCategory(aircraftLandingCategory);
    setWingSpan(wingSpan);
    setWingSpanInterpretation(wingSpanInterpretation);
    setClassWingSpan(classWingSpan);
    setWeight(weight);
    setWeightInterpretation(weightInterpretation);
    setPassengers(passengers);
    setPassengersInterpretation(passengersInterpretation);
    setSpeed(speed);
    setSpeedInterpretation(speedInterpretation);
    setWakeTurbulence(wakeTurbulence);
    setNavigationEquipment(navigationEquipment);
    setNavigationSpecification(navigationSpecification);
    setVerticalSeparationCapability(verticalSeparationCapability);
    setAntiCollisionAndSeparationEquipment(antiCollisionAndSeparationEquipment);
    setCommunicationEquipment(communicationEquipment);
    setSurveillanceEquipment(surveillanceEquipment);
}

CodeTransponder AircraftCharacteristic::getSurveillanceEquipment() const
{
    return surveillanceEquipment;
}

void AircraftCharacteristic::setSurveillanceEquipment(const CodeTransponder &value)
{
    surveillanceEquipment = value;
}

CodeCommunicationMode AircraftCharacteristic::getCommunicationEquipment() const
{
    return communicationEquipment;
}

void AircraftCharacteristic::setCommunicationEquipment(const CodeCommunicationMode &value)
{
    communicationEquipment = value;
}

CodeEquipmentAntiCollision AircraftCharacteristic::getAntiCollisionAndSeparationEquipment() const
{
    return antiCollisionAndSeparationEquipment;
}

void AircraftCharacteristic::setAntiCollisionAndSeparationEquipment(const CodeEquipmentAntiCollision &value)
{
    antiCollisionAndSeparationEquipment = value;
}

bool AircraftCharacteristic::getVerticalSeparationCapability() const
{
    return verticalSeparationCapability;
}

void AircraftCharacteristic::setVerticalSeparationCapability(bool value)
{
    verticalSeparationCapability = value;
}

CodeNavigationSpecification AircraftCharacteristic::getNavigationSpecification() const
{
    return navigationSpecification;
}

void AircraftCharacteristic::setNavigationSpecification(const CodeNavigationSpecification &value)
{
    navigationSpecification = value;
}

CodeNavigationEquipment AircraftCharacteristic::getNavigationEquipment() const
{
    return navigationEquipment;
}

void AircraftCharacteristic::setNavigationEquipment(const CodeNavigationEquipment &value)
{
    navigationEquipment = value;
}

CodeWakeTurbulence AircraftCharacteristic::getWakeTurbulence() const
{
    return wakeTurbulence;
}

void AircraftCharacteristic::setWakeTurbulence(const CodeWakeTurbulence &value)
{
    wakeTurbulence = value;
}

CodeValueInterpretation AircraftCharacteristic::getSpeedInterpretation() const
{
    return speedInterpretation;
}

void AircraftCharacteristic::setSpeedInterpretation(const CodeValueInterpretation &value)
{
    speedInterpretation = value;
}

double AircraftCharacteristic::getSpeed() const
{
    return speed;
}

void AircraftCharacteristic::setSpeed(double value)
{
    speed = value;
}

CodeValueInterpretation AircraftCharacteristic::getPassengersInterpretation() const
{
    return passengersInterpretation;
}

void AircraftCharacteristic::setPassengersInterpretation(const CodeValueInterpretation &value)
{
    passengersInterpretation = value;
}

int AircraftCharacteristic::getPassengers() const
{
    return passengers;
}

void AircraftCharacteristic::setPassengers(int value)
{
    passengers = value;
}

CodeValueInterpretation AircraftCharacteristic::getWeightInterpretation() const
{
    return weightInterpretation;
}

void AircraftCharacteristic::setWeightInterpretation(const CodeValueInterpretation &value)
{
    weightInterpretation = value;
}

double AircraftCharacteristic::getWeight() const
{
    return weight;
}

void AircraftCharacteristic::setWeight(double value)
{
    weight = value;
}

CodeValueInterpretation AircraftCharacteristic::getWingSpanInterpretation() const
{
    return wingSpanInterpretation;
}

void AircraftCharacteristic::setWingSpanInterpretation(const CodeValueInterpretation &value)
{
    wingSpanInterpretation = value;
}

double AircraftCharacteristic::getWingSpan() const
{
    return wingSpan;
}

void AircraftCharacteristic::setWingSpan(double value)
{
    wingSpan = value;
}

CodeAircraftCategory AircraftCharacteristic::getAircraftLandingCategory() const
{
    return aircraftLandingCategory;
}

void AircraftCharacteristic::setAircraftLandingCategory(const CodeAircraftCategory &value)
{
    aircraftLandingCategory = value;
}

int AircraftCharacteristic::getTypeAircraftICAO() const
{
    return typeAircraftICAO;
}

void AircraftCharacteristic::setTypeAircraftICAO(int value)
{
    typeAircraftICAO = value;
}

CodeAircraftEngineNumber AircraftCharacteristic::getNumberEngine() const
{
    return numberEngine;
}

void AircraftCharacteristic::setNumberEngine(const CodeAircraftEngineNumber &value)
{
    numberEngine = value;
}

CodeAircraftEngine AircraftCharacteristic::getEngine() const
{
    return engine;
}

void AircraftCharacteristic::setEngine(const CodeAircraftEngine &value)
{
    engine = value;
}

CodeAircraft AircraftCharacteristic::getType() const
{
    return type;
}

void AircraftCharacteristic::setType(const CodeAircraft &value)
{
    type = value;
}

CodeAircraftWingspanClass AircraftCharacteristic::getClassWingSpan() const
{
    return classWingSpan;
}

void AircraftCharacteristic::setClassWingSpan(const CodeAircraftWingspanClass &value)
{
    classWingSpan = value;
}
