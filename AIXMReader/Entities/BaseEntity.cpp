#include "AIXMReader/Entities/BaseEntity.h"

BaseEntity::BaseEntity()
{
    uuid = QUuid::createUuid();
}

BaseEntity::BaseEntity(QString uuid)
{
    this->uuid = QUuid(uuid);
}

BaseEntity::~BaseEntity(){

}

QString BaseEntity::getUuid(){
    return uuid.toString().mid(1, 36);
}

void BaseEntity::setUuid(QString uuid){
    this->uuid = QUuid(uuid);
}
