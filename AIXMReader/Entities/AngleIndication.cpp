#include "AngleIndication.h"

AngleIndication::AngleIndication() : BaseEntity()
{
    init();
}

AngleIndication::AngleIndication(QString uuid) : BaseEntity(uuid)
{
    init();
}

AngleIndication::~AngleIndication(){

}

double AngleIndication::getAngle() const
{
    return angle;
}

void AngleIndication::setAngle(double value)
{
    angle = value;
}

CodeBearing AngleIndication::getAngleType() const
{
    return angleType;
}

void AngleIndication::setAngleType(const CodeBearing &value)
{
    angleType = value;
}

CodeDirectionReference AngleIndication::getIndicationDirection() const
{
    return indicationDirection;
}

void AngleIndication::setIndicationDirection(const CodeDirectionReference &value)
{
    indicationDirection = value;
}

double AngleIndication::getTrueAngle() const
{
    return trueAngle;
}

void AngleIndication::setTrueAngle(double value)
{
    trueAngle = value;
}

CodeCardinalDirection AngleIndication::getCardinalDirection() const
{
    return cardinalDirection;
}

void AngleIndication::setCardinalDirection(const CodeCardinalDirection &value)
{
    cardinalDirection = value;
}

double AngleIndication::getMinimumReceptionAltitude() const
{
    return minimumReceptionAltitude;
}

void AngleIndication::setMinimumReceptionAltitude(double value)
{
    minimumReceptionAltitude = value;
}

void AngleIndication::init(double angle, CodeBearing angleType, CodeDirectionReference indicationDirection,
                     double trueAngle, CodeCardinalDirection cardinalDirection,
                     double minimumReceptionAltitude)
{
    setAngle(angle);
    setAngleType(angleType);
    setIndicationDirection(indicationDirection);
    setTrueAngle(trueAngle);
    setCardinalDirection(cardinalDirection);
    setMinimumReceptionAltitude(minimumReceptionAltitude);
}
