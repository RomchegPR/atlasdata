#include "AIXMReader/Entities/StandardInstrumentArrival.h"

StandardInstrumentArrival::StandardInstrumentArrival() : Procedure()
{
    init();}

StandardInstrumentArrival::StandardInstrumentArrival(QString uuid) : Procedure(uuid)
{
    init();
}

StandardInstrumentArrival::~StandardInstrumentArrival()
{
}

QString StandardInstrumentArrival::getDesignator() const
{
    return designator;
}

void StandardInstrumentArrival::setDesignator(const QString &value)
{
    designator = value;
}

void StandardInstrumentArrival::init(QString designator)
{
    setDesignator(designator);
}

