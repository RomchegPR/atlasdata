#ifndef DESIGNATEDPOINT_H
#define DESIGNATEDPOINT_H

#include "AIXMReader/Entities/BaseEntity.h"
#include "AIXMReader/DataTypes/CodeDesignatedPoint.h"
#include <QPoint>

// Класс, описывающий геоточку

class DesignatedPoint : public BaseEntity
{
private:
    CodeDesignatedPoint type;
    QString name;
    QString designator;
    QPointF point;
public:
    DesignatedPoint();
    DesignatedPoint(QString uuid);
    virtual ~DesignatedPoint();

    float latitude();
    void setLatitude(float latitude);
    float longitude();
    void setLongitude(float longitude);
    QString getName();
    void setName(const QString& name);
    CodeDesignatedPoint getType();
    void setType(CodeDesignatedPoint type);
    QPointF getPoint() const;
    void setPoint(const QPointF &value);

    QString getDesignator() const;
    void setDesignator(const QString &value);

private:
    void init(
            CodeDesignatedPoint type = CodeDesignatedPoint::OTHER,
            QString name = "",
            QPointF point = QPointF()
            );
};

#endif // DESIGNATEDPOINT_H
