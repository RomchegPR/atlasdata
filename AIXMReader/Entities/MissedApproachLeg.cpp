#include "MissedApproachLeg.h"

MissedApproachLeg::MissedApproachLeg() : ApproachLeg()
{
    init();
}

MissedApproachLeg::MissedApproachLeg(QString uuid) : ApproachLeg(uuid)
{
    init();
}

MissedApproachLeg::~MissedApproachLeg()
{

}

void MissedApproachLeg::init(CodeMissedApproach type, bool thresholdAfterMAPT)
{
    setType(type);
    setThresholdAfterMAPT(thresholdAfterMAPT);
}

CodeMissedApproach MissedApproachLeg::getType() const
{
    return type;
}

void MissedApproachLeg::setType(const CodeMissedApproach &value)
{
    type = value;
}

bool MissedApproachLeg::getThresholdAfterMAPT() const
{
    return thresholdAfterMAPT;
}

void MissedApproachLeg::setThresholdAfterMAPT(bool value)
{
    thresholdAfterMAPT = value;
}
