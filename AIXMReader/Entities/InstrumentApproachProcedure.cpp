#include "AIXMReader/Entities/InstrumentApproachProcedure.h"

InstrumentApproachProcedure::InstrumentApproachProcedure() : Procedure()
{
    init();
}

InstrumentApproachProcedure::InstrumentApproachProcedure(QString uuid) : Procedure(uuid)
{
    init();
}


InstrumentApproachProcedure::~InstrumentApproachProcedure()
{

}

void InstrumentApproachProcedure::init(CodeApproachPrefix approachPrefix, CodeApproach approachType, char multipleIdentification,
                                 double copterTrack, char circlingIdentification, QString courseReversalInstruction,
                                 CodeApproachEquipmentAdditional additionalEquipment, double channelGNSS, bool WAASReliable)
{
    setApproachPrefix(approachPrefix);
    setApproachType(approachType);
    setMultipleIdentification(multipleIdentification);
    setCopterTrack(copterTrack);
    setCirclingIdentification(circlingIdentification);
    setCourseReversalInstruction(courseReversalInstruction);
    setAdditionalEquipment(additionalEquipment);
    setChannelGNSS(channelGNSS);
    setWAASReliable(WAASReliable);
}

bool InstrumentApproachProcedure::getWAASReliable() const
{
    return WAASReliable;
}

void InstrumentApproachProcedure::setWAASReliable(bool value)
{
    WAASReliable = value;
}

double InstrumentApproachProcedure::getChannelGNSS() const
{
    return channelGNSS;
}

void InstrumentApproachProcedure::setChannelGNSS(double value)
{
    channelGNSS = value;
}

CodeApproachEquipmentAdditional InstrumentApproachProcedure::getAdditionalEquipment() const
{
    return additionalEquipment;
}

void InstrumentApproachProcedure::setAdditionalEquipment(const CodeApproachEquipmentAdditional &value)
{
    additionalEquipment = value;
}

QString InstrumentApproachProcedure::getCourseReversalInstruction() const
{
    return courseReversalInstruction;
}

void InstrumentApproachProcedure::setCourseReversalInstruction(const QString &value)
{
    courseReversalInstruction = value;
}

char InstrumentApproachProcedure::getCirclingIdentification() const
{
    return circlingIdentification;
}

void InstrumentApproachProcedure::setCirclingIdentification(char value)
{
    circlingIdentification = value;
}

double InstrumentApproachProcedure::getCopterTrack() const
{
    return copterTrack;
}

void InstrumentApproachProcedure::setCopterTrack(double value)
{
    copterTrack = value;
}

char InstrumentApproachProcedure::getMultipleIdentification() const
{
    return multipleIdentification;
}

void InstrumentApproachProcedure::setMultipleIdentification(char value)
{
    multipleIdentification = value;
}

CodeApproach InstrumentApproachProcedure::getApproachType() const
{
    return approachType;
}

void InstrumentApproachProcedure::setApproachType(const CodeApproach &value)
{
    approachType = value;
}

CodeApproachPrefix InstrumentApproachProcedure::getApproachPrefix() const
{
    return approachPrefix;
}

void InstrumentApproachProcedure::setApproachPrefix(const CodeApproachPrefix &value)
{
    approachPrefix = value;
}
