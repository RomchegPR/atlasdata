#include "AIXMReader/Entities/StandardInstrumentDeparture.h"

StandardInstrumentDeparture::StandardInstrumentDeparture() : Procedure()
{
    init();
}

StandardInstrumentDeparture::StandardInstrumentDeparture(QString uuid) : Procedure(uuid)
{
    init();
}

StandardInstrumentDeparture::~StandardInstrumentDeparture()
{

}

void StandardInstrumentDeparture::init(QString designator, bool contingencyRoute)
{
    setDesignator(designator);
    setContingencyRoute(contingencyRoute);
}

bool StandardInstrumentDeparture::getContingencyRoute() const
{
    return contingencyRoute;
}

void StandardInstrumentDeparture::setContingencyRoute(bool value)
{
    contingencyRoute = value;
}

QString StandardInstrumentDeparture::getDesignator() const
{
    return designator;
}

void StandardInstrumentDeparture::setDesignator(const QString &value)
{
    designator = value;
}
