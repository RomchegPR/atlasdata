#include "AIXMReader/Entities/Airspace.h"

Airspace::Airspace()
{

}

QString Airspace::getLocalType() const
{
    return localType;
}

void Airspace::setLocalType(const QString &value)
{
    localType = value;
}

CodeMilitaryOperations Airspace::getControlType() const
{
    return controlType;
}

void Airspace::setControlType(const CodeMilitaryOperations &value)
{
    controlType = value;
}

QString Airspace::getDesignator() const
{
    return designator;
}

void Airspace::setDesignator(const QString &value)
{
    designator = value;
}

bool Airspace::getDesignatorICAO() const
{
    return designatorICAO;
}

void Airspace::setDesignatorICAO(bool value)
{
    designatorICAO = value;
}

QString Airspace::getName() const
{
    return name;
}

void Airspace::setName(const QString &value)
{
    name = value;
}

CodeAirspace Airspace::getType() const
{
    return type;
}

void Airspace::setType(const CodeAirspace &value)
{
    type = value;
}

QString Airspace::getUpperLowerSeparation() const
{
    return upperLowerSeparation;
}

void Airspace::setUpperLowerSeparation(const QString &value)
{
    upperLowerSeparation = value;
}

QVector<AirspaceVolume *> Airspace::getVolumes() const
{
    return volumes;
}

void Airspace::setVolumes(const QVector<AirspaceVolume *> &value)
{
    volumes = value;
}
