#include "AirspaceVolume.h"

AirspaceVolume::AirspaceVolume()
{

}

double AirspaceVolume::getLowerLimit() const
{
    return lowerLimit;
}

void AirspaceVolume::setLowerLimit(double value)
{
    lowerLimit = value;
}

CodeVerticalReference AirspaceVolume::getLowerLimitReference() const
{
    return lowerLimitReference;
}

void AirspaceVolume::setLowerLimitReference(const CodeVerticalReference &value)
{
    lowerLimitReference = value;
}

double AirspaceVolume::getMaximumLimit() const
{
    return maximumLimit;
}

void AirspaceVolume::setMaximumLimit(double value)
{
    maximumLimit = value;
}

CodeVerticalReference AirspaceVolume::getMaximumLimitReference() const
{
    return maximumLimitReference;
}

void AirspaceVolume::setMaximumLimitReference(const CodeVerticalReference &value)
{
    maximumLimitReference = value;
}

double AirspaceVolume::getMinimumLimit() const
{
    return minimumLimit;
}

void AirspaceVolume::setMinimumLimit(double value)
{
    minimumLimit = value;
}

CodeVerticalReference AirspaceVolume::getMinimumLimitReference() const
{
    return minimumLimitReference;
}

void AirspaceVolume::setMinimumLimitReference(const CodeVerticalReference &value)
{
    minimumLimitReference = value;
}

double AirspaceVolume::getUpperLimit() const
{
    return upperLimit;
}

void AirspaceVolume::setUpperLimit(double value)
{
    upperLimit = value;
}

CodeVerticalReference AirspaceVolume::getUpperLimitReference() const
{
    return upperLimitReference;
}

void AirspaceVolume::setUpperLimitReference(const CodeVerticalReference &value)
{
    upperLimitReference = value;
}

double AirspaceVolume::getWidth() const
{
    return width;
}

void AirspaceVolume::setWidth(double value)
{
    width = value;
}

QVector<QPointF> AirspaceVolume::getCentreline() const
{
    return centreline;
}

void AirspaceVolume::setCentreline(const QVector<QPointF> &value)
{
    centreline = value;
}

QVector<QPointF> AirspaceVolume::getHorizontalProjection() const
{
    return horizontalProjection;
}

void AirspaceVolume::setHorizontalProjection(const QVector<QPointF> &value)
{
    horizontalProjection = value;
}

Airspace *AirspaceVolume::getAirspace() const
{
    return airspace;
}

void AirspaceVolume::setAirspace(Airspace *value)
{
    airspace = value;
}
