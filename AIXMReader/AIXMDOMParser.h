#ifndef AIXMDOMPARSER_H
#define AIXMDOMPARSER_H

#include "Reader/AIXMData.h"
#include <QDomDocument>

// Неиспользуемый класс

class AIXMDOMParser
{
    AIXMData* data;
public:
    AIXMDOMParser(QDomDocument& document);
    AIXMData* getData();
};

#endif // AIXMDOMPARSER_H
