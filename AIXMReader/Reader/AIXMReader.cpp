#include "AIXMReader.h"
#include <QFile>
#include <QDebug>

AIXMReader::AIXMReader() :
    mDocument()
{
}

AIXMReader::AIXMReader(QString filename) :
    mDocument()
{
    setData(filename);
}

void AIXMReader::setData(const QString &filename){
    if(filename != ""){
        QFile inputFile(filename);
        inputFile.open(QIODevice::ReadOnly);
        QByteArray buffer = inputFile.readAll();
        mDocument.setContent(buffer, true);
        inputFile.close();
    }
}

QDomDocument& AIXMReader::getDocument(){
    return mDocument;
}

AIXMReader::~AIXMReader()
{

}
