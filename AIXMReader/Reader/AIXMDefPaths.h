#ifndef AIXMDEFPATHS_H
#define AIXMDEFPATHS_H
/************************************************************************/
// Читаемые тэги
// $(...) - имя аттрибута
/************************************************************************/
#ifndef AIXM_WITHOUT_NAMESPACE
//TMA - group APP
//FIR - group ACC
//CTA - group Domestic

//COMPULSORY - ACC/Reporting point
//ON_REQUEST - ACC/Default point

#define AIXM_DESCRIPTION "description"
#define AIXM_BMESSAGE    "message:AIXMBasicMessage"
#define AIXM_HASMEMBER   "message:hasMember"
#define AIXM_AIRSPACE    "aixm:Airspace"
#define AIXM_AIRPORT     "aixm:AirportHeliport"
#define AIXM_DESPOINT    "aixm:DesignatedPoint"
#define AIXM_ROUTE       "aixm:Route"
#define AIXM_ROUTE_SEG   "aixm:RouteSegment"
#define AIXM_ID          "gml:id"
#define AIXM_TITLE       "xlink:title"
#define AIXM_HREF        "xlink:href"
#define AIXM_GML_NS      "gml:"

//SECTOR bounds
#define AIXM_BOUNDS         "aixm:AirspaceVolume"
#define AIXM_BND_LIM_DIM    "aixm:upperLimit/$(uom)"
#define AIXM_BND_UPPER      "aixm:upperLimit"
#define AIXM_BND_LOWER      "aixm:lowerLimit"
#define AIXM_BND_POS        "aixm:horizontalProjection/aixm:Surface/gml:patches/gml:PolygonPatch/gml:exterior/gml:Ring/gml:curveMember/aixm:Curve/gml:segments/gml:GeodesicString/gml:posList"

#define AIXM_BND_GCOMPONENT "aixm:geometryComponent"
#define AIXM_BND_AIR_POS    "aixm:AirspaceGeometryComponent/aixm:theAirspaceVolume/aixm:AirspaceVolume/aixm:horizontalProjection/aixm:Surface/gml:patches/gml:PolygonPatch/gml:exterior/gml:Ring/gml:curveMember/aixm:Curve/gml:segments/gml:GeodesicString/gml:posList"
#define AIXM_BND_AIR_UP     "aixm:AirspaceGeometryComponent/aixm:theAirspaceVolume/aixm:AirspaceVolume/aixm:upperLimit"
#define AIXM_BND_AIR_LOW    "aixm:AirspaceGeometryComponent/aixm:theAirspaceVolume/aixm:AirspaceVolume/aixm:lowerLimit"

//SECTOR (Airspace) (zoneX:FIR_UIR_AIRSPACES - class:C_Bound)
#define AIXM_UUID       "gml:identifier"
#define AIXM_OPERATE    "aixm:timeSlice/aixm:AirspaceTimeSlice"
#define AIXM_NAME       "aixm:timeSlice/aixm:AirspaceTimeSlice/aixm:name"
#define AIXM_DESIGN     "aixm:timeSlice/aixm:AirspaceTimeSlice/aixm:designator"
#define AIXM_NAME_EN    "aixm:timeSlice/aixm:AirspaceTimeSlice/aixm:annotation/aixm:Note/aixm:translatedNote/aixm:LinguisticNote/aixm:note/$(lang=ENG:NAME_EN)"
#define AIXM_DESIGN_EN  "aixm:timeSlice/aixm:AirspaceTimeSlice/aixm:annotation/aixm:Note/aixm:translatedNote/aixm:LinguisticNote/aixm:note/$(lang=ENG:DESIGNATOR_EN)"//"aixm:timeSlice/aixm:AirspaceTimeSlice/aixm:designator"
#define AIXM_NAME_RU    "aixm:timeSlice/aixm:AirspaceTimeSlice/aixm:annotation/aixm:Note/aixm:translatedNote/aixm:LinguisticNote/aixm:note/$(lang=RUS:NAME_RU)"
#define AIXM_DESIGN_RU  "aixm:timeSlice/aixm:AirspaceTimeSlice/aixm:annotation/aixm:Note/aixm:translatedNote/aixm:LinguisticNote/aixm:note/$(lang=RUS:DESIGNATOR_RU)"
#define AIXM_TYPE       "aixm:timeSlice/aixm:AirspaceTimeSlice/aixm:type"
#define AIXM_LTYPE      "aixm:timeSlice/aixm:AirspaceTimeSlice/aixm:localType"
#define AIXM_BTIME      "aixm:timeSlice/aixm:AirspaceTimeSlice/gml:validTime/gml:TimePeriod/gml:beginPosition"
#define AIXM_ETIME      "aixm:timeSlice/aixm:AirspaceTimeSlice/gml:validTime/gml:TimePeriod/gml:endPosition"
#define AIXM_F_BTIME    "aixm:timeSlice/aixm:AirspaceTimeSlice/aixm:featureLifetime/gml:TimePeriod/gml:beginPosition"
#define AIXM_F_ETIME    "aixm:timeSlice/aixm:AirspaceTimeSlice/aixm:featureLifetime/gml:TimePeriod/gml:endPosition"
#define AIXM_SECTOR_POS "aixm:timeSlice/aixm:AirspaceTimeSlice/aixm:geometryComponent/aixm:AirspaceGeometryComponent/aixm:theAirspaceVolume/aixm:AirspaceVolume/aixm:horizontalProjection/aixm:Surface/gml:patches/gml:PolygonPatch/gml:exterior/gml:Ring/gml:curveMember/aixm:Curve/gml:segments/gml:GeodesicString/gml:posList"
#define AIXM_DIM_LIMIT_UPPER  "aixm:timeSlice/aixm:AirspaceTimeSlice/aixm:geometryComponent/aixm:AirspaceGeometryComponent/aixm:theAirspaceVolume/aixm:AirspaceVolume/aixm:upperLimit/$(uom)"
#define AIXM_DIM_LIMIT_LOWER  "aixm:timeSlice/aixm:AirspaceTimeSlice/aixm:geometryComponent/aixm:AirspaceGeometryComponent/aixm:theAirspaceVolume/aixm:AirspaceVolume/aixm:lowerLimit/$(uom)"
#define AIXM_UPPER      "aixm:timeSlice/aixm:AirspaceTimeSlice/aixm:geometryComponent/aixm:AirspaceGeometryComponent/aixm:theAirspaceVolume/aixm:AirspaceVolume/aixm:upperLimit"
#define AIXM_LOWER      "aixm:timeSlice/aixm:AirspaceTimeSlice/aixm:geometryComponent/aixm:AirspaceGeometryComponent/aixm:theAirspaceVolume/aixm:AirspaceVolume/aixm:lowerLimit"

//ICAO (DesignatedPoint) (zoneX:WAYPOINTS - class:C_Waypoint)
#define AIXM_ICAO_UUID       "gml:identifier"
#define AIXM_ICAO_NAME       "aixm:timeSlice/aixm:DesignatedPointTimeSlice/aixm:name"
#define AIXM_ICAO_DESIGN     "aixm:timeSlice/aixm:DesignatedPointTimeSlice/aixm:designator"
#define AIXM_ICAO_NAME_EN    "aixm:timeSlice/aixm:DesignatedPointTimeSlice/aixm:annotation/aixm:Note/aixm:translatedNote/aixm:LinguisticNote/aixm:note/$(lang=ENG:NAME_EN)"
#define AIXM_ICAO_DESIGN_EN  "aixm:timeSlice/aixm:DesignatedPointTimeSlice/aixm:annotation/aixm:Note/aixm:translatedNote/aixm:LinguisticNote/aixm:note/$(lang=ENG:ICAO6_EN)"
#define AIXM_ICAO_NAME_RU    "aixm:timeSlice/aixm:DesignatedPointTimeSlice/aixm:annotation/aixm:Note/aixm:translatedNote/aixm:LinguisticNote/aixm:note/$(lang=RUS:NAME_RU)"
#define AIXM_ICAO_DESIGN_RU  "aixm:timeSlice/aixm:DesignatedPointTimeSlice/aixm:annotation/aixm:Note/aixm:translatedNote/aixm:LinguisticNote/aixm:note/$(lang=RUS:ICAO6_RU)"
#define AIXM_ICAO_TYPE       "aixm:timeSlice/aixm:DesignatedPointTimeSlice/aixm:type"
#define AIXM_ICAO_LTYPE      "aixm:timeSlice/aixm:DesignatedPointTimeSlice/aixm:localType"
#define AIXM_ICAO_BTIME      "aixm:timeSlice/aixm:DesignatedPointTimeSlice/gml:validTime/gml:TimePeriod/gml:beginPosition"
#define AIXM_ICAO_ETIME      "aixm:timeSlice/aixm:DesignatedPointTimeSlice/gml:validTime/gml:TimePeriod/gml:endPosition"
#define AIXM_ICAO_F_BTIME    "aixm:timeSlice/aixm:DesignatedPointTimeSlice/aixm:featureLifetime/gml:TimePeriod/gml:beginPosition"
#define AIXM_ICAO_F_ETIME    "aixm:timeSlice/aixm:DesignatedPointTimeSlice/aixm:featureLifetime/gml:TimePeriod/gml:endPosition"
#define AIXM_ICAO_POS        "aixm:timeSlice/aixm:DesignatedPointTimeSlice/aixm:location/aixm:ElevatedPoint/gml:pos"
#define AIXM_ICAO_DIM_LIMIT_UPPER  "aixm:timeSlice/aixm:DesignatedPointTimeSlice/aixm:location/aixm:DesignatedPointGeometryComponent/aixm:upperLimit/$(uom)"
#define AIXM_ICAO_DIM_LIMIT_LOWER  "aixm:timeSlice/aixm:DesignatedPointTimeSlice/aixm:location/aixm:DesignatedPointGeometryComponent/aixm:lowerLimit/$(uom)"
#define AIXM_ICAO_UPPER      "aixm:timeSlice/aixm:DesignatedPointTimeSlice/aixm:location/aixm:DesignatedPointGeometryComponent/aixm:upperLimit"
#define AIXM_ICAO_LOWER      "aixm:timeSlice/aixm:DesignatedPointTimeSlice/aixm:location/aixm:DesignatedPointGeometryComponent/aixm:lowerLimit"

//Airport (AirportHeliport) (zoneX:Airports - class:C_MasterAD)
#define AIXM_PORT_UUID       "gml:identifier"
#define AIXM_PORT_NAME       "aixm:timeSlice/aixm:AirportHeliportTimeSlice/aixm:name"
#define AIXM_PORT_DESIGN     "aixm:timeSlice/aixm:AirportHeliportTimeSlice/aixm:designator"
#define AIXM_PORT_NAME_EN    "aixm:timeSlice/aixm:AirportHeliportTimeSlice/aixm:annotation/aixm:Note/aixm:translatedNote/aixm:LinguisticNote/aixm:note/$(lang=ENG:NAME_EN)"
#define AIXM_PORT_DESIGN_EN  "aixm:timeSlice/aixm:AirportHeliportTimeSlice/aixm:annotation/aixm:Note/aixm:translatedNote/aixm:LinguisticNote/aixm:note/$(lang=ENG:DESIGNATOR_EN)"
#define AIXM_PORT_NAME_RU    "aixm:timeSlice/aixm:AirportHeliportTimeSlice/aixm:annotation/aixm:Note/aixm:translatedNote/aixm:LinguisticNote/aixm:note/$(lang=RUS:NAME_RU)"
#define AIXM_PORT_DESIGN_RU  "aixm:timeSlice/aixm:AirportHeliportTimeSlice/aixm:annotation/aixm:Note/aixm:translatedNote/aixm:LinguisticNote/aixm:note/$(lang=RUS:DESIGNATOR_RU)"
#define AIXM_PORT_TYPE       "aixm:timeSlice/aixm:AirportHeliportTimeSlice/aixm:type"
#define AIXM_PORT_CONT_TYPE  "aixm:timeSlice/aixm:AirportHeliportTimeSlice/aixm:controlType"
#define AIXM_PORT_L_INDICATOR "aixm:timeSlice/aixm:AirportHeliportTimeSlice/aixm:locationIndicatorICAO"
#define AIXM_PORT_BTIME      "aixm:timeSlice/aixm:AirportHeliportTimeSlice/gml:validTime/gml:TimePeriod/gml:beginPosition"
#define AIXM_PORT_ETIME      "aixm:timeSlice/aixm:AirportHeliportTimeSlice/gml:validTime/gml:TimePeriod/gml:endPosition"
#define AIXM_PORT_F_BTIME    "aixm:timeSlice/aixm:AirportHeliportTimeSlice/aixm:featureLifetime/gml:TimePeriod/gml:beginPosition"
#define AIXM_PORT_F_ETIME    "aixm:timeSlice/aixm:AirportHeliportTimeSlice/aixm:featureLifetime/gml:TimePeriod/gml:endPosition"
#define AIXM_PORT_POS        "aixm:timeSlice/aixm:AirportHeliportTimeSlice/aixm:ARP/aixm:ElevatedPoint/gml:pos"

//ATS (Route) (zoneX:AIRWAYS - class:C_Airway)
#define AIXM_ATS_UUID       "gml:identifier"
#define AIXM_ATS_NAME       "aixm:timeSlice/aixm:RouteTimeSlice/aixm:annotation/aixm:Note/aixm:translatedNote/aixm:LinguisticNote/aixm:note/$(lang=ENG:DESIGNATOR_EN)"
#define AIXM_ATS_DESIGN     "aixm:timeSlice/aixm:RouteTimeSlice/aixm:annotation/aixm:Note/aixm:translatedNote/aixm:LinguisticNote/aixm:note/$(lang=ENG:DESIGNATOR_EN)"
#define AIXM_ATS_NAME_RU    "aixm:timeSlice/aixm:RouteTimeSlice/aixm:annotation/aixm:Note/aixm:translatedNote/aixm:LinguisticNote/aixm:note/$(lang=RUS:DESIGNATOR_RU)"
#define AIXM_ATS_DESIGN_RU  "aixm:timeSlice/aixm:RouteTimeSlice/aixm:annotation/aixm:Note/aixm:translatedNote/aixm:LinguisticNote/aixm:note/$(lang=RUS:DESIGNATOR_RU)"
#define AIXM_ATS_BTIME      "aixm:timeSlice/aixm:RouteTimeSlice/gml:validTime/gml:TimePeriod/gml:beginPosition"
#define AIXM_ATS_ETIME      "aixm:timeSlice/aixm:RouteTimeSlice/gml:validTime/gml:TimePeriod/gml:endPosition"
#define AIXM_ATS_F_BTIME    "aixm:timeSlice/aixm:RouteTimeSlice/aixm:featureLifetime/gml:TimePeriod/gml:beginPosition"
#define AIXM_ATS_F_ETIME    "aixm:timeSlice/aixm:RouteTimeSlice/aixm:featureLifetime/gml:TimePeriod/gml:endPosition"
#define AIXM_ATS_USE        "aixm:timeSlice/aixm:RouteTimeSlice/aixm:internationalUse"
#define AIXM_ATS_ROUTE_TYPE "aixm:timeSlice/aixm:RouteTimeSlice/aixm:annotation/aixm:Note/aixm:translatedNote/aixm:LinguisticNote/aixm:note/$(lang=ENG:ROUTE_TYPE)"
#define AIXM_ATS_REMARK     "aixm:timeSlice/aixm:RouteTimeSlice/aixm:annotation/aixm:Note/aixm:translatedNote/aixm:LinguisticNote/aixm:note/$(lang=ENG:REMARK)"

#define AIXM_ATS_DES_PREF   "aixm:timeSlice/aixm:RouteTimeSlice/aixm:designatorPrefix"
#define AIXM_ATS_SCND_LETER "aixm:timeSlice/aixm:RouteTimeSlice/aixm:designatorSecondLetter"
#define AIXM_ATS_NUMBER     "aixm:timeSlice/aixm:RouteTimeSlice/aixm:designatorNumber"

//(RouteSegment) (zoneX:AIRWAYS - class:C_Airway)
#define AIXM_SEG_UUID       "gml:identifier"
#define AIXM_SEG_BTIME      "aixm:timeSlice/aixm:RouteSegmentTimeSlice/gml:validTime/gml:TimePeriod/gml:beginPosition"
#define AIXM_SEG_ETIME      "aixm:timeSlice/aixm:RouteSegmentTimeSlice/gml:validTime/gml:TimePeriod/gml:endPosition"
#define AIXM_SEG_F_BTIME    "aixm:timeSlice/aixm:RouteSegmentTimeSlice/aixm:featureLifetime/gml:TimePeriod/gml:beginPosition"
#define AIXM_SEG_F_ETIME    "aixm:timeSlice/aixm:RouteSegmentTimeSlice/aixm:featureLifetime/gml:TimePeriod/gml:endPosition"
#define AIXM_SEG_DIM_LIMIT_UPPER  "aixm:timeSlice/aixm:RouteSegmentTimeSlice/aixm:upperLimit/$(uom)"
#define AIXM_SEG_DIM_LIMIT_LOWER  "aixm:timeSlice/aixm:RouteSegmentTimeSlice/aixm:lowerLimit/$(uom)"
#define AIXM_SEG_UPPER      "aixm:timeSlice/aixm:RouteSegmentTimeSlice/aixm:upperLimit"
#define AIXM_SEG_LOWER      "aixm:timeSlice/aixm:RouteSegmentTimeSlice/aixm:lowerLimit"
#define AIXM_SEG_LENGHT     "aixm:timeSlice/aixm:RouteSegmentTimeSlice/aixm:length"
#define AIXM_SEG_DIM_WIDTH  "aixm:timeSlice/aixm:RouteSegmentTimeSlice/aixm:widthLeft/$(uom)"
#define AIXM_SEG_WIDTH_L    "aixm:timeSlice/aixm:RouteSegmentTimeSlice/aixm:widthLeft"
#define AIXM_SEG_WIDTH_R    "aixm:timeSlice/aixm:RouteSegmentTimeSlice/aixm:widthRight"
#define AIXM_SEG_POS        "aixm:timeSlice/aixm:RouteSegmentTimeSlice/aixm:curveExtent/aixm:Curve/gml:segments/gml:GeodesicString/gml:posList"

#define AIXM_SEG_ROUTE_NAME "aixm:timeSlice/aixm:RouteSegmentTimeSlice/aixm:routeFormed/$(xlink:title)"
#define AIXM_SEG_ROUTE_UUID "aixm:timeSlice/aixm:RouteSegmentTimeSlice/aixm:routeFormed/$(xlink:href)"
#define AIXM_SEG_START_REP  "aixm:timeSlice/aixm:RouteSegmentTimeSlice/aixm:start/aixm:EnRouteSegmentPoint/aixm:reportingATC"
#define AIXM_SEG_END_REP    "aixm:timeSlice/aixm:RouteSegmentTimeSlice/aixm:end/aixm:EnRouteSegmentPoint/aixm:reportingATC"
#define AIXM_SEG_START_NAME "aixm:timeSlice/aixm:RouteSegmentTimeSlice/aixm:start/aixm:EnRouteSegmentPoint/aixm:pointChoice_fixDesignatedPoint/$(xlink:title)"
#define AIXM_SEG_END_NAME   "aixm:timeSlice/aixm:RouteSegmentTimeSlice/aixm:end/aixm:EnRouteSegmentPoint/aixm:pointChoice_fixDesignatedPoint/$(xlink:title)"
#define AIXM_SEG_START_UUID "aixm:timeSlice/aixm:RouteSegmentTimeSlice/aixm:start/aixm:EnRouteSegmentPoint/aixm:pointChoice_fixDesignatedPoint/$(xlink:href)"
#define AIXM_SEG_END_UUID   "aixm:timeSlice/aixm:RouteSegmentTimeSlice/aixm:end/aixm:EnRouteSegmentPoint/aixm:pointChoice_fixDesignatedPoint/$(xlink:href)"

#define AIXM_SEG_ROUTE      "aixm:timeSlice/aixm:RouteSegmentTimeSlice/aixm:routeFormed"
#define AIXM_SEG_START      "aixm:timeSlice/aixm:RouteSegmentTimeSlice/aixm:start/aixm:EnRouteSegmentPoint/aixm:pointChoice_fixDesignatedPoint"
#define AIXM_SEG_END        "aixm:timeSlice/aixm:RouteSegmentTimeSlice/aixm:end/aixm:EnRouteSegmentPoint/aixm:pointChoice_fixDesignatedPoint"

#define AIXM_SEG_ORDER_NUM  "aixm:timeSlice/aixm:RouteSegmentTimeSlice/aixm:annotation/aixm:Note/aixm:translatedNote/aixm:LinguisticNote/aixm:note/$(lang=ENG:ORDER_NUM)"


#else
#define AIXM_HASMEMBER "hasMember"
#define AIXM_AIRSPACE  "Airspace"
#define AIXM_DESPOINT  "DesignatedPoint"
#define AIXM_ROUTE     "Route"
#define AIXM_ROUTE_SEG "RouteSegment"
#define AIXM_ID        "n0:id"
#define AIXM_TITLE     "n0:title"
#define AIXM_HREF      "n0:href"


//SECTOR (Airspace) (zoneX:FIR_UIR_AIRSPACES - class:C_Bound)
#define AIXM_UUID       "identifier"
#define AIXM_NAME       "aixm:timeSlice/aixm:AirspaceTimeSlice/aixm:name"
#define AIXM_DESIGN     "aixm:timeSlice/aixm:AirspaceTimeSlice/aixm:designator"
#define AIXM_NAME_EN    "aixm:timeSlice/aixm:AirspaceTimeSlice/aixm:annotation/aixm:Note/aixm:translatedNote/aixm:LinguisticNote/aixm:note/$(lang=ENG:NAME_EN)"
#define AIXM_DESIGN_EN  "aixm:timeSlice/aixm:AirspaceTimeSlice/aixm:annotation/aixm:Note/aixm:translatedNote/aixm:LinguisticNote/aixm:note/$(lang=ENG:DESIGNATOR_EN)"//"aixm:timeSlice/aixm:AirspaceTimeSlice/aixm:designator"
#define AIXM_NAME_RU    "aixm:timeSlice/aixm:AirspaceTimeSlice/aixm:annotation/aixm:Note/aixm:translatedNote/aixm:LinguisticNote/aixm:note/$(lang=RUS:NAME_RU)"
#define AIXM_DESIGN_RU  "aixm:timeSlice/aixm:AirspaceTimeSlice/aixm:annotation/aixm:Note/aixm:translatedNote/aixm:LinguisticNote/aixm:note/$(lang=RUS:DESIGNATOR_RU)"
#define AIXM_TYPE       "aixm:timeSlice/aixm:AirspaceTimeSlice/aixm:type"
#define AIXM_LTYPE      "aixm:timeSlice/aixm:AirspaceTimeSlice/aixm:localType"
#define AIXM_BTIME      "aixm:timeSlice/aixm:AirspaceTimeSlice/aixm:validTime/gml:TimePeriod/gml:beginPosition"
#define AIXM_ETIME      "aixm:timeSlice/aixm:AirspaceTimeSlice/aixm:validTime/gml:TimePeriod/gml:endPosition"
#define AIXM_F_BTIME    "aixm:timeSlice/aixm:AirspaceTimeSlice/aixm:featureLifetime/gml:TimePeriod/gml:beginPosition"
#define AIXM_F_ETIME    "aixm:timeSlice/aixm:AirspaceTimeSlice/aixm:featureLifetime/gml:TimePeriod/gml:endPosition"
#define AIXM_SECTOR_POS "aixm:timeSlice/aixm:AirspaceTimeSlice/aixm:geometryComponent/aixm:AirspaceGeometryComponent/aixm:theAirspaceVolume/aixm:AirspaceVolume/aixm:horizontalProjection/Surface/patches/PolygonPatch/exterior/Ring/curveMember/Curve/segments/GeodesicString/posList"
#define AIXM_DIM_LIMIT  "aixm:timeSlice/aixm:AirspaceTimeSlice/aixm:geometryComponent/aixm:AirspaceGeometryComponent/aixm:theAirspaceVolume/aixm:AirspaceVolume/aixm:upperLimit/$(uom)"
#define AIXM_UPPER      "aixm:timeSlice/aixm:AirspaceTimeSlice/aixm:geometryComponent/aixm:AirspaceGeometryComponent/aixm:theAirspaceVolume/aixm:AirspaceVolume/aixm:upperLimit"
#define AIXM_LOWER      "aixm:timeSlice/aixm:AirspaceTimeSlice/aixm:geometryComponent/aixm:AirspaceGeometryComponent/aixm:theAirspaceVolume/aixm:AirspaceVolume/aixm:lowerLimit"

//ICAO (DesignatedPoint) (zoneX:WAYPOINTS - class:C_Waypoint)
#define AIXM_ICAO_UUID       "identifier"
#define AIXM_ICAO_NAME       "aixm:timeSlice/aixm:DesignatedPointTimeSlice/aixm:name"
#define AIXM_ICAO_DESIGN     "aixm:timeSlice/aixm:DesignatedPointTimeSlice/aixm:designator"
#define AIXM_ICAO_NAME_EN    "aixm:timeSlice/aixm:DesignatedPointTimeSlice/aixm:annotation/aixm:Note/aixm:translatedNote/aixm:LinguisticNote/aixm:note/$(lang=ENG:NAME_EN)"
#define AIXM_ICAO_DESIGN_EN  "aixm:timeSlice/aixm:DesignatedPointTimeSlice/aixm:annotation/aixm:Note/aixm:translatedNote/aixm:LinguisticNote/aixm:note/$(lang=ENG:DESIGNATOR_EN)"
#define AIXM_ICAO_NAME_RU    "aixm:timeSlice/aixm:DesignatedPointTimeSlice/aixm:annotation/aixm:Note/aixm:translatedNote/aixm:LinguisticNote/aixm:note/$(lang=RUS:NAME_RU)"
#define AIXM_ICAO_DESIGN_RU  "aixm:timeSlice/aixm:DesignatedPointTimeSlice/aixm:annotation/aixm:Note/aixm:translatedNote/aixm:LinguisticNote/aixm:note/$(lang=RUS:DESIGNATOR_RU)"
#define AIXM_ICAO_TYPE       "aixm:timeSlice/aixm:DesignatedPointTimeSlice/aixm:type"
#define AIXM_ICAO_LTYPE      "aixm:timeSlice/aixm:DesignatedPointTimeSlice/aixm:localType"
#define AIXM_ICAO_BTIME      "aixm:timeSlice/aixm:DesignatedPointTimeSlice/aixm:validTime/gml:TimePeriod/gml:beginPosition"
#define AIXM_ICAO_ETIME      "aixm:timeSlice/aixm:DesignatedPointTimeSlice/aixm:validTime/gml:TimePeriod/gml:endPosition"
#define AIXM_ICAO_F_BTIME    "aixm:timeSlice/aixm:DesignatedPointTimeSlice/aixm:featureLifetime/gml:TimePeriod/gml:beginPosition"
#define AIXM_ICAO_F_ETIME    "aixm:timeSlice/aixm:DesignatedPointTimeSlice/aixm:featureLifetime/gml:TimePeriod/gml:endPosition"
#define AIXM_ICAO_POS        "aixm:timeSlice/aixm:DesignatedPointTimeSlice/aixm:location/ElevatedPoint/pos"
#define AIXM_ICAO_DIM_LIMIT  "aixm:timeSlice/aixm:DesignatedPointTimeSlice/aixm:location/AirspaceGeometryComponent/upperLimit/$(uom)"
#define AIXM_ICAO_UPPER      "aixm:timeSlice/aixm:DesignatedPointTimeSlice/aixm:location/AirspaceGeometryComponent/upperLimit"
#define AIXM_ICAO_LOWER      "aixm:timeSlice/aixm:DesignatedPointTimeSlice/aixm:location/AirspaceGeometryComponent/lowerLimit"

//ATS (Route) (zoneX:AIRWAYS - class:C_Airway)
#define AIXM_ATS_UUID       "identifier"
#define AIXM_ATS_NAME       "aixm:timeSlice/aixm:RouteTimeSlice/aixm:annotation/aixm:Note/aixm:translatedNote/aixm:LinguisticNote/aixm:note/$(lang=ENG:DESIGNATOR_EN)"
#define AIXM_ATS_DESIGN     "aixm:timeSlice/aixm:RouteTimeSlice/aixm:annotation/aixm:Note/aixm:translatedNote/aixm:LinguisticNote/aixm:note/$(lang=ENG:DESIGNATOR_EN)"
#define AIXM_ATS_NAME_RU    "aixm:timeSlice/aixm:RouteTimeSlice/aixm:annotation/aixm:Note/aixm:translatedNote/aixm:LinguisticNote/aixm:note/$(lang=RUS:DESIGNATOR_RU)"
#define AIXM_ATS_DESIGN_RU  "aixm:timeSlice/aixm:RouteTimeSlice/aixm:annotation/aixm:Note/aixm:translatedNote/aixm:LinguisticNote/aixm:note/$(lang=RUS:DESIGNATOR_RU)"
#define AIXM_ATS_BTIME      "aixm:timeSlice/aixm:RouteTimeSlice/validTime/gml:TimePeriod/gml:beginPosition"
#define AIXM_ATS_ETIME      "aixm:timeSlice/aixm:RouteTimeSlice/validTime/gml:TimePeriod/gml:endPosition"
#define AIXM_ATS_F_BTIME    "aixm:timeSlice/aixm:RouteTimeSlice/featureLifetime/gml:TimePeriod/gml:beginPosition"
#define AIXM_ATS_F_ETIME    "aixm:timeSlice/aixm:RouteTimeSlice/featureLifetime/gml:TimePeriod/gml:endPosition"

#define AIXM_ATS_DES_PREF    "aixm:timeSlice/aixm:RouteTimeSlice/designatorPrefix"
#define AIXM_ATS_SCND_LETER  "aixm:timeSlice/aixm:RouteTimeSlice/designatorSecondLetter"
#define AIXM_ATS_NUMBER      "aixm:timeSlice/aixm:RouteTimeSlice/designatorNumber"

//(RouteSegment) (zoneX:AIRWAYS - class:C_Airway)
#define AIXM_SEG_UUID       "identifier"
#define AIXM_SEG_BTIME      "aixm:timeSlice/aixm:RouteSegmentTimeSlice/aixm:validTime/gml:TimePeriod/gml:beginPosition"
#define AIXM_SEG_ETIME      "aixm:timeSlice/aixm:RouteSegmentTimeSlice/aixm:validTime/gml:TimePeriod/gml:endPosition"
#define AIXM_SEG_F_BTIME    "aixm:timeSlice/aixm:RouteSegmentTimeSlice/aixm:featureLifetime/gml:TimePeriod/gml:beginPosition"
#define AIXM_SEG_F_ETIME    "aixm:timeSlice/aixm:RouteSegmentTimeSlice/aixm:featureLifetime/gml:TimePeriod/gml:endPosition"
#define AIXM_SEG_DIM_LIMIT  "aixm:timeSlice/aixm:RouteSegmentTimeSlice/aixm:upperLimit/$(uom)"
#define AIXM_SEG_UPPER      "aixm:timeSlice/aixm:RouteSegmentTimeSlice/aixm:upperLimit"
#define AIXM_SEG_LOWER      "aixm:timeSlice/aixm:RouteSegmentTimeSlice/aixm:lowerLimit"
#define AIXM_SEG_LENGHT     "aixm:timeSlice/aixm:RouteSegmentTimeSlice/aixm:length"
#define AIXM_SEG_WIDTH_L    "aixm:timeSlice/aixm:RouteSegmentTimeSlice/aixm:widthLeft"
#define AIXM_SEG_WIDTH_R    "aixm:timeSlice/aixm:RouteSegmentTimeSlice/aixm:widthRight"
#define AIXM_SEG_POS        "aixm:timeSlice/aixm:RouteSegmentTimeSlice/aixm:curveExtent/Curve/segments/GeodesicString/posList"

#define AIXM_SEG_ROUTE_NAME "aixm:timeSlice/aixm:RouteSegmentTimeSlice/aixm:routeFormed/$(n0:title)"
#define AIXM_SEG_ROUTE_UUID "aixm:timeSlice/aixm:RouteSegmentTimeSlice/aixm:routeFormed/$(n0:href)"
#define AIXM_SEG_START_NAME "aixm:timeSlice/aixm:RouteSegmentTimeSlice/aixm:start/aixm:EnRouteSegmentPoint/aixm:pointChoice_fixDesignatedPoint/$(n0:title)"
#define AIXM_SEG_END_NAME   "aixm:timeSlice/aixm:RouteSegmentTimeSlice/aixm:end/aixm:EnRouteSegmentPoint/aixm:pointChoice_fixDesignatedPoint/$(n0:title)"
#define AIXM_SEG_START_UUID "aixm:timeSlice/aixm:RouteSegmentTimeSlice/aixm:start/aixm:EnRouteSegmentPoint/aixm:pointChoice_fixDesignatedPoint/$(n0:href)"
#define AIXM_SEG_END_UUID   "aixm:timeSlice/aixm:RouteSegmentTimeSlice/aixm:end/aixm:EnRouteSegmentPoint/aixm:pointChoice_fixDesignatedPoint/$(n0:href)"

#define AIXM_SEG_ROUTE      "aixm:timeSlice/aixm:RouteSegmentTimeSlice/aixm:routeFormed"
#define AIXM_SEG_START      "aixm:timeSlice/aixm:RouteSegmentTimeSlice/aixm:start/aixm:EnRouteSegmentPoint/aixm:pointChoice_fixDesignatedPoint"
#define AIXM_SEG_END        "aixm:timeSlice/aixm:RouteSegmentTimeSlice/aixm:end/aixm:EnRouteSegmentPoint/aixm:pointChoice_fixDesignatedPoint"
#endif //AIXM_WITHOUT_NAMESPACE

#endif // AIXMDEFPATHS_H
