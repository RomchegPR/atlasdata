#include <QCoreApplication>
#include "DataStorage/DataStorage.h"
#include <QDebug>
#include <Loader/Loader.h>

int main(int argc, char *argv[])
{
    Loader mainLoader;
    mainLoader.readFilesFromDefaultFolder();
//    QCoreApplication a(argc, argv);
//    AixmApi* aixmApi = AixmApi::getInstance();
//    aixmApi->parseFile("C:\\Users/tas/WorkProjects/Atlas2017/atlasdata/AIXMReader/Res/aixmdataLED.xml");

//    QVector<Procedure*> procedures = aixmApi->getAllProcedures();
//    for(Procedure* procdeure : procedures){
//        dataStorage->addObject(procdeure);
//    }

//    QVector<VerticalStructure*> obstacles = aixmApi->getAllObstacles();
//    for (VerticalStructure* obstacle : obstacles ){
//         dataStorage->addObject(obstacle);
//    }

//    foreach (SceneObject* sceneObject, dataStorage->getAllObjects())
//      qDebug() << sceneObject->;

//    C:\Users\pra\aximreader\AIXMReader\aixmdataLED.xml

    // Пример для вывода данных о воздушных пространствах или по типу данных воздушного пространства:


//    QVector<VerticalStructure*> obstacles = aixmApi->getAllObstacles();
//    for (VerticalStructure* obstacle : obstacles ){
//        qDebug() << 1;
//        qDebug() << obstacle->getName();
//        qDebug() << obstacle->getType()._to_string();
//    }

//    QVector<Airspace*> airspaces = aixmApi->getAllAirspaces();
//    //QVector<Airspace*> airspaces = aixmApi->getAirspacesByType(CodeAirspace::R);
//    for(Airspace* airspace : airspaces){
//        qDebug() << "Airspace :";
//        qDebug() << "Uuid :" << airspace->getUuid();
//        qDebug() << "Name :" << airspace->getName();
//        qDebug() << "Control type :" << airspace->getControlType()._to_string();
//        qDebug() << "Designator :" << airspace->getDesignator();
//        qDebug() << "DesignatorICAO :" << ((airspace->getDesignatorICAO() == true) ? "YES" : "NO");
//        qDebug() << "Local type :" << airspace->getLocalType();
//        qDebug() << "Type :" << airspace->getType()._to_string();

//        QVector<AirspaceVolume*> volumes = airspace->getVolumes();
//        qDebug() << "Volumes :";
//        for(AirspaceVolume* volume : volumes){
//            qDebug() << "---------------------------------";
//            qDebug() << "Lower limit :" << volume->getLowerLimit();
//            qDebug() << "Lower limit reference :" << volume->getLowerLimitReference()._to_string();
//            qDebug() << "Upper limit :" << volume->getUpperLimit();
//            qDebug() << "Upper limit reference :" << volume->getUpperLimitReference()._to_string();
//            qDebug() << "Coords :" << volume->getHorizontalProjection();
//        }
//        qDebug() << "\n";
//    }

    // Пример поиска по имени

    /*Airspace* airspace = aixmApi->getAirspaceByName("UUR380");
    qDebug() << "Airspace :";
    qDebug() << "Uuid :" << airspace->getUuid();
    qDebug() << "Name :" << airspace->getName();
    qDebug() << "Control type :" << airspace->getControlType()._to_string();
    qDebug() << "Designator :" << airspace->getDesignator();
    qDebug() << "DesignatorICAO :" << ((airspace->getDesignatorICAO() == true) ? "YES" : "NO");
    qDebug() << "Local type :" << airspace->getLocalType();
    qDebug() << "Type :" << airspace->getType()._to_string();

    QVector<AirspaceVolume*> volumes = airspace->getVolumes();
    qDebug() << "Volumes :";
    for(AirspaceVolume* volume : volumes){
        qDebug() << "---------------------------------";
        qDebug() << "Lower limit :" << volume->getLowerLimit();
        qDebug() << "Lower limit reference :" << volume->getLowerLimitReference()._to_string();
        qDebug() << "Upper limit :" << volume->getUpperLimit();
        qDebug() << "Upper limit reference :" << volume->getUpperLimitReference()._to_string();
        qDebug() << "Coords :" << volume->getHorizontalProjection();
    }
    qDebug() << "\n";*/


    // Пример для вывода данных о процедурах или процедурах по типу процедуры


//    QVector<Procedure*> procedures = aixmApi->getAllProcedures();
//    QVector<Procedure*> procedures = aixmApi->getProceduresByType(ProcedureType::StandardInstrumentArrival);
//    for(Procedure* procdeure : procedures){
//        qDebug() << "Procedure :";
//        qDebug() << "Uuid :" << procdeure->getUuid();
//        qDebug() << "Communication Failure Instruction :" << procdeure->getCommunicationFailureInstruction();
//        qDebug() << "Instruction :" << procdeure->getInstruction();
//        qDebug() << "Design Criteria :" << procdeure->getDesignCriteria()._to_string();
//        qDebug() << "Coding Standard :" << procdeure->getCodingStandard()._to_string();
//        qDebug() << "" << procdeure->getType()._to_string();
//        qDebug() << "Flight checked :" << ((procdeure->getFlightChecked() == true) ? "YES" : "NO");
//        qDebug() << "Name :" << procdeure->getName();
//        qDebug() << "RNAV :" << ((procdeure->getRNAV() == true) ? "YES" : "NO");
//        qDebug() << "Procedure type :" << procdeure->getType()._to_string();


//        QVector < ProcedureTransition* > transitions = procdeure->getFlightTransition();
//        qDebug() << "Segments :";
//        for(ProcedureTransition* transition : transitions){
//            QVector<SegmentLeg* > segments = transition->getSegments();
//            for(SegmentLeg* segment : segments){
//                qDebug() << "---------------------------------";
//                qDebug() << "Uuid :" << segment->getUuid();
//                qDebug() << "Segment Type :" << segment->getSegmentLegType()._to_string();
//                qDebug() << "Lower limit :" << segment->getLowerLimitAltitude();
//                qDebug() << "Upper limit :" << segment->getUpperLimitAltitude();
//                TerminalSegmentPoint* startPoint = segment->getStartPoint();
//                if(startPoint != NULL){
//                    qDebug() << "Start point :" << startPoint->getFixDesignatedPoint()->getUuid();
//                }
//                TerminalSegmentPoint* endPoint = segment->getEndPoint();
//                if(endPoint != NULL){
//                   qDebug() << "End point :" << endPoint->getFixDesignatedPoint()->getUuid();
//                }
//                qDebug() << "Coords :" << segment->getCoords();
//            }
//        }
//    }
//    qDebug() << 1;
    return 0;

//    return a.exec();
}
