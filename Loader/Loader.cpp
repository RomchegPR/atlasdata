#include "Loader.h"
#include <QFile>
#include <QDir>
#include <ApplicationSettings.h>
#include <QDebug>

Loader::Loader()
{

}

Loader::readFilesFromDefaultFolder()
{
    qDebug() << applicationSettings.MainWorkingDirectory;
    QDir defaultDirectory = applicationSettings.MainWorkingDirectory;
    if(!defaultDirectory.exists())
    {
        defaultDirectory.mkdir(applicationSettings.MainWorkingDirectory);
        qDebug() << "Creating " << applicationSettings.MainWorkingDirectory << " directory";
    }
    else
    {
        mFilesNames = defaultDirectory.entryList();
        qDebug() << mFilesNames;
    }
}

Loader::check
