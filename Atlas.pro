QT += core xml
QT -= gui

CONFIG += c++11

TARGET = AtlasData
CONFIG += console
CONFIG -= app_bundle
QMAKE_CXXFLAGS += -std=c++0x

TEMPLATE = app

HEADERS += \
    AIXMReader/CodeDesignStandard.h \
    AIXMReader/AIXMNames.h \
    AIXMReader/AIXMDOMParser.h \
    AIXMReader/AixmApi.h \
    AIXMReader/DataTypes/CodeAircraft.h \
    AIXMReader/DataTypes/CodeAircraftCategory.h \
    AIXMReader/DataTypes/CodeAircraftEngine.h \
    AIXMReader/DataTypes/CodeaircraftEngineNumber.h \
    AIXMReader/DataTypes/CodeAircraftWingSpanClass.h \
    AIXMReader/DataTypes/codeairspace.h \
    AIXMReader/DataTypes/codeairspaceaggregation.h \
    AIXMReader/DataTypes/CodeAltitudeUse.h \
    AIXMReader/DataTypes/CodeApproach.h \
    AIXMReader/DataTypes/CodeApproachEquipmentAdditional.h \
    AIXMReader/DataTypes/CodeApproachGuidance.h \
    AIXMReader/DataTypes/CodeApproachPrefix.h \
    AIXMReader/DataTypes/CodeAtcReporting.h \
    AIXMReader/DataTypes/CodeBearing.h \
    AIXMReader/DataTypes/CodeCardinalDirection.h \
    AIXMReader/DataTypes/codecolour.h \
    AIXMReader/DataTypes/CodeCommunicationMode.h \
    AIXMReader/DataTypes/CodeCourse.h \
    AIXMReader/DataTypes/CodeDesignatedPoint.h \
    AIXMReader/DataTypes/CodeDesignStandard.h \
    AIXMReader/DataTypes/CodeDirectionReference.h \
    AIXMReader/DataTypes/CodeDirectionTurn.h \
    AIXMReader/DataTypes/CodeDistanceIndication.h \
    AIXMReader/DataTypes/CodeEquipmentAntiCollision.h \
    AIXMReader/DataTypes/CodeFinalGuidance.h \
    AIXMReader/DataTypes/codemilitaryoperations.h \
    AIXMReader/DataTypes/CodeMissedApproach.h \
    AIXMReader/DataTypes/CodeNavigationEquipment.h \
    AIXMReader/DataTypes/CodeNavigationSpecification.h \
    AIXMReader/DataTypes/CodeProcedureCodingStandard.h \
    AIXMReader/DataTypes/CodeProcedureFixRole.h \
    AIXMReader/DataTypes/CodeProcedurePhase.h \
    AIXMReader/DataTypes/CodeRelativePosition.h \
    AIXMReader/DataTypes/CodeSegmentPath.h \
    AIXMReader/DataTypes/CodeSegmentTermination.h \
    AIXMReader/DataTypes/CodeSide.h \
    AIXMReader/DataTypes/CodeSpeedReference.h \
    AIXMReader/DataTypes/codestatusconstruction.h \
    AIXMReader/DataTypes/CodeTrajectory.h \
    AIXMReader/DataTypes/CodeTransponder.h \
    AIXMReader/DataTypes/CodeValueInterpretation.h \
    AIXMReader/DataTypes/CodeVerticalReference.h \
    AIXMReader/DataTypes/codeverticalstructure.h \
    AIXMReader/DataTypes/codeverticalstructuremarking.h \
    AIXMReader/DataTypes/codeverticalstructurematerial.h \
    AIXMReader/DataTypes/CodeWakeTurbulence.h \
    AIXMReader/DataTypes/geometrytype.h \
    AIXMReader/DataTypes/ProcedureType.h \
    AIXMReader/DataTypes/SegmentLegType.h \
    AIXMReader/Entities/AircraftCharacteristic.h \
    AIXMReader/Entities/AirspaceGeometryComponent.h \
    AIXMReader/Entities/AirspaceVolume.h \
    AIXMReader/Entities/AngleIndication.h \
    AIXMReader/Entities/ApproachLeg.h \
    AIXMReader/Entities/ArrivalFeederLeg.h \
    AIXMReader/Entities/ArrivalLeg.h \
    AIXMReader/Entities/BaseEntity.h \
    AIXMReader/Entities/Curve.h \
    AIXMReader/Entities/DepartureLeg.h \
    AIXMReader/Entities/DesignatedPoint.h \
    AIXMReader/Entities/DistanceIndication.h \
    AIXMReader/Entities/FinalLeg.h \
    AIXMReader/Entities/HoldingPattern.h \
    AIXMReader/Entities/InitialLeg.h \
    AIXMReader/Entities/InstrumentApproachProcedure.h \
    AIXMReader/Entities/IntermediateLeg.h \
    AIXMReader/Entities/MissedApproachLeg.h \
    AIXMReader/Entities/ProcedureTransition.h \
    AIXMReader/Entities/ProcedureTransitionLeg.h \
    AIXMReader/Entities/SegmentLeg.h \
    AIXMReader/Entities/StandardInstrumentArrival.h \
    AIXMReader/Entities/StandardInstrumentDeparture.h \
    AIXMReader/Entities/TerminalSegmentPoint.h \
    AIXMReader/Entities/VerticalStructure.h \
    AIXMReader/Entities/VerticalStructurePart.h \
    AIXMReader/Reader/AIXMData.h \
    AIXMReader/Reader/AIXMDefPaths.h \
    AIXMReader/Reader/AIXMReader.h \
    AIXMReader/Utils/AixmEnumMapper.h \
    AIXMReader/Utils/AixmHelper.h \
    AIXMReader/Utils/enum.h \
    AIXMReader/Utils/MapperFromXml.h \
    AIXMReader/Utils/ProcedureFactory.h \
    AIXMReader/Utils/SegmentLegFactory.h \
    DataStorage/Factory/SceneObjectFactory.h \
    DataStorage/RenderData/RenderData.h \
    DataStorage/InputData.h \
    DataStorage/SceneObject.h \
    DataStorage/Factory/AirspaceObjectFactory.h \
    AIXMReader/Entities/Airspace.h \
    AIXMReader/Entities/Procedure.h \
    DataStorage/DataStorage.h \
    Loader/Loader.h \
    ApplicationSettings.h

SOURCES += \
    AIXMReader/CodeDesignStandard.cpp \
    AIXMReader/AIXMDOMParser.cpp \
    AIXMReader/AixmApi.cpp \
    AIXMReader/Entities/AircraftCharacteristic.cpp \
    AIXMReader/Entities/AirspaceGeometryComponent.cpp \
    AIXMReader/Entities/AirspaceVolume.cpp \
    AIXMReader/Entities/AngleIndication.cpp \
    AIXMReader/Entities/ApproachLeg.cpp \
    AIXMReader/Entities/ArrivalFeederLeg.cpp \
    AIXMReader/Entities/ArrivalLeg.cpp \
    AIXMReader/Entities/BaseEntity.cpp \
    AIXMReader/Entities/Curve.cpp \
    AIXMReader/Entities/DepartureLeg.cpp \
    AIXMReader/Entities/DesignatedPoint.cpp \
    AIXMReader/Entities/DistanceIndication.cpp \
    AIXMReader/Entities/FinalLeg.cpp \
    AIXMReader/Entities/HoldingPattern.cpp \
    AIXMReader/Entities/InitialLeg.cpp \
    AIXMReader/Entities/InstrumentApproachProcedure.cpp \
    AIXMReader/Entities/IntermediateLeg.cpp \
    AIXMReader/Entities/MissedApproachLeg.cpp \
    AIXMReader/Entities/ProcedureTransition.cpp \
    AIXMReader/Entities/ProcedureTransitionLeg.cpp \
    AIXMReader/Entities/SegmentLeg.cpp \
    AIXMReader/Entities/StandardInstrumentArrival.cpp \
    AIXMReader/Entities/StandardInstrumentDeparture.cpp \
    AIXMReader/Entities/TerminalSegmentPoint.cpp \
    AIXMReader/Entities/VerticalStructure.cpp \
    AIXMReader/Entities/VerticalStructurePart.cpp \
    AIXMReader/Reader/AIXMData.cpp \
    AIXMReader/Reader/AIXMReader.cpp \
    AIXMReader/Utils/AixmEnumMapper.cpp \
    AIXMReader/Utils/AixmHelper.cpp \
    AIXMReader/Utils/MapperFromXml.cpp \
    AIXMReader/Utils/ProcedureFactory.cpp \
    AIXMReader/Utils/SegmentLegFactory.cpp \
    main.cpp \
    DataStorage/Factory/SceneObjectFactory.cpp \
    DataStorage/RenderData/RenderData.cpp \
    DataStorage/DataStorage.cpp \
    DataStorage/InputData.cpp \
    DataStorage/SceneObject.cpp \
    DataStorage/Factory/AirspaceObjectFactory.cpp \
    AIXMReader/Entities/Airspace.cpp \
    AIXMReader/Entities/Procedure.cpp \
    Loader/Loader.cpp \
    ApplicationSettings.cpp

DISTFILES += \
    AIXMReader/Res/14_03_2016_aixm.xml \
    AIXMReader/Res/AIXM_Airspace.xml \
    AIXMReader/Res/AIXM_Airspace_ULLI.xml \
    AIXMReader/Res/aixm_obstacles.xml \
    AIXMReader/Res/AIXM_Obstacles_Moscow.xml \
    AIXMReader/Res/aixmdataLED.xml \
    AIXMReader/Res/allinone.xml \
    AIXMReader/Res/DM_01.xml \
    AIXMReader/Res/Donlon.xml \
    AIXMReader/Res/export_sample_LSZB_1.aixm5 \
    AIXMReader/Res/MA-19.xml \
    AIXMReader/Res/test_aixm.xml
