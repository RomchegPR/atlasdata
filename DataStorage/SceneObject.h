#ifndef SCENEOBJECT_H
#define SCENEOBJECT_H
#include "RenderData/RenderData.h"
#include "InputData.h"
#include <QString>

class SceneObject
{
public:
    enum AeroObjectType {
        AIRSPACE,
        OBSTACLE,
        PROCEDURE,
        AIRWAY
    };

    enum SceneObjectStatus
    {
        PAINT,
        BLUR,
        NOPAINT
    };

    SceneObject();
    virtual ~SceneObject() {}


private:
    RenderData              mRenderData;
    SceneObjectStatus       mStatus;

};

#endif // SCENEOBJECT_H


