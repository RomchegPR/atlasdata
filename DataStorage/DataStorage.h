#ifndef DATASOURCE_H
#define DATASOURCE_H
#include <QVector>
#include "AIXMReader/Entities/DesignatedPoint.h"
#include "AIXMReader/Entities/Procedure.h"
#include "AIXMReader/Entities/ProcedureTransition.h"
#include "AIXMReader/Entities/Airspace.h"
#include "AIXMReader/Entities/VerticalStructure.h"
#include "AIXMReader/Entities/BaseEntity.h"
#include <QMap>

class Procedure;

// Класс (синглтон), хранящий данные обо всем.
// Каждый get метод возвращает указатель на объект или NULL, если объект не найден
class DataStorage
{
private:
    static DataStorage* instance;

private:    
    QVector < DesignatedPoint* >     mDesignatedPoints;
    QVector < SegmentLeg* >          mSegmentLegs;
    QVector < Procedure* >           mProcedures;
    QVector < Airspace* >            mAirspaces;
    QVector < VerticalStructure* >   mObstacles;
    QMap < QString, BaseEntity* >    mAllObjects;

    DataStorage( ) {}
    DataStorage( DataStorage& );
    DataStorage& operator=( DataStorage& );

public:
    virtual ~DataStorage( );
    static DataStorage* getInstance( );

    void addDesignatedPoint(QString key, DesignatedPoint* point);
    DesignatedPoint* getDesignatedPoint(QString key);

    void addSegmentLeg(QString key, SegmentLeg* segment);
    SegmentLeg* getSegmentLeg(QString key);

    void addProcedure(QString key, Procedure* procedure);
    Procedure* getProcedure(QString key);
    QVector < Airspace* > getAirspacesByType(CodeAirspace type);
    void addAirspace(QString key, Airspace* airspace);
    Airspace* getAirspace(QString key);

    void addObstacle(QString key, VerticalStructure* obstacle);
    VerticalStructure* getObstacle(QString key);

    BaseEntity* getObject(QString key);

    QVector <Airspace*>& getAirspaces();
    QVector <Procedure*>& getProcedures();
    QVector <DesignatedPoint*>& getDesignatedPoints();
    QVector <VerticalStructure*>& getObstacles();
    QMap < QString, BaseEntity* >& getAllObjects();
};

#endif // DATASOURCE_H
