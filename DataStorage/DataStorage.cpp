#include "DataStorage.h"

DataStorage* DataStorage::instance = NULL;

DataStorage::~DataStorage()
{
    delete instance;
}

DataStorage* DataStorage::getInstance(){
    if( !instance ){
        instance = new DataStorage();
    }
    return instance;
}

void DataStorage::addDesignatedPoint(QString key, DesignatedPoint *point){
    mDesignatedPoints.push_back(point);
    mAllObjects.insert(key, point);

}

DesignatedPoint* DataStorage::getDesignatedPoint(QString key){
    DesignatedPoint* disignatedPoint = NULL;

    for (DesignatedPoint* designatedPointTmp : mDesignatedPoints)
    {
        if (designatedPointTmp->getUuid() == key)
        {
            disignatedPoint = designatedPointTmp;
            break;
        }
    }
    return disignatedPoint;
}


void DataStorage::addSegmentLeg(QString key, SegmentLeg *segment){
    mSegmentLegs.push_back(segment);
    mAllObjects.insert(key, segment);
}

SegmentLeg* DataStorage::getSegmentLeg(QString key){
    SegmentLeg* segment = NULL;

    for (SegmentLeg* segmentTmp : mSegmentLegs)
    {
        if (segmentTmp->getUuid() == key)
        {
            segment = segmentTmp;
            break;
        }
    }
    return segment;
}

void DataStorage::addProcedure(QString key, Procedure *procedure){
    mProcedures.push_back(procedure);
    mAllObjects.insert(key, procedure);
}

Procedure* DataStorage::getProcedure(QString key){
    Procedure* procedure = NULL;

    for (Procedure* procedureTmp : mProcedures)
    {
        if (procedureTmp->getUuid() == key)
        {
            procedure = procedureTmp;
            break;
        }
    }
    return procedure;
}

void DataStorage::addAirspace(QString key, Airspace *airspace){
    mAirspaces.push_back(airspace);
    mAllObjects.insert(key, airspace);
}

Airspace* DataStorage::getAirspace(QString key){
    Airspace* airspace = NULL;

    for (Airspace* airspaceTmp : mAirspaces)
    {
        if (airspaceTmp->getUuid() == key)
        {
            airspace = airspaceTmp;
            break;
        }
    }
    return airspace;
}

QVector < Airspace* >& DataStorage::getAirspaces(){
    return mAirspaces;
}

QVector < Procedure* >& DataStorage::getProcedures(){
    return mProcedures;
}

QVector<Airspace*> DataStorage::getAirspacesByType(CodeAirspace type){
    QVector<Airspace*> filteredAirspaces;
    for(Airspace* airspace : mAirspaces){
        if(airspace->getType() == type){
            filteredAirspaces.append(airspace);
        }
    }
    return filteredAirspaces;
}

QVector < DesignatedPoint* >& DataStorage::getDesignatedPoints(){
    return mDesignatedPoints;
}

QVector < VerticalStructure* >& DataStorage::getObstacles(){
    return mObstacles;
}

QMap < QString, BaseEntity* >& DataStorage::getAllObjects(){
    return mAllObjects;
}

void DataStorage::addObstacle(QString key, VerticalStructure *obstacle){
    mObstacles.push_back(obstacle);
    mAllObjects.insert(key, obstacle);
}

VerticalStructure* DataStorage::getObstacle(QString key){
    VerticalStructure* obstacle = NULL;

    for (VerticalStructure* obstacleTmp : mObstacles)
    {
        if (obstacleTmp->getUuid() == key)
        {
            obstacle = obstacleTmp;
            break;
        }
    }
    return obstacle;
}

BaseEntity* DataStorage::getObject(QString key)
{
        BaseEntity* object = NULL;
        if(mAllObjects.contains(key)){
            object = mAllObjects[key];
        }
        return object;
}

