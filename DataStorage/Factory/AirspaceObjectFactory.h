#ifndef AIRSPACEFACTORY_H
#define AIRSPACEFACTORY_H
#include "DataStorage/Factory/SceneObjectFactory.h"

class BaseEntity;

class AirspaceObjectFactory : public SceneObjectFactory
{

private:
    AirspaceObjectFactory() {}
    AirspaceObjectFactory( const AirspaceObjectFactory&);
    AirspaceObjectFactory& operator=( AirspaceObjectFactory& );

public:
    static AirspaceObjectFactory& getInstance() {
        static AirspaceObjectFactory  instance;
        return instance;
    }

public:
    SceneObject* createObject(BaseEntity *AixmObject) override;
};

#endif // AIRSPACEFACTORY_H
