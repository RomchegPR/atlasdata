#ifndef SCENEOBJECTFACTORY_H
#define SCENEOBJECTFACTORY_H
#include "DataStorage/SceneObject.h"
#include "AIXMReader/Entities/BaseEntity.h"

class SceneObjectFactory
{
public :

   virtual SceneObject* createObject(BaseEntity *AixmObject) = 0;
   virtual ~SceneObjectFactory() {};
};

#endif // SCENEOBJECTFACTORY_H
